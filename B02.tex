\chapter{Campionamento e quantizzazione}
\section[]{Campionamento\footnote{Si veda il capitolo~\ref{cap7}.}}

\section{Quantizzazione}
\begin{figure}
	\centering
	\includegraphics[width=0.45\linewidth]{pic/B02/Digital-signal.pdf}
	\caption[]{\footnotemark}
\end{figure}
\footnotetext{Questa immagine è tratta da Wikimedia Commons (\href{https://commons.wikimedia.org/wiki/File:Digital.signal.svg}{Digital.signal.svg}), è stata realizzata da \href{https://cs.wikipedia.org/wiki/Wikipedista:Petr.adamek}{Petr Adámek}, dall'utente \href{https://commons.wikimedia.org/wiki/User:Rbj}{Rbj} e da \href{https://de.wikipedia.org/wiki/Benutzer:Wdwd}{Walter Dvorak} e si trova nel dominio pubblico.}

Un quantizzatore con \textbf{risoluzione} $n_q$ suddivide l'intervallo di ampiezze $\left[ -V , V \right]$ in un numero finito $L = 2^{n_q}$ intervalli di ampiezza uniforme:
\[
\Delta = \frac{2V}{L}
\]

Al centro di ogni intervallo vi è un livello, che è rappresentato da una sequenza di $n_q$ bit.

L'operazione di \textbf{quantizzazione} permette di rappresentare un segnale in forma numerica: ogni campione della sequenza reale $x \left[ n \right]$ campionata viene approssimato al livello associato all'intervallo a cui appartiene:
\[
y = Q \left[ x \left( n T_c \right) \right] = \begin{cases} -V + \frac{1}{2} \Delta & \text{se } x \left( n T_c \right) \in \left( -V , -V + \Delta \right) \\
-V + \frac{3}{2} \Delta & \text{se } x \left( n T_c \right) \in \left( -V + \Delta , -V + 2 \Delta \right) \\
\vdots & \vdots \\
V - \frac{3}{2} \Delta & \text{se } x \left( n T_c \right) \in \left( V - 2 \Delta , V - \Delta \right) \\
V - \frac{1}{2} \Delta & \text{se } x \left( n T_c \right) \in \left( V - \Delta , V  \right) \end{cases}
\]

La caratteristica ingresso/uscita di un quantizzatore $Q$ è una scalinata a $L$ livelli:
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.65]{pic/B02/Quantizer_characteristic}
\end{figure}

\subsection{Errore di quantizzazione}
\begin{figure}
	\centering
	\includegraphics[scale=0.65]{pic/B02/Errore_di_quantizzazione}
\end{figure}

\noindent
Si definisce \textbf{errore} (o rumore) \textbf{di quantizzazione} $\varepsilon_Q \left[ n \right]$ la differenza fra un campione reale $x \left[ n \right]$ e la sua versione quantizzata $x_Q \left[ n \right]$:
\[
\varepsilon_Q \left[ n \right] = x \left[ n \right] - x_Q \left[ n \right]
\]
\FloatBarrier

\subsection{Rapporto segnale rumore}
La qualità del segnale quantizzato è espressa in termini del \textbf{rapporto segnale rumore} $\text{SNR}$:
\[
\text{SNR}_Q = \frac{P_S}{P_N}
\]
dove:
\begin{itemize}
\item $P_S$ è la potenza del segnale non ancora quantizzato $x \left[ n \right]$:
\[
P_S = \lim_{N \to + \infty} \frac{1}{2N+1} \sum_{n = -N}^N {\left| x \left[ n \right] \right|}^2 = \frac{V^2}{3}
\]

\item $P_N$ è la potenza del rumore di quantizzazione $\varepsilon_Q \left[ n \right]$:
\[
P_N = \lim_{N \to + \infty} \frac{1}{2N+1} \sum_{n = -N}^N {\left| \varepsilon_Q \left[ n \right] \right|}^2 = \frac{\Delta^2}{12}
\]
\end{itemize}

\begin{framed}
\paragraph{Dimostrazione}
Se il segnale $x \left[ n \right]$ ha una distribuzione delle ampiezze uniforme nell'intervallo $\left[ -V , V \right]$:
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.55]{pic/B02/Distribuzione_di_x}
\end{figure}
\noindent
nell'ipotesi di impiegare un numero sufficientemente elevato di livelli di quantizzazione, l'errore di quantizzazione $\varepsilon_Q \left[ n \right]$ può essere modellato come un processo casuale bianco, ergodico, stazionario in senso lato, statisticamente indipendente (scorrelato) dal segnale $x \left[ n \right]$, e distribuito uniformemente nell'intervallo $\left[ - \frac{\Delta}{2}, \frac{\Delta}{2} \right]$:
\[
f \left( \varepsilon \right) = \begin{cases} \displaystyle {1 \over \Delta} & \displaystyle \text{se } \varepsilon \in \left[ - {\Delta \over 2}, {\Delta \over 2} \right] \\
0 & \text{altrimenti} \end{cases}
\]

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.55]{pic/B02/Distribuzione_di_epsilon}
\end{figure}

Siccome l'errore di quantizzazione $\varepsilon_Q \left[ n \right]$ è ergodico:
\[
P_N = \sigma_\varepsilon^2 = \int_{- \infty}^{+ \infty} \varepsilon^2 f \left( \varepsilon \right) d \varepsilon = \frac{1}{\Delta} \int_{- \frac{1}{\Delta}}^{{\Delta \over 2}} \varepsilon^2 d \varepsilon = \frac{\Delta^2}{12}
\]
e la potenza del segnale $x \left[ n \right]$:
\[
P_S = \frac{1}{2V} \int_{-V}^V x^2 dx = \frac{1}{2V} \left. \frac{x^3}{3} \right \vert_{-V}^V = \frac{V^2}{3}
\]
\end{framed}

Assumendo che la dinamica del quantizzatore $D$ sia uguale alla dinamica del segnale ($D = 2V$), si ricava che il rapporto segnale rumore $\text{SNR}$ si riduce all'aumentare del numero di livelli $L$:
\[
\text{SNR}_Q = L^2
\]

\begin{framed}
\paragraph{Dimostrazione}
\[ \Delta = \frac{D}{L} = \frac{2V}{L} \Rightarrow \text{SNR}_Q = \frac{P_S}{P_N} = \frac{\frac{V^2}{3}}{\frac{\Delta^2}{12}} = 4 \frac{V^2}{4 \frac{V^2}{L^2}} = L^2 \]
\end{framed}

Ogni bit aggiuntivo di risoluzione incrementa di 6 dB il rapporto segnale rumore:
\[
\left. \text{SNR}_Q \right \vert_{\text{dB}} \cong 6 n_Q
\]

\begin{framed}
\paragraph{Dimostrazione}
\[
\left. \text{SNR}_Q \right \vert_{\text{dB}} = 10 \log_{10} {\left( L^2 \right)} = 10 \log_{10} {\left( 2^{2 n_Q} \right)} = n \cdot 20 \log_{10} {2} \cong 6 n_Q
\]
\end{framed}

Se invece la dinamica del quantizzatore $D$ è sganciata da quella del segnale ($D < 2V$), compare un addendo logaritmico:
\[
\left. \text{SNR}_Q \right \vert_{\text{dB}} \cong 6 n_Q + 10 \log_{10} \left( \frac{4V^2}{D^2} \right)
\]

\begin{framed}
\paragraph{Dimostrazione}
\[
P_N = \frac{\Delta^2}{12} = \frac{D^2}{12 L^2}
\]
\[ \left. \text{SNR}_Q \right \vert_{\text{dB}} = 10 \log_{10} \frac{P_S}{P_N} = 10 \log_{10} \left( \frac{V^2}{3} {12L^2 \over D^2} \right) = \]
\[ = 10 \log_{10} \left( L^2 \right)  + 10 \log_{10} \left( \frac{4V^2}{D^2} \right) \cong 6 n_Q + 10 \log_{10} \left( \frac{4V^2}{D^2} \right) \]
\end{framed}