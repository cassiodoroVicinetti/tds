\chapter{Campionamento}
\label{cap7}
Un segnale analogico è più facile da elaborare se viene campionato in un segnale numerico, cioè tempo discreto e discreto in ampiezza. Come campionare un segnale tempo-continuo senza perdere informazione?

\paragraph{Teorema del campionamento (o di Nyquist-Shannon)}
Un segnale tempo-continuo può essere campionato e perfettamente ricostruito a partire dai suoi campioni se la frequenza di campionamento $f_c$ è maggiore del doppio della banda\footnote{Per banda si intende qui la lunghezza del supporto in frequenza.} $B$ del segnale:
\[
f_c \triangleq \frac{1}{T_c} > 2B
\]
con la condizione che la banda $B$ sia limitata.

Il teorema del campionamento garantisce che il segnale campionato può essere ricostruito perfettamente tramite un \textbf{filtro interpolatore} (ricostruttore), e che il segnale ricostruito coinciderà con il segnale tempo-continuo di partenza.

\section{Filtro anti-aliasing}
La maggioranza dei segnali utilizzati nella realtà ha banda illimitata: esiste un intervallo al di fuori del quale il segnale è significativamente vicino a zero, ma non è mai identicamente nullo. Il segnale campionato quindi presenterà nel dominio della frequenza delle sovrapposizioni degli spettri che alla fine non possono essere ricostruite dal filtro interpolatore. Il \textbf{filtro anti-aliasing} serve per eliminare le parti ad alta frequenza prima del campionamento:
\[
\mathrm{AA} \left( f \right)  \begin{cases} = 0 & \left| f \right| > B_{\mathrm{AA}} \\
\neq 0 & \text{altrove} \end{cases}
\]
con $B_x < B_{\mathrm{AA}} < \tfrac{f_c}{2}$. La distorsione del filtro anti-aliasing non è ugualmente rimediabile, ma è comunque preferibile rispetto all'effetto di aliasing (o sovrapposizione).

\section{Campionatori reali}
\begin{figure}
	\centering
	\includegraphics[scale=0.55]{pic/07/Campionatore_reale.png}
\end{figure}

\noindent
Il campionatore ideale (treno di delta):
\[
x ' \left( t \right) = x \left( t \right) * \delta \left( - t \right) = \sum_{n = - \infty}^{+ \infty} \delta \left( t - n T_c \right) x \left( t \right)
\]
è impossibile da realizzare nella realtà, perché la delta $\delta \left( t \right)$ è un impulso con ampiezza illimitata e supporto infinitesimo $\Rightarrow$ si utilizza un impulso $h \left( t \right)$ il più possibile simile alla delta, cioè con ampiezza molto grande e supporto molto piccolo:
\[
x ' \left( t \right) = x \left( t \right) * h \left( - t \right) \Rightarrow x ' \left( n T_c \right) = \int_{- \infty}^{+ \infty} h \left( \tau - n T_c \right) x \left( \tau \right) d \tau
\]
\FloatBarrier

\section{Interpolatori}
\subsection{Condizioni ideali}
Un segnale campionato\footnote{Si veda la sezione~\ref{sez:campionamento_nel_tempo_e_periodicizzazione_in_frequenza}.}:
\[
\begin{cases} \displaystyle x_c \left( t \right) = \sum_{n = - \infty}^{+ \infty} x \left( n T_c \right) \delta \left( t - n T_c \right) \\
\displaystyle X_c \left( f \right) = \frac{1}{T_c} \sum_{n = - \infty}^{+ \infty} X \left( f - \frac{n}{T_c} \right) \end{cases}
\]
può essere ricostruito tramite un filtro passa-basso ideale, detto \textbf{filtro ricostruttore ottimo} $K \left( f \right)$:
\[
x \left( t \right) = x_c \left( t \right) * K \left( t \right) =  \left[ \sum_{n = - \infty}^{+ \infty} x \left( n T_c \right) \delta \left( t - n T_c \right) \right] * K \left( t \right) = \sum_{n = - \infty}^{+ \infty} x \left( n T_c \right) K \left( t - n T_c \right) ,
\]
\[
\quad K \left( f \right ) = \begin{cases} T_c & \left| f \right| < B \\
\text{qualsiasi valore} & B < \left| f \right| < f_c - B \\
0 & \left| f \right| > f_c - B \end{cases}
\]

Il filtro ricostruttore ottimo $K \left( f \right)$ deve essere:
\begin{itemize}
\item non distorcente nella banda del segnale (piatto);
\item nullo al di fuori della banda del segnale per eliminare le componenti ad alta frequenza.
\end{itemize}

\subsubsection{Esempi di interpolatori distorcenti}
\paragraph{Costante a tratti}
Il segnale viene approssimato a una serie di rettangoli:
\[
\begin{cases} K \left( t \right) = {\Pi}_{T_c} \left( t \right) \\
K \left( f \right) = T_c \mathrm{sinc} \left( f T_c \right) \end{cases}
\]

\paragraph{Lineare}
Il segnale viene approssimato a una serie di trapezi:
\[
\begin{cases} K \left( t \right) = {\Lambda}_{T_c} \left( t \right) \\
K \left( f \right) = T_c {\mathrm{sinc}}^2 \left( f t_c \right) \end{cases}
\]

\subsubsection{Esempi di interpolatori non distorcenti}
\paragraph{Funzione sinc}
Il filtro interpolatore ideale è il seguente:
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{pic/07/Esempio_interpolatore_1.png}
\end{figure}
\[ \begin{cases} K \left( t \right) = B T_c \mathrm{sinc} \left( t B \right) \\
K \left( f \right)  = T_c \Pi_B \left( f \right) \end{cases} \]
perché il segnale viene ricostruito da una sommatoria di infinite funzioni sinc, dove per ogni $n$ esiste una sinc che assume esattamente il valore del campione $n$-esimo all'istante $n T_c$ e un valore nullo in tutti gli altri istanti di campionamento:
\[
x \left( t \right) = B T_c \sum_{n = - \infty}^{+ \infty} x \left( n T_c \right) \mathrm{sinc} \left( B \left( t - n T_c \right) \right)
\]

Tuttavia nei punti intermedi agli istanti di campionamento il segnale $x \left( t \right)$ è dato dalla somma dei contributi di infinite funzioni sinc:
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{pic/07/Esempio_interpolatore_2.png}
\end{figure}

\paragraph{Coseno rialzato}
\[ K \left( t \right) = N T_c \mathrm{sinc} \left( Bt \right) \cdot \frac{\cos{\alpha B \pi t}}{1 - {\alpha B t}^2} \]

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{pic/07/Esempio_interpolatore_3.png}
\end{figure}

\subparagraph{Condizioni:}
\begin{itemize}
\item roll-off $\alpha$: $0 < \alpha < 1$ (se $\alpha = 0$ diventa la funzione sinc)
\item $\frac{B}{2} \left( 1 - \alpha \right) > B_x$
\item $\frac{B}{2} \left( 1 + \alpha \right) < f_c - B_x$
\end{itemize}

\subsection{Condizioni reali}
Il filtro ricostruttore può integrare anche un filtro equalizzatore che rimedi agli effetti del filtro anti-aliasing e alle distorsioni provocate da un campionatore reale:
\[
K \left( f \right) = \begin{cases} \displaystyle \frac{T_c}{\mathrm{AA} \left( f \right) H \left( f \right)} & \left| f \right| < B_{\mathrm{AA}} \\
0 & \left| f \right| > f_c - B_{\mathrm{AA}} \end{cases}
\]
a patto che $H \left( f \right)$, cioè la trasformata di Fourier dell'impulso campionatore $h \left( t \right)$, non cancelli definitivamente qualche frequenza compresa nella banda $B_{\mathrm{AA}}$ del filtro anti-aliasing:
\[
H \left( f \right) \neq 0 \quad \forall \left| f \right| < B_{\mathrm{AA}}
\]

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{pic/07/Campionamento_reale.png}
\end{figure}