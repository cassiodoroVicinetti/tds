\chapter{Spettro di energia e segnali troncati}
\section{Segnali troncati}
\label{sez:segnali_troncati}
A volte non si conosce il segnale su tutto il supporto ma solo su un certo intervallo $T$:\footnote{Si sottointende che l'intervallo $T$ è centrato rispetto all'origine.}
\[ \begin{cases} x_T \left( t \right) = x \left( t \right) \cdot \Pi_T \left( t  \right) \\
X_T \left( f \right) = X \left( f \right) * \mathcal{F} \left\{ \Pi_T \left( t  \right) \right\} = X \left( f \right) * T \mathrm{sinc} \left( f T \right)  \end{cases} \]

Se il segnale $x \left( t \right)$ ha una banda limitata in frequenza:
\[ X \left( f \right) = 0 \quad \left| f \right| > \frac{B}{2} \]
si può scrivere:
\[ X_T \left( f \right) = T \int_{- \frac{T}{2}}^{\frac{T}{2}} X \left( \varphi \right) \mathrm{sinc} \left( \left( f - \varphi \right) T \right) d \varphi \]

Siccome un segnale a supporto limitato nel tempo non può avere una banda limitata in frequenza e viceversa, il segnale $x \left( t \right)$ è a banda limitata e al contrario il segnale troncato ha una banda illimitata:
\[
X_T \left( f \right) \neq 0 \quad \left| f \right| > \frac{B}{2}
\]
per effetto delle oscillazioni della funzione sinc:
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.55]{pic/08/Segnali_troncati.png}
\end{figure}

\subsection{Fenomeno di Gibbs}
Benché facendo tendere la funzione sinc a una delta si eliminano le oscillazioni:
\[
\lim_{T \to + \infty} \left[ T \mathrm{sinc} \left( f T \right) \right] = \delta \left( f \right)
\]
la seguente relazione:
\[ \lim_{T \to + \infty} X_T \left( f \right) = X \left( f \right) \]
continua a non valere se il segnale $x \left( t \right)$ presenta delle discontinuità nel dominio delle frequenze.

\paragraph{Esempio}
\[
\begin{cases} x \left( t \right) = B \mathrm{sinc} \left( B t \right) \\
X \left( f \right) = \Pi_B \left( f \right) \end{cases}
\]

Facendo tendere $T$ a infinito si possono restringere le oscillazioni secondarie fino a rette verticali, come nel segnale di partenza, ma i massimi e i minimi, rispettivamente $1,09$ e $-0,09$, non cambiano e rimangono diversi da quelli del segnale di partenza, rispettivamente 1 e 0:
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.55]{pic/08/Fenomeno_di_Gibbs.png}
\end{figure}

\section{Spettri di energia e di potenza e funzione di autocorrelazione}
\subsection{Segnali a energia finita}
\subsubsection{Spettro di energia}
Lo \textbf{spettro di energia} $S_x \left( f \right)$ di un segnale dà informazioni sul contenuto di energia alle singole frequenze:
\[ S_x \left( f \right) \triangleq {\left| X \left( f \right) \right|}^2  = X \left( f \right) X^* \left( f \right) \Rightarrow E \left( x \right) = \int S_x \left( f \right) d f \]

Per un segnale all'uscita di un sistema LTI:
\[ Y \left( f \right) = H \left( f \right) X \left( f \right) \Rightarrow  S_y \left( f \right) = {\left| H \left( f \right) \right|}^2 S_x \left( f \right) \Rightarrow E \left( y \right) = \int S_y \left( f \right) df \]

\subsubsection{Funzione di autocorrelazione}
La \textbf{funzione di autocorrelazione} $R_x \left( \tau \right)$ è definita:
\[
R_x \left( \tau \right) \triangleq {\mathcal{F}}^{-1} \left\{ S_x \left( f \right) \right\} = x \left( \tau \right) * x^* \left( - \tau \right) = \int_{- \infty}^{+ \infty} x \left( t + \tau \right) x^* \left( t \right) dt = \langle x \left( t + \tau \right) , x \left( t \right) \rangle
\]

Nell'origine:
\[
R_x \left( 0 \right) = \int_{- \infty}^{+ \infty} x \left( t \right) x^* \left( t \right) dt = E \left( x \right) \in {\mathbb{R}}^+
\]

\paragraph{Proprietà}
\begin{itemize}
\item Per la funzione di autocorrelazione vale la simmetria hermitiana:
\[
R_x \left( \tau \right) = R_x^* \left( - \tau \right)
\]

Se il segnale $x \left( t \right)$ è reale, la funzione di autocorrelazione è pari:
\[
R_x \left( \tau \right) = R_x \left ( - \tau \right)
\]

\item Per la disuguaglianza di Schwarz, la funzione di autocorrelazione ha un massimo nell'origine:
\[ {\left| R_x \left( \tau \right) \right|}^2 = {\left| \int x \left( t + \tau \right) x^* \left( t \right) d t \right|}^2 \leq E^2 \left( x \right) = R_x^2 \left( 0 \right) \]
\end{itemize}

\subsubsection{Mutua correlazione}
Considerando una coppia di segnali $x \left( t \right)$ e $y \left( t \right)$, si definiscono \textbf{funzione di mutua correlazione} $R_{xy} \left( \tau \right)$:
\[
R_{xy} \left( \tau \right) \triangleq x \left( \tau \right) * y \left( - \tau \right) = \int x \left( t + \tau \right) y^* \left( t \right) dt = R_{yx}^* \left( \tau \right)
\]
e \textbf{spettro di energia mutua} $S_{xy} \left( f \right)$:
\[
S_{xy} \left( f \right) \triangleq \mathcal{F} \left( R_{xy} \left( \tau \right) \right) = X \left( f \right) Y^* \left( f \right) = S_{yx}^* \left( f \right)
\]

Considerando la somma $z \left( t \right)$ di questi due segnali:
\[
{\left| Z \left( f \right) \right|}^2 = {\left| X \left( f \right) \right|}^2 + {\left| Y \left( f \right) \right|}^2 + 2 \Re{\left\{ X \left( f \right) Y^* \left( f \right) \right\}} \Rightarrow \begin{cases} S_z \left( f \right) = S_x \left( f \right) + S_y \left( f \right) + 2 \Re{\left\{ S_{xy} \left( f \right) \right\}} \\ R_z \left( \tau \right) = R_x \left( \tau \right) + R_y \left( \tau \right) + 2 \Re{\left\{ R_{xy} \left( \tau \right) \right\}} \end{cases}
\]
\begin{framed}
\paragraph{Dimostrazione}
\[ R_z \left( \tau \right) = \int_{- \infty}^{+\infty} z \left( t + \tau \right) z^* \left( t \right) dt = \int_{- \infty}^{+ \infty} \left[ x \left( t + \tau \right) + y \left( t + \tau \right) \right] \left[ x^* \left( t \right) + y^* \left( t \right) \right] dt = \]
\[ = R_x \left( \tau \right) + R_y \left( \tau \right) + R_{xy} \left( \tau \right) + R_{yx} \left( \tau \right) = R_x \left( \tau \right) + R_y \left( \tau \right) + 2 \Re{\left\{ R_{xy} \left( \tau \right) \right\}} \]
\end{framed}

\subsection{Segnali periodici}
Ricordando le formule della potenza\footnote{Si veda la sezione~\ref{sez:periodicita}.} e dell'energia\footnote{Si veda la sezione~\ref{sez:serie_di_fourier}.}, la potenza media di un segnale periodico $x \left( t \right)$ è finita:
\[ \begin{cases} \displaystyle  P \left( x \right) = \frac{1}{T} E \left( x_T \right) \\
\displaystyle E \left( x_T \right) = T \sideset{}{_i}\sum {\left| \mu_i \right|}^2 \end{cases} \Rightarrow P \left( x \right) = \sideset{}{_i}\sum {\left| \mu_i \right|}^2 \in \mathbb{R} \]

\subsubsection{Spettro di potenza}
Lo \textbf{spettro di potenza} $G_x \left( f \right)$ di un segnale periodico $x \left( t \right)$ vale:
\[
G_x \left( f \right) \triangleq \sideset{}{_i}\sum {\left| \mu_i \right|}^2 \delta \left( f - \frac{i}{T} \right)
\]

La formula dello spettro di potenza $G_x \left( f \right)$ ricorda quella della trasformata di Fourier\footnote{Si veda la sezione~\ref{sez:trasformata_di_fourier_di_un_segnale_periodico}.} di $x \left( t \right)$:
\[ X \left( f \right) = \sum_{i = - \infty}^{+ \infty} {\mu}_i \delta \left( f - \frac{i}{T} \right) \]

\subsubsection{Funzione di autocorrelazione}
\label{sez:funzione_di_autocorrelazione}
La funzione di autocorrelazione $R_x \left( \tau \right)$ di un segnale periodico $x \left( t \right)$ vale:
\[
R_x \left( \tau \right) \triangleq \frac{1}{T} \int_{- \frac{T}{2}}^{\frac{T}{2}} x \left( t + \tau \right) x^* \left( t \right) dt
\]

Si noti che la formula della funzione di autocorrelazione per un segnale periodico è leggermente diversa da quella per i segnali non periodici definita sopra.

\subsection{Segnali aperiodici a potenza finita}
Tutti i segnali periodici hanno potenza finita, ma non tutti i segnali a potenza finita sono periodici.

\subsubsection{Spettro di energia}
Per segnali non periodici ma a potenza finita si definisce \textbf{periodogramma} lo spettro di energia del segnale troncato\footnote{Si veda la sezione~\ref{sez:segnali_troncati}.} $x_T \left( t \right)$ (normalizzato):
\[
S_T \left( f \right) \triangleq \frac{1}{T} {\left| X_T \left( f \right) \right|}^2
\]
dove $T$ è un intervallo a piacere.

\subsubsection{Spettro di potenza}
Lo spettro di potenza $G_x \left( f \right)$ è definito:
\[
G_x \left( f \right) \triangleq \lim_{T \to + \infty} S_T \left( f \right) = \lim_{T \rightarrow + \infty} \frac{1}{T} {\left| X_T \left( f \right) \right|}^2
\]

Anche in questo caso la formula dello spettro di potenza per segnali aperiodici è differente da quella per segnali periodici.

Per un segnale all'uscita di un sistema LTI, lo spettro di potenza è pari a:
\[
G_y \left( f \right) = G_x \left( f \right) {\left| H \left( f \right) \right|}^2
\]

\subsubsection{Funzione di autocorrelazione}
\label{sez:funzione_di_autocorrelazione_3}
La funzione di autocorrelazione $\Phi_x \left( \tau \right)$ è definita:
\[
\Phi_x \left( \tau \right) \triangleq {\mathcal{F}}^{-1} \left\{ G_x \left( f \right) \right\} = \lim_{T \to + \infty} \frac{1}{T} \int_{- \frac{T}{2}}^{\frac{T}{2}} x \left( t + \tau \right) x^* \left( t \right) dt
\]

L'integrale $\int_{- \frac{T}{2}}^{\frac{T}{2}} x \left( t + \tau \right) x^* \left( t \right) dt$ cresce linearmente con $T$, quindi questa crescita è compensata da $\tfrac{1}{T}$.