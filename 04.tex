\chapter{Sistemi lineari}
\label{cap4}
Un \textbf{sistema} è un blocco che trasforma un segnale $x \left( t \right)$ in un altro segnale $y \left( t \right)$:
\[
y \left( t \right) = \mathcal{T} \left\{ x \left( t \right) \right\}
\]

È una relazione deterministica: a un certo segnale $x \left( t \right)$ corrisponde sempre il segnale $y \left( t \right)$.

\section{Classificazione}
\subsection{Sistemi lineari}
Un sistema è \textbf{lineare} se vale il principio di sovrapposizione degli effetti:
\[
\mathcal{T} \left\{ a_1 x_1 \left( t \right) + a_2 x_2 \left( t \right) \right\} = a_1 \mathcal{T} \left\{ x_1 \left( t \right) \right\} + a_2 \mathcal{T} \left\{ x_2 \left( t \right) \right\}
\]

La trasformata di Fourier è un sistema lineare.

\subsection{Sistemi tempo-invarianti}
Un sistema è \textbf{tempo-invariante} se un ritardo sugli ingressi si traduce in un ritardo sulle uscite:
\[
\mathcal{T} \left\{ x \left( t - \theta \right) \right\} = y \left( t - \theta \right)
\]

\subsection{Sistemi causali}
Un sistema è \textbf{causale} se l'uscita in un certo istante $t_0$ non dipende dagli ingressi negli istanti successivi, ma dipende solo dagli ingressi negli istanti precedenti e nell'istante corrente:
\[
y \left( t_0 \right) = \mathcal{T} \left\{ \left. x \left( t \right) \right \vert_{- \infty}^{t_0} \right\} \quad \forall t_0
\]

\subsection{Sistemi senza memoria}
Un sistema è \textbf{senza memoria} se l'uscita $y \left( t \right)$ dipende solo dal valore assunto dall'ingresso $x \left( t \right)$ nel dato istante di tempo $t_0$:
\[
y \left( t_0 \right)  = \mathcal{T} \left\{ x \left( t_0 \right) \right\} \quad \forall t_0
\]

\subsection{Sistemi reali}
Un sistema è \textbf{reale} se a un ingresso reale corrisponde un'uscita reale.

Un sistema è \textbf{fisicamente realizzabile} se è causale e reale.

\subsection{Sistemi stabili}
Un sistema è \textbf{stabile} se a un ingresso limitato in ampiezza corrisponde un'uscita limitata in ampiezza, e per questo motivo viene detto Bounded Input Bounded Output (BIBO):
\[
\forall x \left( t \right) : \, \left| x \left( t \right) \right| \in \mathbb{R} , \, \forall t \Rightarrow \left| y \left( t \right) \right| \in \mathbb{R} , \, \forall t
\]

\section{Sistemi lineari tempo-invarianti (LTI)}
\label{sez:sistemi_lineari_tempo_invarianti_LTI}
I \textbf{sistemi lineari tempo-invarianti} (LTI) possono essere descritti da un'unica funzione $h \left( t \right)$ che è la risposta all'impulso:
\[
y \left( t \right) = \mathcal{T} \left\{ x \left( t \right) \right\} = x \left( t \right) * h \left( t \right)
\]
dove:
\[
h \left( t \right) \triangleq \mathcal{T} \left\{ \delta \left( t \right) \right\}
\]
\begin{framed}
\paragraph{Dimostrazione}
Ogni segnale è ottenibile come combinazione lineare dell'insieme di delta:
\[
x \left( t \right) = \int x \left( u \right) \delta \left( t - u \right) du
\]

Perciò:
\[ y \left( t \right) = \mathcal{T} \left\{ x \left( t \right) \right\} = \mathcal{T} \left\{ \int x \left( u \right) \delta \left( t - u \right) du \right\} = \]

Poiché il sistema $\mathcal{T}$ è lineare:
\[
= \int x \left( u \right) \mathcal{L} \left\{ \delta \left( t - u \right) \right\} du =
\]

Poiché il sistema $\mathcal{T}$ è tempo-invariante:
\[
= \int x \left( u \right) h \left( t - u \right) du = x \left( t \right) * h \left( t \right)
\]
\end{framed}

La trasformata di Fourier dell'uscita di un sistema LTI vale:
\[
Y \left( f \right) = \mathcal{F} \left\{ \mathcal{T} \left\{ x \left( t \right) \right\} \right\} = H \left( f \right) X \left( f \right)
\]
dove $H \left( f \right)$ è detta \textbf{funzione di trasferimento}:
\[
H \left( f \right) = \frac{Y \left( f \right)}{X \left( f \right)}
\]

I sistemi LTI non alterano la frequenza di un segnale sinusoidale posto all'ingresso, ma solo la fase e l'ampiezza $\Rightarrow$ le sinusoidi sono autofunzioni di un sistema LTI: l'uscita di una sinusoide è la sinusoide stessa moltiplicata per una costante (complessa $\Rightarrow$ modulo e fase).
\begin{framed}
\paragraph{Dimostrazione}
Considerando la sinusoide complessa $x \left( t \right) = e^{j 2 \pi f_0 t}$:
\[
Y \left( f \right) = X \left( f \right) H \left( f \right) = \delta \left( f - f_0 \right) H \left( f \right) =
\]

Per la proprietà della delta di Dirac:
\[
= \delta \left( f - f_0 \right) H \left( f_0 \right)
\]

Antitrasformando:
\[ y \left( t \right) = H \left( f_0 \right) e^{j 2 \pi f_0 t} = x \left( t \right) H \left( f_0 \right) \]

Se $h \left( t \right)$ è una funzione reale, vale la simmetria hermitiana:
\[
H \left(  - f \right) = H^* \left(  - f \right)
\]

%http://tex.stackexchange.com/a/159576
\vspace*{\dimexpr-\parskip-\baselineskip}
\noindent\rule{\linewidth}{0.2mm}

Se $x \left( t \right) = \cos{\left( 2 \pi f_0 t \right)}$:
\[
Y \left( f \right) = X \left( f \right) H \left( f \right) = \frac{1}{2} \left[ \delta \left( f - f_0 \right) + \delta \left( f + f_0 \right) \right] H \left( f \right) =
\]
\[
=\frac{1}{2} \left[ \delta \left( f - f_0 \right) H \left( f_0 \right) + \delta \left( f + f_0 \right) H \left( - f_0 \right) \right]
\]
\[
y \left( t \right) = \frac{1}{2} \left[ H \left( f_0 \right) e^{j 2 \pi f_0 t} + H \left( - f_0 \right) e^{- j 2 \pi f_0 t} \right] =
\]

Se $h \left( t \right) \in \mathbb{R}$:
\[
= \Re{\left\{ H \left( f_0 \right) e^{j2 \pi f_0 t} \right\}} = \left| H \left( f_0 \right) \right| \cos{\left[ 2 \pi f_0 t + \arg{X \left( f_0 \right)} \right]}
\]

%http://tex.stackexchange.com/a/159576
\vspace*{\dimexpr-\parskip-\baselineskip}
\noindent\rule{\linewidth}{0.2mm}

Se $x \left( t \right) = \sin{\left( 2 \pi f_0 t \right)}$:
\[
Y \left( f \right) = X \left( f \right) H \left( f \right) = \frac{1}{2j} \left[ \delta \left( f - f_0 \right) - \delta \left( f + f_0 \right) \right] H \left( f \right) =
\]
\[
=\frac{1}{2j} \left[ \delta \left( f - f_0 \right) H \left( f_0 \right) - \delta \left( f + f_0 \right) H \left( - f_0 \right) \right]
\]
\[
y \left( t \right) = \frac{1}{2j} \left[ H \left( f_0 \right) e^{j 2 \pi f_0 t} - H \left( - f_0 \right) e^{- j 2 \pi f_0 t} \right] =
\]

Se $h \left( t \right) \in \mathbb{R}$:
\[
= \Im{\left\{ H \left( f_0 \right) e^{j2 \pi f_0 t} \right\}} = \left| H \left( f_0 \right) \right| \sin{\left[ 2 \pi f_0 t + \arg{X \left( f_0 \right)} \right]}
\]
\end{framed}

\subsection{Sistemi LTI causali}
La risposta all'impulso $h \left( t \right)$ di un sistema LTI causale è nulla per $t < 0$:
\[
h \left( t \right) = u \left( t \right) h \left( t \right)
\]
\begin{framed}
\paragraph{Dimostrazione}
\[ y \left( t \right) = x \left( t \right) * h \left( t \right) = \int_{- \infty}^{+ \infty} y \left( \tau \right) h \left( t - \tau \right) d \tau = \]

Siccome il sistema è causale:
\[
= \int_{- \infty}^{t} y \left( \tau \right) h \left( t - \tau \right) d \tau = \int_{- \infty}^{+ \infty} x \left( \tau \right) u \left( t - \tau \right) h \left( t - \tau \right) d \tau
\]
\end{framed}

\subsection{Sistemi LTI reali}
La risposta all'impulso $h \left( t \right)$ di un sistema LTI reale è reale, e la funzione di trasferimento $H \left( f \right)$ deve avere:
\begin{itemize}
\item parte reale pari;
\item parte immaginaria dispari;
\item modulo pari;
\item fase dispari.
\end{itemize}

\subsection{Sistemi LTI stabili}
Un sistema LTI è stabile se e solo se la risposta all'impulso $h \left( t \right)$ è modulo integrabile:
\[
\text{stabile} \, \Leftrightarrow \int \left| h \left( t \right) \right| dt \in \mathbb{R} \Rightarrow \left| H \left( f \right) \right| \in \mathbb{R}
\]
\begin{framed}
\paragraph{Dimostrazione}
\subparagraph{Condizione sufficiente}
\[
\text{stabile} \, \Leftarrow \int \left| h \left( t \right) \right| dt \in \mathbb{R}
\]
\[
\left| y  \left( t \right) \right| = \left| \int x \left( t - \tau \right) h \left( \tau \right) d \tau \right| \leq \int \left| x \left( x - \tau \right) \right| \left| h \left( \tau \right) \right| d \tau \leq \int \max{\left| x \left( t - \tau \right) \right|} \left| h \left( \tau \right) \right| d \tau =
\]
\[
=\max{\left| x \left( t - \tau \right) \right|} \int \left| h \left( \tau \right) \right| d \tau
\]
\[
\int \left| h \left( \tau \right) \right| d \tau \in \mathbb{R} \Rightarrow \left| y  \left( t \right) \right| \in \mathbb{R}
\]

\subparagraph{Condizione necessaria}
\[
\text{stabile} \, \Rightarrow \int \left| h \left( t \right) \right| dt \in \mathbb{R}
\]

Consideriamo una funzione $x \left( t \right)$:
\[
x \left( t \right) = e^{-j \arg{h \left( - t \right)}}
\]
che è di ampiezza limitata:
\[
\left| x \left( t \right) \right| = 1 \; \forall t
\]

\[ y \left( 0 \right) = \int  \left. x \left( t - \tau \right) \right \vert_{t = 0} h \left( \tau \right) d \tau = \int e^{-j \arg{h \left( \tau \right)}} \left| h \left( \tau \right) \right| e^{j \arg{h \left( \tau \right)}} d \tau = \int \left| h \left( \tau \right) \right| d \tau \]
\end{framed}

\section{Esempi}
\subsection{Filtro RC con una porta di ingresso}
\begin{figure}
	\centering
	\includegraphics[scale=0.45]{pic/04/Low-pass_RC_cell.png}
\end{figure}

\[ v_2 \left( t \right) = R i \left( t \right) + v_1 \left( t \right) = R C \frac{d v_1 \left( t \right)}{dt} + v_1 \left( t \right) \]
\[ V_2 \left( f \right) = \left( 1 + j 2 \pi f RC \right) V_1 \left( f \right) \Rightarrow H \left( f \right) = \frac{1}{1 + j 2 \pi f RC} \]
\FloatBarrier

\subsection{Canale radio con eco}
Un segnale radio quando viene trasmesso si propaga in tutte le direzioni, e viene riflesso dagli oggetti fisici dell'ambiente, detti scatterer. Gli echi arrivano perciò al ricevitore ognuno con un certo ritardo ${\tau}_i$ e una certa amplificazione/attenuazione ${\alpha}_i$. Trasmettendo un impulso $\delta$, viene ricevuto un segnale con eco $h \left( t \right)$:
\[
h \left( t \right) = \sum_{i = 1}^P {\alpha}_i \delta \left( t -  {\tau}_i \right)
\]
dove $P$ è il numero di segnali eco che arrivano al ricevitore.

La sua trasformata $H \left( f \right)$ vale:
\[
H \left( f \right) = \sum_{i = 1}^P {\alpha}_i e^{-j2 \pi f {\tau}_i}
\]

Si dimostra che il suo modulo al quadrato vale:
\[
{\left| H \left( f \right) \right|}^2 = \sum_{i  =1}^P {\left| {\alpha}_i \right|}^2 + \sum_{i = 1}^P \sum_{\overset{\scriptstyle k=1} {\scriptstyle k>1}}^P 2 {\alpha}_i {\alpha}_k \cos{\left( 2 \pi f \Delta {\tau}_{ik}\right)}
\]

Se $P = 1$, il canale non è selettivo in frequenza, cioè tutte le sinusoidi vengono moltiplicate per la stessa ampiezza:
\[
\left| H \left( f \right) \right| = \left| {\alpha}_1 \right|
\]

Se $P = 2$ si introduce la selettività in frequenza:
\[
{\left| H \left( f \right) \right|}^2 = {\left| {\alpha}_1 \right|}^2 + {\left| {\alpha}_2 \right|}^2 + 2 {\alpha}_1 {\alpha}_2 \cos{\left( 2 \pi f \Delta {\tau}_{12}\right)}
\]
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{pic/04/Segnale_radio_con_eco.png}
\end{figure}