\chapter{Segnali a tempo discreto}
L'\textbf{elaborazione numerica dei segnali} (ENS) è l'applicazione di un algoritmo ad una serie di numeri che rappresenta un segnale.

Un segnale $x \left( n T_c \right)$ è \textbf{a tempo discreto} se è definito rispetto a una variabile indipendente $n$ che assume solo valori interi ($n \in \mathbb{Z}$). Per semplicità si parla di $x \left( n T_c \right)$ come la \textbf{sequenza} $x \left( n \right)$. Il segnale è detto \textbf{numerico} o \textbf{digitale} se assume solo ampiezze discrete.

\section{Classificazione}
\subsection{Durata di una sequenza}
Una sequenza può avere:
\begin{itemize}
\item \textbf{durata finita:} la sequenza è identicamente nulla all'esterno di un intervallo finito di tempo $\left[ n_1 , n_2 \right]$;
\item \textbf{durata infinita:} il supporto temporale può essere bilatero ($\left( - \infty , + \infty \right)$) o monolatero ($\left[ n_1 , + \infty \right)$ o $\left( - \infty, n_2 \right)$).
\end{itemize}

\subsection{Causalità}
Una sequenza è:
\begin{itemize}
\item \textbf{casuale} se è identicamente nulla per valori di $n$ minori di 0;
\item \textbf{anticasuale} se è identicamente nulla per valori di $n$ maggiori o uguali di 0.
\end{itemize}

\subsection{Parità}
Una sequenza $x \left( n \right)$ reale è detta:
\begin{itemize}
\item \textbf{pari} se $x \left( n \right) = x \left( - n \right)$;
\item \textbf{dispari} se $x \left( n \right) = - x \left( - n \right)$.
\end{itemize}

Una sequenza $x \left( n \right)$ complessa è detta:
\begin{itemize}
\item \textbf{coniugata simmetrica} se $x \left( n \right) = x^* \left( - n \right)$;
\item \textbf{coniugata antisimmetrica} se $x \left( n \right) = - x^* \left( - n \right)$.
\end{itemize}

Una qualunque sequenza complessa $x \left( n \right)$ può essere scritta come somma di una sequenza coniugata simmetrica $x_p \left( n \right)$ e di una sequenza coniugata antisimmetrica $x_d \left( n \right)$:
\[
x \left( n \right) = x_p \left( n \right) + x_d \left( n \right)
\]
dove:
\[
\begin{cases} \displaystyle x_p \left( n \right) = \frac{1}{2} x \left( n \right) + {1 \over 2} x^* \left( - n \right) = x_p^* \left( -n \right) \\
\displaystyle x_d \left( n \right) = {1 \over 2} x \left( n \right) - {1 \over 2} x^* \left( - n \right) = -x_d^* \left( - n \right) \end{cases}
\]
\begin{framed}
\paragraph{Dimostrazione}
\[
x \left( n \right) = {1 \over 2} x \left( n \right) + {1 \over 2} x \left( n \right) = \underbrace{{1 \over 2} x \left( n \right) + {1 \over 2} x^* \left( - n \right)}_{x_p \left( n \right)} + \underbrace{{1 \over 2} x \left( n \right) - {1 \over 2} x^* \left( - n \right)}_{x_d \left( n \right)}
\]
\end{framed}

\subsection{Periodicità}
Una sequenza $x \left( n \right)$ è \textbf{periodica} se è possibile trovare un intervallo di tempo $N$ per cui vale la relazione:
\[
x \left( n \right)  = x \left( n \pm N \right) \quad N \in \mathbb{N}
\]

Il \textbf{periodo} è il più piccolo valore intero positivo di $N$ per cui la sequenza è periodica.

\subsection{Sequenze limitate in ampiezza}
Una sequenza $x \left( n \right)$ è \textbf{limitata} se per qualunque istante di tempo discreto $n$ assume valori contenuti entro un intervallo finito $X_0$ (costante reale finita positiva):
\[ \left| x \left( n \right) \right| \leq X_0 \quad \forall n \]

\subsection{Sequenze sommabili}
Una sequenza $x \left( n \right)$ è \textbf{assolutamente sommabile} se:
\[
\sum_{n = - \infty}^{+ \infty} \left| x \left( n \right) \right| \in \mathbb{R}
\]

Una sequenza $x \left( n \right)$ è \textbf{quadraticamente sommabile} se:
\[
\sum_{n = - \infty}^{+ \infty} {\left| x \left( n \right) \right|}^2 \in \mathbb{R}
\]

\section{Sequenze elementari}
\subsection{Sequenza gradino unitario}
\begin{figure}
	\centering
	\includegraphics[scale=0.409]{pic/B01/Sequenza_gradino_unitario}
\end{figure}

\[ u \left( n \right) = \begin{cases} 0 , & n < 0 \\
1 , & n \geq 0 \end{cases} \]
\FloatBarrier

\subsection{Delta di Kronecker (impulso unitario)}
\begin{figure}
	\centering
	\includegraphics[scale=0.409]{pic/B01/Delta_di_Kroenecher}
\end{figure}

\[ \delta \left( n \right) = \begin{cases} 0, & n \neq 0 \\
1 , & n = 0 \end{cases} \]

Qualsiasi segnale $x \left( n \right)$ può essere espresso come somma di impulsi:
\[
x \left( n \right) = \sum_{i = - \infty}^{+ \infty} x \left ( i \right) \delta \left( n - i \right)
\]

\paragraph{Relazione tra delta numerica e gradino unitario}
\[ u \left( n \right) = \sum_{i = 0}^{+ \infty} \delta \left( n - i \right) = \delta \left( n \right) + \delta \left( n - 1 \right) + \delta \left( n - 2 \right) + \ldots \]
\[ \delta \left( n \right) = u \left( n \right) - u \left( n - 1 \right) \]
\FloatBarrier

\subsection{Sequenza rampa}
\begin{figure}
	\centering
	\includegraphics[scale=0.409]{pic/B01/Sequenza_rampa}
\end{figure}

\[ r \left( n \right) = n u \left( n \right) = \begin{cases} 0 , & n < 0 \\
n , & n \geq 0 \end{cases} \]
\FloatBarrier

\subsection{Sequenza sinc}
\begin{figure}
	\centering
	\includegraphics[scale=0.409]{pic/B01/Sequenza_sinc}
\end{figure}

\[ \mathrm{sinc} \left( \frac{n}{N} \right) = \frac{\sin{\left( \pi \frac{n}{N} \right)}}{\pi \frac{n}{N}} , \quad N \in \mathbb{N} \]

Interseca l'asse orizzontale in $N$, $2N$, ecc.

Se $N = 1$, la sequenza $sinc \left( n \right)$ coincide con la delta di Kronecker.
\FloatBarrier

\subsection{Sequenza triangolo}
\begin{figure}
	\centering
	\includegraphics[scale=0.409]{pic/B01/Sequenza_triangolo}
\end{figure}

\[ t_{2N + 1} \left( n \right) = \begin{cases} 1 - \frac{\left| n \right|}{N} , & \left| n \right| \leq N \\
0 , & \left| n \right| > N \end{cases} , \quad N \in \mathbb{N} \]
\FloatBarrier

\subsection{Sequenza esponenziale}
\begin{figure}
	\centering
	\includegraphics[scale=0.409]{pic/B01/Sequenza_esponenziale}
\end{figure}

\[ x \left( n \right) = a^n u \left( n \right) \]

Se $a$ è complesso:
\[
a = A e^{j \theta} \Rightarrow x \left( n \right) = A^n e^{j n \theta} u \left( n \right)
\]
\FloatBarrier

\section{Sinusoidi a tempo discreto}
\paragraph{Proprietà 1}
Sinusoidi che differiscono per un numero intero di angoli giro sono indistinguibili nel dominio del tempo discreto:
\[
A \cos{\left( 2 \pi f_0 n + 2 \pi k n + \theta \right)} = A \cos{\left( 2 \pi f_0 n  + \theta \right)} \quad k \in \mathbb{Z}
\]

\paragraph{Proprietà 2}
La frequenza delle oscillazioni di una sinusoide a tempo discreto:
\begin{itemize}
\item $0 < f_0 < \tfrac{1}{2}$: aumenta all'aumentare di $f_0$;
\item $\tfrac{1}{2} < f_0 < 1$: diminuisce all'aumentare di $f_0$.
\end{itemize}

\paragraph{Proprietà 3}
Una sinusoide è periodica se il prodotto $N f_0$ è un numero intero:
\[
x \left( n + N \right)  = x \left( n \right) \Rightarrow A \cos{\left( 2 \pi f_0 n  + 2 \pi f_0 N + \theta \right)} = \cos{\left( 2 \pi f_0 n + \theta \right)} \quad N \in \mathbb{Z}
\]

Una sinusoide discreta perciò non necessariamente è periodica di periodo $\tfrac{1}{f_0}$. Se $f_0$ non è un numero razionale, la sinusoide non è periodica ($N$ dev'essere intero).

\section{Operazioni elementari}
\subsection{Somma e prodotto}
Le operazioni di somma e prodotto si applicano tra coppie di campioni osservati nei medesimi istanti di tempo.

\subsection{Traslazione e ribaltamento}
\paragraph{Traslazione}
La traslazione consiste nel campio di variabile $n \to n - N$, dove $N \in \mathbb{N}$ è pari al numero di campioni per cui il segnale è ritardato o anticipato:
\[
y \left( n \right) = x \left( n - N \right)
\]

\paragraph{Ribaltamento}
Il ribaltamento consiste nel cambio di variabile $n \to -n$ e realizza l'inversione dell'asse dei tempi:
\[
y \left( n \right) = x \left( - n \right)
\]

L'operazione di traslazione ha la precedenza su quella di ribaltamento:
\[
x \left( n \right) \to x \left( n - N \right) \to x \left( -n -N \right)
\]

\subsection{Scalamento temporale}
\paragraph{Sottocampionamento}
L'operazione di sottocampionamento corrisponde a costruire la sequenza $y \left( n \right)$ prendendo un campione ogni $D$ della sequenza $x \left( n \right)$:
\[
y \left( n \right) = D x \left( n \right) \quad D \in \mathbb{N}
\]

Corrisponde all'operazione di compressione nel dominio del tempo continuo. La funzione Matlab è \texttt{downsample}.

\paragraph{Sovracampionamento}
L'operazione di sovracampionamento corrisponde a costruire la sequenza $y \left( n \right)$ inserendo $I - 1$ zeri tra ogni campione della sequenza $x \left( n \right)$:
\[
y \left( n \right)  = \begin{cases} \displaystyle x \left( \frac{n}{I} \right) & \forall n = \ldots , - 2 I, - I , 0 , + I, +2I , \ldots \\
0 & \text{altrimenti} \end{cases}
\]

Corrisponde all'operazione di dilatazione nel dominio del tempo continuo. La funzione Matlab è \texttt{upsample}.

\subsection{Convoluzione lineare}
La \textbf{convoluzione lineare} tra due sequenze discrete $x \left( n \right)$ e $y \left( n \right)$ è definita:
\[
x \left( n \right) * y \left( n \right) = \sum_{k = - \infty}^{+ \infty} x \left( k \right)  y \left( n - k \right)
\]

\paragraph{Proprietà}
Il supporto della convoluzione è pari alla somma dei singoli supporti meno 1.

\begin{itemize}
\item commutativa:
\[
x \left( n \right) * y \left( n \right) = y \left( n \right) * x \left( n \right)
\]
\item distributiva:
\[
x \left( n \right) * \left[ y \left( n \right) + z \left( n \right) \right] = x \left( n \right) * y \left( n \right) + x \left( n \right) * z \left( n \right)
\]
\item associativa:
\[
x \left( n \right) * \left[ y \left( n \right) * z \left( n \right) \right] = \left[ x \left( n \right) * y \left( n \right) \right] * z \left( n \right)
\]
\end{itemize}

La funzione Matlab è \texttt{conv}.

\section{Energia e potenza media}
\subsection{Energia}
\[
E_x = \sum_{n = - \infty}^{+ \infty} {\left| x \left( n \right) \right|}^2
\]

Per sequenze a energia finita, l'energia non dipende da traslazioni temporali di $x \left( n \right)$:
\[
E_x = \sum_{n = - \infty}^{+ \infty} {\left| x \left( n \right) \right |}^2 = \sum_{n = - \infty}^{+ \infty} {\left| x \left( n - N \right) \right |}^2 \quad \forall N \in \mathbb{Z}
\]

L'energia di un segnale analogico $x \left( t  \right)$ è approssimabile alla sua sequenza $x \left( n T_c \right)$ campionata a intervalli $T_c$ molto piccoli:
\[
E_x = \int_{- \infty}^{+ \infty} {\left| x \left( t \right) \right|}^2 dt \approx T_c \sum_{n = - \infty}^{+ \infty} {\left| x \left( n T_c \right) \right|}^2
\]

\subsection{Potenza media}
Per sequenze a energia infinita è possibile definire la potenza media:
\[
P_x = \lim_{N \to \infty} \frac{1}{2N + 1} \sum_{n = - N}^{+N} {\left| x \left( n \right) \right|}^2
\]

\begin{itemize}
\item Le sequenze a energia finita hanno potenza media nulla.
\item Le sequenze a potenza media finita (e non nulla) hanno energia infinita.
\end{itemize}

\paragraph{Esempio}
La sequenza gradino unitario $u \left( n \right)$ ha energia infinita ma potenza media finita:
\[
E_x = \sum_{n = - \infty}^{+ \infty} {\left| u \left( n \right) \right|}^2 = \sum_{n = 0}^{ + \infty} 1 \to + \infty
\]
\[
P_x = \lim_{N \to + \infty} \frac{1}{2N + 1} \sum_{n = - N}^{+ N} {\left| u \left( n \right) \right|}^2 = \lim_{N \to + \infty} \frac{1}{2N+1} \sum_{n = 0}^{+N} 1 = \lim_{N \to + \infty} \frac{N + 1}{2N+1} = \frac{1}{2}
\]

La potenza media di un segnale periodico è pari alla potenza media calcolata in un suo periodo. La potenza media $P_x$ di un segnale periodico dipende dall'energia del segnale all'interno di un singolo periodo:
\[
P_x = \frac{1}{N} \sum_{n = 0}^{N - 1} {\left| x \left( n \right) \right|}^2
\]

La potenza di un segnale analogico $x \left( t \right)$ è approssimabile alla sua sequenza $x \left( n T_c \right)$ campionata a intervalli $T_c$ molto piccoli:
\[
P_x = \lim_{T \to + \infty} \frac{1}{2T} \int_{-T}^T {\left| x \left( t \right) \right|}^2 dt \cong \lim_{N \to + \infty} \frac{1}{\left( 2N+1 \right) \cancel{T_c}} \sum_{n = -N}^{+N} {\left| x \left( n T_c \right) \right|}^2 \cancel{T_c}
\]

Inoltre, se il segnale è periodico:
\[
P_x = \frac{1}{T} \int_0^T {\left| x \left( t \right) \right|}^2 dt \cong \frac{1}{N \cancel{T_c}} \sum_{n = 0}^{N - 1} {\left| x \left( n T_c \right) \right|}^2 \cancel{T_c}
\]

\section{Funzioni di correlazione}
\afterpage{
\begin{landscape}
\begin{table}[p]
 \centering
 \begin{tabular}{|c|c|c|}
  \cline{2-3}
  \multicolumn{1}{c|}{} & \textbf{Mutua correlazione} & \textbf{Autocorrelazione} \\
  \cline{2-3}
  \multicolumn{1}{c|}{} & $\displaystyle R_{xy} \left( n \right) = \sum_{k = - \infty}^{+ \infty} x^* \left( k + n \right) y \left( k \right)$ & $\displaystyle R_x \left( n \right) = \sum_{k = - \infty}^{+ \infty} x^* \left( k + n \right) x \left( k \right)$ \\
  \hline
  \textbf{Sequenze a potenza finita} & $\displaystyle \Phi_{xy} \left( n \right) = \lim_{N \to + \infty} \frac{1}{2N+1} \sum_{k = - N}^{+N} x^* \left( k + n \right) y \left( k \right)$ & $\displaystyle \Phi_x \left( n \right) = \lim_{N \to + \infty} \frac{1}{2N + 1} \sum_{k = -N}^{+N} x^* \left( k+ n \right) x \left( k \right)$ \\
  \hline
  \textbf{Sequenze periodiche} & $\displaystyle \Phi_{xy} \left( n \right) = \frac{1}{N} \sum_{k = 0}^{N -1 } x^* \left( k+ n \right) y \left(k \right)$ & $\displaystyle \Phi_x \left( n \right) = \frac{1}{N} \sum_{k = 0}^{N -1} x^* \left( k + n \right) x \left( k \right)$ \\
  \hline
  \textbf{Proprietà} & \begin{tabular}[c]{@{}c@{}}se la sequenza è reale:\\$\displaystyle R_{xy} \left( n \right) = R_{yn} \left( - n \right)$\end{tabular} & $\displaystyle R_x \left( 0 \right) = \sum_{k = - \infty}^{+ \infty} {\left| x \left( k \right) \right|}^2 = E_x$ \\
  \hline
 \end{tabular}
 \caption{Funzioni di correlazione.}
\end{table}
\end{landscape}
}

\subsection{Esempio: segnale radar}
\afterpage{
\begin{landscape}
\begin{figure}[p]
\centering
	\begin{subfigure}[b]{.35\linewidth}
		\centering
		\includegraphics[scale=0.409]{pic/B01/Segnale_radar_1}
		\caption{$x \left( n \right)$}
	\end{subfigure}%
	\begin{subfigure}[b]{.35\linewidth}
		\centering
		\includegraphics[scale=0.409]{pic/B01/Segnale_radar_2}
		\caption{$r \left( n \right)$}
	\end{subfigure}%
	\begin{subfigure}[b]{.35\linewidth}
		\centering
		\includegraphics[scale=0.409]{pic/B01/Segnale_radar_3}
		\caption{$z \left( n \right)$}
	\end{subfigure}
	\caption{Esempio: segnale radar.}
\end{figure}
\end{landscape}
}

La funzione di mutua correlazione può essere usata per ricavare informazioni sul grado di similarità tra due sequenze a energia finita.

L'eco $r \left( n \right)$ di un segnale radar $x \left( t \right)$ è del tipo:
\[
r \left( n \right) = \alpha x \left( n - D \right) + g \left( n \right)
\]
\begin{itemize}
\item $\alpha$ è l'attenuazione del segnale;
\item $D$ è il ritardo del segnale;
\item $g \left( n \right)$ è il rumore.
\end{itemize}

La funzione di mutua correlazione $z \left( n \right)$ ha un picco in $n = D$ $\Rightarrow$ sapendo il ritardo è possibile calcolare la distanza dell'oggetto: $d = \tfrac{D}{2} \cdot c$.
