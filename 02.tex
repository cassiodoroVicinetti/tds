\chapter{Serie e trasformata}
\section{Base canonica}
\subsection{Funzione porta}
\begin{figure}
	\centering
	\includegraphics[scale=0.65]{pic/02/Porta_Fourier}
\end{figure}

\noindent
${\Pi}_{\Delta t} \left( t \right)$ è la \textbf{funzione porta} unitaria di supporto $\Delta t$ centrato nell'origine. Un segnale $x \left( t \right)$ può essere approssimato con un segnale $x' \left( t \right)$ costante a tratti ottenuto da una combinazione lineare di infinite porte ortogonali tra di loro (cioè con supporto disgiunto):
\[
x \left( t \right) \approx x ' \left( t \right) = \sum_{n = - \infty}^{+ \infty} x \left( n \Delta t \right) {\Pi}_{\Delta t} \left( t - n \Delta t \right)
\]

Se $\Delta t \rightarrow 0$ l'approssimazione diventa un'identità:
\[
x \left( t \right) = x ' \left( t \right) = \lim_{\Delta t \rightarrow 0} \sum_{n = - \infty}^{+ \infty} x \left( n \Delta t \right) {\Pi}_{\Delta t} \left( t - n \Delta t \right)
\]
\FloatBarrier

\subsection{Seno cardinale}
\begin{figure}
	\centering
	\includegraphics[scale=0.4]{pic/02/Plot_of_Sinc_function}
	\caption[]{\footnotemark}
\end{figure}
\footnotetext{Questa immagine è tratta da Wikimedia Commons (\href{https://commons.wikimedia.org/wiki/File:Sinc_function_(normalized).svg}{Sinc function (normalized).svg}), è stata realizzata dall'utente \href{https://en.wikipedia.org/wiki/User:Omegatron}{Omegatron}, dall'utente \href{https://en.wikipedia.org/wiki/User:Krishnavedala}{Krishnavedala} e dall'utente \href{https://commons.wikimedia.org/wiki/User:Aflafla1}{Aflafla1}, ed è concessa sotto le licenze Creative Commons Attribuzione - Condividi allo stesso modo \href{http://creativecommons.org/licenses/by-sa/3.0/deed.it}{3.0 Unported}, \href{http://creativecommons.org/licenses/by-sa/2.5/deed.it}{2.5 Generico}, \href{http://creativecommons.org/licenses/by-sa/2.0/deed.it}{2.0 Generico} e \href{http://creativecommons.org/licenses/by-sa/1.0/deed.it}{1.0 Generico}.}

\[
\mathrm{sinc} \left( t \right) \triangleq \frac{\sin{\left( \pi t \right)}}{\pi t}
\]
\FloatBarrier

\subsection{Delta di Dirac}
\paragraph{Definizione}
\[
x \left( 0 \right) = \int_{- \infty}^{+ \infty} x \left( \tau \right) \delta \left( \tau \right) d \tau
\]

La \textbf{delta di Dirac} si può costruire come limite di varie funzioni, tra cui:
\[
\delta \left( t \right) = \lim_{\Delta t \to 0} \frac{1}{\Delta t} {\Pi}_{\Delta t} \left( t \right) = \lim_{\Delta t \to 0} \frac{1}{\Delta t} \mathrm{sinc} \left( \frac{t}{\pi \Delta t} \right)
\]
dove:
\[
\mathrm{sinc} \left( \frac{t}{\pi \Delta t} \right) = \frac{\sin{\frac{t}{\Delta t}}}{\frac{t}{\Delta t}}
\]

\paragraph{Proprietà}
\begin{itemize}
\item La delta di Dirac ha area unitaria:
\[
\int_{- \infty}^{+ \infty} \delta \left( \tau \right) d \tau = 1
\]
\item Traslare la delta di Dirac significa trovare tutti i campioni che assume la funzione $x \left( t \right)$:
\[
\int_{- \infty}^{+ \infty} x \left( \tau \right) \delta \left( t - \tau \right) d \tau = x \left( t \right)
\]
\item La delta di Dirac ha energia infinita:
\[
E \left( \delta \right)  \to + \infty
\]
\begin{framed}
\paragraph{Dimostrazione}
\[
 {\delta}^2 \left( t \right) = \lim_{\Delta t \to 0} \frac{1}{{\Delta t}^2} {\Pi}_{\Delta t} \left( t \right)
\]
\[
E \left( \delta \right) = \int {\delta}^2 \left( t \right) dt = \lim_{\Delta t \to 0} \frac{1}{{\Delta t}^2} \int {\Pi}_{\Delta t} \left( t \right) dt = \lim_{\Delta t \to 0} \frac{1}{{\Delta t}^2} \Delta t \to + \infty
\]
\end{framed}

\item La radice della delta di Dirac:
\[
\sqrt{\delta \left( t \right)} = \lim_{\Delta t \to 0} \frac{1}{\sqrt{\Delta t}} {\Pi}_{\Delta t} \left( t \right)
\]
ha energia unitaria:
\[
E \left( \sqrt{\delta} \right) = 1
\]
\begin{framed}
\paragraph{Dimostrazione}
\[
 E \left( \sqrt{\delta} \right) = \int {\sqrt {\delta \left( t \right)}}^2 dt = \int \delta \left( t \right) dt = 1
\]
\end{framed}
\end{itemize}

\subsection{Definizione della base canonica}
L'insieme infinito e non numerabile di delta $\delta \left( t - \tau \right)$ può essere quindi visto come una \textbf{base canonica} non normalizzata per l'insieme dei segnali, cioè un segnale può essere rappresentato come l'insieme dei suoi campioni:
\[
x \left( t \right) = \int_{- \infty}^{+ \infty} x \left( \tau \right) \delta \left( t - \tau \right) d \tau = \lim_{\Delta t \rightarrow 0} \sum_{n = - \infty}^{+ \infty} x \left( n \Delta t \right) \Delta t \cdot \frac{1}{\Delta t} {\Pi}_{\Delta t} \left( t - n \Delta t \right)
\]
dove gli elementi della base (infiniti e non numerabili) sono:
\[
\lim_{\Delta t \rightarrow 0} \frac{1}{\Delta t} {\Pi}_{\Delta t} \left( t - n \Delta t \right)
\]
e i coefficienti (infiniti e non numerabili) sono:
\[
\lim_{\Delta t \rightarrow 0} x \left( n \Delta t \right) \Delta t
\]

È però una base ortogonale non normalizzata perché ha energia infinita. Le radici della delta invece formano una base ortonormale:
\[
x \left( t \right) = \lim_{\Delta t \rightarrow 0} \sum_{n = - \infty}^{+ \infty} x \left( n \Delta t \right) \sqrt{\Delta t} \cdot \frac{1}{\sqrt{\Delta t}} {\Pi}_{\Delta t} \left( t - n \Delta t \right)
\]

\paragraph[Energia]{Energia\footnote{Si assume un segnale $x \left( t \right)$ reale.}}
\[
E \left( x \right) = \int x^2 \left( t \right) dt = \lim_{\Delta \to 0} \sum_{n = - \infty}^{+ \infty} x^2 \left( n \Delta \tau \right) \Delta \tau
\]

\paragraph{Prodotto scalare}
\[
\langle x , y \rangle = \int x \left( t \right) y^* \left( t \right) dt = \lim_{\Delta t \to 0} \sum_{n = - \infty}^{+ \infty} x \left( n \Delta \tau \right) y^* \left( n \Delta \tau \right) \Delta \tau
\]

Tuttavia non conviene usare questa base perché non introduce alcuna semplificazione.

\section{Base alternativa sinusoidale}
\subsection{Serie di Fourier}
\label{sez:serie_di_fourier}
Un segnale è \textbf{a supporto finito} se non è nullo solo nell'intervallo $\left[ - \tfrac{T}{2} , \tfrac{T}{2} \right]$.\footnote{Si assume che il supporto sia simmetrico rispetto all'origine.} La base canonica per questa classe di segnali è l'insieme dei campioni ristretto al supporto:
\[
x \left( t \right) = \int_{- \frac{T}{2}}^{\frac{T}{2}} x \left( \tau \right) \delta \left( t - \tau \right) d \tau
\]

Esiste un'altra base ortonormale completa per tutti i segnali complessi ad energia finita e supporto finito, che è un insieme infinito ma, a differenza della base canonica, numerabile:
\[
x \left( t \right) = \sum_{n = - \infty}^{+ \infty} c_n w_n \left( t \right) = \frac{1}{\sqrt{T}} \sum_{n = - \infty}^{+ \infty} \langle x \left( t \right) , w_n \left( t \right) \rangle e^{j \frac{2 \pi}{T} nt}
\]
dove gli elementi della base $w_n \left( t \right)$ (infiniti e numerabili) sono:
\[
w_n \left( t \right) = \frac{1}{\sqrt{T}} e^{j \frac{2 \pi}{T} nt} \, , \quad - \frac{T}{2} \leq t \leq \frac{T}{2}
\]
e i coefficienti $c_n$ (complessi, infiniti e numerabili) sono:
\[
c_n = \langle x \left( t \right) , w_n \left( t \right) \rangle = \frac{1}{\sqrt{T}} \int_{- \frac{T}{2}}^{\frac{T}{2}} x \left( \theta \right) e^{-j \frac{2\pi}{T} n \theta} d \theta
\]

Siccome la base è completa vale l'uguaglianza di Parseval:
\[
E \left( x \right) = \sum_{n = - \infty}^{+ \infty} {\left| c_n \right|}^2 = \sum_{n = - \infty}^{+ \infty} {\left| \langle x \left( t \right) , w_n \left( t \right) \rangle \right|}^2
\]

Il segnale $x \left( t \right)$ è in corrispondenza biunivoca con una sequenza infinita di coefficienti, e si può vedere come un vettore a infinite dimensioni:
\[
x \left( t \right) \Leftrightarrow {\left( c_n \right)}_{n = - \infty}^{+ \infty}
\]

La base ortonormale introdotta precedentemente è la normalizzazione della seguente base ortogonale:
\[
x \left( t \right) = \sum_{n = - \infty}^{+ \infty} {\mu}_n w_n' \left( t \right) = \sum_{n = - \infty}^{+ \infty} \langle x \left( t \right) , w_n' \left( t \right) \rangle e^{j \frac{2 \pi}{T} nt}
\]
dove gli elementi della base $w_n' \left( t \right)$ sono:
\[
w_n' \left( t \right) = \sqrt{T} w_n \left( t \right) = e^{j \frac{2 \pi}{T} nt} \, , \quad - \frac{T}{2} \leq t \leq \frac{T}{2}
\]
e i coefficienti ${\mu}_n$ sono:
\[
{\mu}_n = \frac{1}{\sqrt{T}} c_n =  \frac{1}{T} \int_{- \frac{T}{2}}^{\frac{T}{2}} x \left( \theta \right) e^{-j \frac{2\pi}{T} n \theta} d \theta
\]

Usando questa base l'energia vale:
\[
E \left( x \right) = T \sum_{n = - \infty}^{+ \infty} {\left| {\mu}_n \right|}^2
\]

\subsection{Trasformata di Fourier}
Segnali a supporto $T$ infinito si approssimano con la trasformata di Fourier:
\[
x \left( t \right) =  \int_{-\infty}^{+ \infty} X \left( f \right) e^{j2 \pi ft} df
\]
\begin{itemize}
\item $\displaystyle X \left( f \right) = \mathcal{F} \left\{ x \left( t \right) \right\} \triangleq \int_{- \infty}^{+ \infty} x \left( \theta \right) e^{-j2 \pi f \theta} d \theta$
\item $\displaystyle x \left( t \right) = {\mathcal{F}}^{-1} \left\{ X \left( f \right) \right\} \triangleq \int_{- \infty}^{+ \infty} X \left( f \right) e^{j 2 \pi f t} df$
\end{itemize}
\begin{framed}
\paragraph[Dimostrazione]{Dimostrazione\footnotemark}
Partendo dalla base ortogonale per supporti finiti:
\[
x \left( t \right) = \sum_{n = - \infty}^{+ \infty} \left[ \frac{1}{T} \int_{- \frac{T}{2}}^{\frac{T}{2}} x \left( \theta \right) e^{-j 2 \pi \frac{n}{T} \theta} d \theta \right] e^{j 2 \pi \frac{n}{T} t} =
\]
facendo tendere a infinito l'ampiezza del supporto $T$ e considerando che la variabile discreta $T$ diventa la variabile continua $f$ (sequenza di infinitesimi $df$):
\[
= \begin{cases} \sum \longrightarrow \int \\
\Delta f = \frac{1}{T} \longrightarrow df \\
\frac{n}{T} \longrightarrow f \end{cases} \Rightarrow x \left( t \right) = \int_{- \infty}^{+ \infty} \left[ df \int_{- \infty}^{+ \infty} x \left( \theta \right) e^{-j2 \pi f \theta} d \theta \right] e^{j 2 \pi f t}
\]
\end{framed}
\footnotetext{Per semplicità non si considerano alcune condizioni al contorno.}

\paragraph[Condizione di esistenza]{Condizione di esistenza\footnote{Non si considera l'estensione del dominio delle funzioni al dominio delle distribuzioni.}}
Nel dominio delle funzioni, il segnale $x \left( t \right)$ deve essere modulo integrabile:
\[
\int_{- \infty}^{+ \infty} \left| x \left( t \right) \right| dt \in \mathbb{R}
\]

\subsubsection{Alcune trasformate fondamentali}
\label{sez:trasformate_fondamentali}
\paragraph{Delta di Dirac}
\[
\mathcal{F} \left\{ \delta \left( t \right) \right\} = 1 \Rightarrow \delta \left( t \right) = {\mathcal{F}}^{-1} \left\{ 1 \right\} = \int_{-\infty}^{+ \infty} e^{j2 \pi ft} df
\]
\begin{framed}
\paragraph{Dimostrazione}
\[
\mathcal{F} \left\{ \delta \left( t \right) \right\} = \int_{- \infty}^{+ \infty} \delta \left( t \right) e^{-j2 \pi f t} dt = e^{-j2 \pi f 0} = 1
\]
\end{framed}

\paragraph{Funzione segno}
\[
\mathcal{F} \left\{ \text{sgn} t \right\} = \frac{1}{j \pi f}
\]
\begin{framed}
\paragraph{Dimostrazione}
\[
\mathcal{F} \left\{  \text{sgn} t \right\} = \int_{- \infty}^{+ \infty}  \text{sgn} t e^{-j2 \pi f t} dt = \int_{- \infty}^{+ \infty}  \text{sgn} t \left[ \cos{\left( 2 \pi ft \right)} - j \sin{\left( 2 \pi f t \right)} \right] dt =
\]
\[
= \int_{- \infty}^{+ \infty}  \text{sgn} t \cos{\left( 2 \pi ft \right)} dt -j \int_{- \infty}^{+ \infty}  \text{sgn} t \sin{\left( 2 \pi f t \right)} dt = -2j \int_0^{+ \infty} \sin{\left( 2 \pi f t \right)} dt =
\]
\[
= \frac{j}{\pi f} {\left[ \cos{\left( 2 \pi ft \right)} \right]}_0^{+ \infty} = \lim_{a \to + \infty} ja \frac{\cos{\left( 2 \pi fa \right)}}{\pi fa} - \frac{j}{\pi f} = - \frac{j}{\pi f} = \frac{1}{j \pi f}
\]
\end{framed}

\paragraph{Funzione gradino}
\[
U \left( f \right) = \mathcal{F} \left\{ u \left( t \right) \right\} = \frac{1}{2} \delta \left( f \right) + \frac{1}{j2 \pi f}
\]