\chapter{Proprietà trasformata}
\section{Convoluzione}
L'operatore \textbf{prodotto di convoluzione} è definito in questo modo:
\[
z \left( t \right) = x \left( t \right) * y \left( t \right) \triangleq \int_{- \infty}^{+ \infty} x \left( \tau \right) y \left( t - \tau \right) d \tau
\]

La convoluzione tra due segnali $x \left( t \right)$, a supporto finito $\left[ -a , +a \right]$ e $y \left( t \right)$, a supporto finito $\left[ -b, +b \right]$:
\begin{itemize}
\item ha un supporto di ampiezza pari alla somma delle ampiezze: $\left[ - \left( b + a \right) , + \left( b + a \right) \right]$;
\item riduce le discontinuità, e in particolare è di classe $C^{n+1}$ se le due funzioni sono di classe $C^n$.
\end{itemize}

\subsection{Convoluzione di porte}
La convoluzione $z \left( t \right)$ tra la porta $x \left( \tau \right) = {\Pi}_{2a} \left( \tau \right)$ e la porta $y \left( \tau \right) = {\Pi}_{2b} \left( \tau \right)$ ha supporto $2 \left( b + a \right)$ ed è una funzione continua (classe $C^0$):
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{pic/03/Convoluzione1}
\end{figure}

Se $a = b$ la convoluzione $z \left( t \right)$ è la funzione $2a \operatorname{tri} \left( \tfrac{t}{2a} \right)$:
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.4]{pic/03/Convoluzione2}
\end{figure}

Effettuando infinite convoluzioni si ottiene una gaussiana.

Al contrario, nel dominio delle frequenze il supporto è ristretto all'intervallo di frequenze in cui entrambe le trasformate $X \left( f \right)$ e $Y \left( f \right)$ non sono nulle:
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{pic/03/Convoluzione3}
\end{figure}

\subsection{Proprietà}
\paragraph{Commutativa}
\[
x \left( t \right) * y \left( t \right) = y \left( t \right) * x \left( t \right)
\]

\paragraph{Associativa}
\[
x \left( t \right) * \left[ y \left( t \right) * w \left( t \right) \right] = \left[ x \left( t \right) * y \left( t \right) \right] * w \left( t \right)
\]

\paragraph{Distributiva}
\[
x \left( t \right) * \left[ y \left( t \right) + w \left( t \right) \right] =  x \left( t \right) * y \left( t \right) + x \left( t \right) * w \left( t \right)
\]

\section{Proprietà della delta di Dirac}
\subsection{Campionamento}
La moltiplicazione di una funzione $x \left( t \right)$ per una funzione delta $\delta \left( t - \tau \right)$, traslata di $\tau$, restituisce il campione di $x \left( t \right)$ in $t = \tau$:
\[
x \left( t \right) \delta \left( t  - \tau \right) = x \left( \tau \right) \delta \left( t - \tau \right)
\]
e pertanto si può introdurre una semplificazione sostituendo $x \left( t \right)$ una qualsiasi funzione $y \left( t \right)$ che assuma lo stesso valore in $t = \tau$, in particolare la funzione costante $y \left( t \right) = x \left( \tau \right)$.

\subsection{Traslazione}
La convoluzione di una funzione $x \left( t \right)$ con una funzione delta $\delta \left( t - \tau \right)$, traslata di $\tau$, restituisce il segnale traslato:
\[
x \left( t \right) * \delta \left( t - \tau \right) = x \left( t - \tau \right)
\]
\begin{framed}
\paragraph{Dimostrazione}
\[
x \left( t \right) * \delta \left( t - \tau \right) = \int_{- \infty}^{+ \infty} \delta \left( t' - \tau \right) x \left( t - t' \right) dt' = x \left( t - \tau \right)
\]
\end{framed}

\section{Proprietà della trasformata di Fourier}
\subsection{Linearità}
La trasformata di Fourier e la sua inversa sono \textbf{operatori lineari}:\footnote{Questa proprietà discende direttamente dalla linearità dell'operatore integrale.}
\[
\mathcal{F} \left\{ a_1 x_1 \left( t \right) + a_2 x_2 \left( t \right) \right\} = a_1 \mathcal{F} \left\{ x_1 \left( t \right) \right\} + a_2 \mathcal{F} \left\{ x_2 \left( t \right) \right\}
\]

\subsection{Anticipo o ritardo}
La trasformata di Fourier del segnale $x \left( t \right)$ \textbf{ritardato} o \textbf{anticipato} di una fase $\theta$ vale:
\[
\mathcal{F}\left\{ x \left( t - \theta \right) \right\} = \mathcal{F} \left\{ x \left( t \right) \right\} e^{-j2 \pi \theta f}
\]

Corrisponde graficamente a una rotazione della fase ($\arg X \left( f \right) - 2 \pi \theta f$), mentre il modulo $\left| X \left( f \right) \right|$ non varia.

\subsection{Modulazione e traslazione}
La \textbf{modulazione} del segnale $x \left( t \right)$, di una frequenza $f_0$, corrisponde alla \textbf{traslazione} della sua trasformata di Fourier:
\[
\mathcal{F} \left\{ x \left( t \right) e^{j2 \pi f_0 t} \right\} = X \left( f - f_0 \right)
\]
e si ottiene dalla composizione di due trasformate del segnale una simmetrica all'altra rispetto all'asse verticale:
\[
\mathcal{F} \left\{ x \left( t \right) \cos { \left( 2 \pi f_0 t \right) } \right\} = \frac{1}{2} \left[ X \left( f - f_0 \right) + X \left( f + f_0 \right) \right]
\]
\[
\mathcal{F} \left\{ x \left( t \right) \sin { \left( 2 \pi f_0 t \right) } \right\} = \frac{1}{2j} \left[ X \left( f - f_0 \right) -  X \left( f + f_0 \right) \right]
\]
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{pic/03/Modulazione_di_frequenza}
\end{figure}

\subsection{Scalamento}
Lo \textbf{scalamento} corrisponde a una dilatazione o un restrigimento sull'asse dei tempi, e a rispettivamente un restringimento o una dilatazione sull'asse delle frequenze:
\[
\mathcal{F} \left\{ x \left( Kt \right) \right\} = \frac{1}{\left| K \right|} X \left( \frac{f}{K} \right)
\]

\subsection{Relazioni di parità}
Se il segnale $x \left( t \right)$ è reale, allora la sua trasformata di Fourier $X \left( f \right)$ ha le seguenti relazioni di parità:
\begin{itemize}
\item la parte reale $\Re{\left\{ X \left( f \right) \right\}}$ è pari:
\[
\Re{\left\{X \left( -f \right) \right\}} = \Re{\left\{X \left( f \right) \right\}}
\]
\item la parte immaginaria $\Im{\left\{ X \left( f \right) \right\}}$ è dispari:
\[
\Im{\left\{X \left( -f \right) \right\}} = - \Im{\left\{X \left( f \right) \right\}}
\]
\item il modulo $\left| X \left( f \right) \right|$ è pari:
\[
{\left| X \left( f \right) \right|}^2 = {\Re}^2{\left\{X \left( f \right) \right\}} + {\Im}^2{\left\{X \left( f \right) \right\}} = \, \text{pari} \, \times \, \text{pari} \, + \, \text{dispari} \, \times \, \text{dispari} \, = \, \text{pari}
\]
\item la fase $\arg{X\left( f \right)}$ è dispari:
\[
\arg{X \left( f \right)} = \mathrm{arctg} \frac{\Im{\left\{X \left( f \right) \right\}}}{\Re{\left\{X \left( f \right) \right\}}} = \mathrm{arctg} \left(  \text{dispari}  \right) = \, \text{dispari}
\]
\end{itemize}

Se il segnale $x \left( t \right)$ è reale vale inoltre la \textbf{simmetria hermitiana} (o simmetria coniugata):
\[
X^* \left( -  f \right) = X \left( f \right)
\]

\subsection{Convoluzione e prodotto}
La trasformata di Fourier della convoluzione è pari al prodotto delle singole trasformate:
\[
Z \left( f \right) = \mathcal{F} \left\{ x \left( t \right) * y \left( t \right) \right\} = X \left( f \right) Y \left( f \right)
\]

\subsection{Derivazione ed integrazione}
\paragraph{Derivazione}
\[ \mathcal{F} \left\{ {\partial \over \partial t} x \left( t \right) \right\} = j2 \pi f X \left( f \right) \]
\[ \mathcal{F} \left\{ {{\partial}^n \over \partial t^n} x \left( t \right) \right\} = {\left( j2 \pi f \right)}^n X \left( f \right) \]

\paragraph{Integrazione}
\[
\mathcal{F} \left\{ \int_{- \infty}^t x \left( r \right) dr \right\} = \frac{1}{2} X \left( 0 \right) \delta \left( f \right) + \frac{X \left( f \right)}{j2 \pi f}
\]
\begin{framed}
\paragraph{Dimostrazione}
L'integrale fino a $t$ si può estendere fino a $+ \infty$ cancellando la parte a destra di $t$ con una funzione gradino:
\[
\int_{- \infty}^t x \left( r \right) dr = \int_{- \infty}^{+ \infty} u \left( t - r \right) x \left( r \right) d t = u \left( t \right) * x \left( t \right)
\]
\[
\mathcal{F} \left\{ \int_{- \infty}^{t} x \left( r \right) dr \right\} = \mathcal{F} \left\{ u \left( t \right) * x \left( t \right)  \right\} = U \left( f \right) X \left( f \right) =
\]

Ricordando la trasformata fondamentale\footnotemark\ della funzione gradino $u \left(t \right)$:
\[
U \left( f \right) = \frac{1}{2} \delta \left( f \right) + \frac{1}{j2 \pi f}
\]
si trova:
\[
 = \frac{1}{2} X \left( 0 \right) \delta \left( f \right) + \frac{X \left( f \right)}{j 2 \pi f}
\]
\end{framed}
\footnotetext{Si veda la sezione~\ref{sez:trasformate_fondamentali}.}

\subsection{Dualità}
\[ \mathcal{F} \left\{ X \left( t \right) \right\} = x \left( - f \right) \Rightarrow \mathcal{F} \left\{ x \left( t \right) \right\} = \mathcal{F}^{-1} \left\{ x \left( - f \right) \right\} \]

\subsection{Altre proprietà}
\paragraph{Uguaglianza di Parseval}
\[
E \left( x \right) = \int {\left| x \left( t \right) \right|}^2 dt = \int {\left| X \left( f \right) \right|}^2 df
\]

\paragraph{Invarianza prodotto scalare}
\[
\langle x \left( t \right), y \left( t \right) \rangle = \langle X \left( f \right) , Y \left( f \right) \rangle
\]

\paragraph{Diseguaglianza di Schwarz}
\[
\left| \langle X \left( f \right) , Y \left( f \right) \rangle \right| \leq \left\| X \left( f \right) \right\| \left\| Y \left( f \right) \right\|
\]

\subsection{Relazione tempo-frequenza}
\label{sez:relazione_tempo-frequenza}
Secondo la proprietà dello scalamento espandere l'asse dei tempi corrisponde a comprimere l'asse delle frequenze.

Per valutare quantitativamente la compattezza non si può tuttavia usare il supporto, perché si dimostra che:\footnote{Si noti che se la funzione o la sua trasformata hanno supporto infinito non si può dire niente sul supporto della corrispondente funzione.}
\begin{itemize}
\item se la funzione $x \left( t \right)$ ha supporto finito, la sua trasformata $X \left( f \right)$ non ha supporto finito;
\item se la funzione $X \left( f \right)$ ha supporto finito, la sua antitrasformata $x \left( t \right)$ non ha supporto finito.
\end{itemize}

Si definisce \textbf{estensione temporale} $d$:
\[
d^2 = \int t^2 \frac{{\left| x \left( t \right) \right|}^2}{E \left( x \right)} dt
\]

Si definisce \textbf{estensione di frequenza} $D$:
\[
D^2 = 4 {\pi}^2 \int f^2 \frac{{\left| X \left( f \right) \right|}^2}{E \left( x \right)} df
\]

È possibile dimostrare che vale:
\[ d \cdot D \geq \frac{1}{2} \]

L'estensione è il valore quadratico medio di una variabile casuale con distribuzione pari a:
\begin{itemize}
\item tempo: $\displaystyle \frac{{\left| x \left( t \right) \right|}^2}{E \left( x \right)}$
\item frequenza: $\displaystyle \frac{{\left| X \left( f \right) \right|}^2}{E \left( x \right)}$
\end{itemize}

Supponendo nullo il valor medio, il valore quadratico medio coincide con la varianza.

\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|}
  \hline
  \textbf{Linearità} & $\displaystyle \mathcal{F} \left\{ a_1 x_1 \left( t \right) + a_2 x_2 \left( t \right) \right\} = a_1 \mathcal{F} \left\{ x_1 \left( t \right) \right\} + a_2 \mathcal{F} \left\{ x_2 \left( t \right) \right\}$ \\
  \hline
  \textbf{Anticipo o ritardo} & $\displaystyle \mathcal{F}\left\{ x \left( t - \theta \right) \right\} = \mathcal{F} \left\{ x \left( t \right) \right\} e^{-j2 \pi \theta f} $ \\
  \hline
  \textbf{Modulazione e traslazione} & $\displaystyle \mathcal{F} \left\{ x \left( t \right) e^{j2 \pi f_0 t} \right\} = X \left( f - f_0 \right)$ \\
  \hline
  \textbf{Scalamento} & $\displaystyle \mathcal{F} \left\{ x \left( Kt \right) \right\} = \frac{1}{\left| K \right|} X \left( \frac{f}{K} \right)$ \\
  \hline
  \textbf{Relazioni di parità} & $\displaystyle x \left( t \right) \in \mathbb{R} \Leftrightarrow \begin{cases} \Re{ \left\{ X \left( f \right) \right\} } \, \text{e} \, \left| X \left( f \right) \right| \, \text{sono pari} \\ \Im{ \left\{ X \left( f \right) \right\} } \, \text{e} \, \arg{X \left ( f \right)} \, \text{sono dispari} \end{cases}$ \\
  \hline
  \textbf{Convoluzione e prodotto} & $\displaystyle \mathcal{F} \left\{ x \left( t \right) * y \left( t \right) \right\} = X \left( f \right) Y \left( f \right)$ \\
  \hline
  \textbf{Dualità} & $\displaystyle \mathcal{F} \left\{ X \left( f \right) \right\} = x \left( - t \right) \Rightarrow \mathcal{F} \left\{ x \left( t \right) \right\} = \mathcal{F}^{-1} \left\{ x \left( - t \right) \right\}$ \\
  \hline
 \end{tabular}}
\end{table}

\section{Esempi di trasformate}
\subsection{Funzione porta}
\begin{figure}
	\centering
	\includegraphics[scale=0.8]{pic/03/Sinc4}
\end{figure}

\[
\mathcal{F} \left\{ {\Pi}_T \left( t \right) \right\} = T \mathrm{sinc} \left( f T \right)
\]
\FloatBarrier

\subsection{Segnale numerico}
Nel trasferimento in modulazione di un segnale digitale, il segnale sagomatore di riferimento viene moltiplicato in ampiezza per $+1$ o $-1$ (invertito) a seconda se il bit da trasferire è rispettivamente 1 o 0. Generalizzando da una base binaria a una base qualunque, il segnale sagomatore $r \left( t - i T \right)$ viene moltiplicato per una opportuna costante $a_i$, e il segnale digitale $x \left( t \right)$ sia ottenuto dalla seguente combinazione lineare:
\[
x \left( t \right) = \sum_{i = - \infty}^{+ \infty} a_i r \left( t - i T \right) \cos{\left( 2 \pi f_0 t \right)}
\]

Per la proprietà del ritardo:
\[
\mathcal{F} \left\{ r \left( t -iT \right) \right\} = R \left( f \right) e^{- j 2 \pi f i T}
\]

Per la proprietà di linearità:
\[
\mathcal{F} \left\{ \sum_{i = - \infty}^{+ \infty} a_i r \left( t - i T \right) \right\} = \sum_{i = - \infty}^{+ \infty} a_i \mathcal{F} \left\{ r \left( t -iT \right) \right\} = R \left( f \right) \sum_{- \infty}^{+ \infty} a_i  e^{- j 2 \pi f i T} = Z \left( f \right)
\]

Siccome $R \left( t \right)$ è a coefficiente della sommatoria, un segnale numerico $z \left( t \right)$ non può avere un'occupazione spettrale maggiore di quella del segnale sagomatore $r \left( t \right)$ $\Rightarrow$ l'ampiezza dello spettro $Z \left( t \right)$ del segnale è controllabile tramite un opportuno segnale sagomatore.

Infine per la proprietà di modulazione:
\[
\mathcal{F} \left\{ x \left( t \right) \right\} = \frac{1}{2} \left[ Z \left( f - f_0 \right) + Z \left( f - f_0 \right) \right]
\]