\chapter{Diagrammi a blocchi}
\section{Diagrammi a blocchi}
Un \textbf{diagramma a blocchi} è un sistema LTI costituito da un insieme di sistemi LTI interconnessi tramite dei blocchi fondamentali.

\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|}
  \hline
  \textbf{Sommatore} & \begin{tabular}[c]{@{}c@{}}\includegraphics[scale=0.5]{pic/05/Diagramma_sommatore.png}\end{tabular} & \multicolumn{2}{|c|}{\begin{tabular}[c]{@{}c@{}}$ \displaystyle y \left( t \right) = x_1 \left( t \right) + x_2 \left( t \right)$\\
  $\displaystyle Y \left( f \right) = X_1 \left( f \right) + X_2 \left( f \right)$\end{tabular}} \\
  \hline
  \textbf{Ritardatore} & \begin{tabular}[c]{@{}c@{}}\includegraphics[scale=0.45]{pic/05/Diagramma_ritardatore.png}\end{tabular} & \begin{tabular}[c]{@{}c@{}}$ \displaystyle h \left( t \right) = \delta \left( t - T \right)$\\
  $\displaystyle H \left( f \right) = e^{- j 2 \pi f T}$\end{tabular} & \begin{tabular}[c]{@{}c@{}}$ \displaystyle y \left( t \right) = x \left( t - T \right)$\\
  $\displaystyle Y \left( f \right) = X \left( f \right) e^{- j 2 \pi f T}$\end{tabular} \\
  \hline
  \textbf{Amplificatore} & \begin{tabular}[c]{@{}c@{}}\includegraphics[scale=0.45]{pic/05/Diagramma_amplificatore.png}\end{tabular} & \begin{tabular}[c]{@{}c@{}}$ \displaystyle h \left( t \right) = A \delta \left( t \right)$\\
  $\displaystyle H \left( f \right) = A$\end{tabular} & \begin{tabular}[c]{@{}c@{}}$ \displaystyle y \left( t \right) = A x \left( t \right)$\\
  $\displaystyle Y \left( f \right) = A X \left( f \right)$\end{tabular} \\
  \hline
 \end{tabular}}
 \caption{Blocchi fondamentali.}
\end{table}

\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|}
  \hline
  \textbf{Serie} & \begin{tabular}[c]{@{}c@{}}\includegraphics[scale=0.45]{pic/05/Diagramma_serie.png}\end{tabular} & \begin{tabular}[c]{@{}c@{}}$\displaystyle y \left( t \right) = x \left( t \right) * h_1 \left( t \right) * h_2 \left( t \right)$\\$\displaystyle Y \left( f \right) = X \left( f \right) H_1 \left( f \right) H_2 \left( f \right)$\end{tabular} \\
  \hline
  \textbf{Parallelo} & \begin{tabular}[c]{@{}c@{}}\includegraphics[scale=0.45]{pic/05/Diagramma_parallelo.png}\end{tabular} & \begin{tabular}[c]{@{}c@{}}$\displaystyle y \left( t \right) = x \left( t \right) * \left[ h_1 \left( t \right) + h_2 \left( t \right) \right]$\\$\displaystyle Y \left( f \right) = X \left( f \right) \left[ H_1 \left( f \right) + H_2 \left( f \right) \right]$\end{tabular} \\
  \hline
  \begin{tabular}[c]{@{}c@{}}\textbf{Retroazione}\\\textbf{(o feedback)}\end{tabular} & \begin{tabular}[c]{@{}c@{}}\includegraphics[scale=0.45]{pic/05/Diagramma_retroazione.png}\end{tabular} & \begin{tabular}[c]{@{}c@{}}$\displaystyle y \left( t \right) = \left[ x \left( t \right) + y \left( t \right) * h_2 \left( t \right) \right] * h_1 \left( t \right)$\\$\displaystyle Y \left( f \right) = X \left( f \right) \frac{H_1 \left( f \right)}{1 - H_1 \left( f \right) H_2 \left( f \right)}$\end{tabular} \\
  \hline
 \end{tabular}}
 \caption{Tipi di interconnessione.}
\end{table}

\begin{framed}
\paragraph{Dimostrazione retroazione}
\[
Y \left( f \right) = \left[ X \left( f \right) + Y \left( f \right) H_2 \left( f \right) \right] H_1 \left( f \right)
\]
\[
Y \left( f \right) \left[ 1 - H_2 \left( f \right) H_1 \left( f \right) \right] = X \left( f \right) H_1 \left( f \right)
\]
\[
H \left( f \right) = \frac{Y \left( f \right)}{X \left( f \right)} = \frac{H_1 \left( f \right)}{1 - H_2 \left( f \right) H_1 \left( f \right)}
\]
\end{framed}

Con ritardatori e amplificatori opportunamente concatenati si possono ottenere sistemi LTI con funzioni di trasferimento desiderate nella forma:
\begin{itemize}
\item senza retroazione (Finite Impulse Response [FIR]):
\[
H_{\text{FIR}} \left( f \right) = \sideset{}{_i}\sum{{\alpha}_i e^{-j2 \pi f {\tau}_i}}
\]
\item con retroazione (Infinite Impulse Response [IIR]):
\[
H_{\text{IIR}} \left( f \right) = \frac{\sideset{}{_i}\sum{{\alpha}_i e^{-j2 \pi f {\tau}_i}}}{\sideset{}{_k}\sum{{\beta}_k e^{-j2 \pi f {\tau}_k}}}
\]
\end{itemize}

\section{Definizioni di banda}
La \textbf{banda} di un segnale o di un sistema\footnote{Per ``banda di un sistema'' s'intende la banda della sua funzione di trasferimento $H \left( f \right)$.} può essere definita in vari modi a seconda del contesto applicativo.

Siccome la trasformata di Fourier di un segnale o di un sistema reale è pari, quindi simmetrica rispetto all'asse delle ordinate, spesso si considera solo la parte positiva dell'asse delle frequenze misurando la \textbf{banda unilatera}, che equivale alla metà della \textbf{banda bilatera}.

\subsection{Misura del supporto}
La larghezza di banda è pari al supporto del segnale nel dominio della frequenza, cioè all'intervallo di frequenze in cui il segnale non è nullo.

Spesso la definizione di banda come misura del supporto della trasformata di Fourier è troppo restrittiva, perché molti segnali e sistemi hanno in realtà una trasformata di Fourier con supporto infinito. Molti sistemi tuttavia presentano un intervallo di frequenze dove la funzione di trasferimento è quasi nulla.

\subsection{Banda a 3 dB}
Questa definizione è usata in particolare quando si parla di filtri.

\paragraph{Su scala lineare (\texorpdfstring{${\left| H \left( f \right) \right|}^2$}{Hf2})}
La larghezza di banda $B_p$ è pari all'intervallo di frequenze compreso tra la frequenza di taglio inferiore $f_1$ e la frequenza di taglio superiore $f_2$:
\begin{itemize}
\item la frequenza di taglio inferiore $f_1$ corrisponde a un dimezzamento rispetto al valore massimo $A$;
\item la frequenza di taglio superiore $f_2$ corrisponde a un dimezzamento rispetto al valore massimo $A$.
\end{itemize}
\begin{figure}[H]%WARNING
	\centering
	\includegraphics[scale=0.6]{pic/05/Banda_a_3_dB_lineare.png}
\end{figure}

\paragraph{Su scala logaritmica (\texorpdfstring{${\left| H \left( f \right) \right|}_{\text{dB}}^2 = 20 \log_{10}{\left| H \left( f \right) \right|}$}{Hf2db=20log10Hf})}
La larghezza di banda $B_p$ è pari all'intervallo di frequenze compreso tra la frequenza di taglio inferiore $f_1$ e la frequenza di taglio superiore $f_2$:
\begin{figure}[H]%WARNING
	\centering
	\includegraphics[scale=0.6]{pic/05/Banda_a_3_dB_logaritmica.png}
\end{figure}
\begin{itemize}
\item la frequenza di taglio inferiore $f_1$ corrisponde a un'attenuazione di 3 dB rispetto al valore massimo $A_{\text{dB}}$;
\item la frequenza di taglio superiore $f_2$ corrisponde a un'attenuazione di 3 dB rispetto al valore massimo $A_{\text{dB}}$.
\end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.45]{pic/05/Classificazione_filtri.png}
\end{figure}

\paragraph{Classificazione dei filtri}
\begin{itemize}
\item \textbf{filtro passa-basso} (detto banda base): il filtro ha banda finita centrata intorno all'origine (DC);
\item \textbf{filtro passa-banda:} il filtro ha banda finita, che non include l'origine;
\item \textbf{filtro passa-alto:} il filtro ha banda infinita, che non include l'origine;
\item \textbf{filtro elimina-banda:} il filtro ha banda infinita, e non include un certo intervallo di frequenze.
\end{itemize}

Gli ultimi due tipi di filtri sono ideali perché non esistono filtri a banda infinita.

Per \textbf{filtro ideale} si intende quello che presenta le migliori caratteristiche possibili:
\begin{itemize}
\item guadagno unitario nella banda passante;
\item guadagno nullo nella banda attenuata (o oscura).
\end{itemize}

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.5]{pic/05/Passa-basso_ideale_reale.png}
\end{figure}

I filtri ideali non sono realizzabili.

\subsection{Banda equivalente di rumore}
\begin{figure}
	\centering
	\includegraphics[scale=0.6]{pic/05/Banda_equivalente_di_rumore.png}
\end{figure}

\noindent
La larghezza di banda $B_{\text{eq}}$ è pari alla larghezza del rettangolo:
\begin{itemize}
\item la cui altezza è pari al massimo $A$ di ${\left| H \left( f \right) \right|}^2$;
\item la cui area è uguale all'energia complessiva $E \left( H \right)$ della funzione di trasferimento, cioè all'area di ${\left| H \left( f \right) \right|}^2$ sull'intero asse delle frequenze:
\[
B_{\text{eq}} = \frac{E \left( H \left( f \right) \right)}{A}
\]
\end{itemize}
\FloatBarrier

\subsection{Banda che contiene una data percentuale di energia}
\begin{figure}
	\centering
	\includegraphics[scale=0.6]{pic/05/Banda_percentuale.png}
\end{figure}

\noindent
La larghezza di banda $B_{x \%}$ è pari al supporto della parte di grafico la cui energia corrisponde all'$x\%$ dell'energia complessiva $E \left( H \right)$ della funzione di trasferimento, ovvero dell'area di ${\left| H \left( f \right) \right|}^2$ sull'intero asse delle frequenze:
\[
B_{x \%} = \left| a - b \right| : \, \int_a^b {\left| H \left( f \right) \right|}^2 df = \frac{x}{100} E \left( H \right)
\]

\subsection[Estensione di frequenza]{Estensione di frequenza\footnote{Per approfondire, si veda la sezione~\ref{sez:relazione_tempo-frequenza}.}}
La banda è l'estensione di frequenza $D$:
\[
D^2 = 4 {\pi}^2 \int f^2 \frac{{\left| X \left( f \right) \right|}^2}{E \left( x \right)} df
\]

\section{Distorsione lineare}
La \textbf{distorsione lineare} è il fenomeno che modifica la forma del segnale di ingresso di un sistema LTI: le sinusoidi che passano per un sistema LTI non sono tutte moltiplicate per uno stesso valore costante. Un sistema LTI non presenta distorsione lineare se introduce solo un'amplificazione e/o un ritardo, senza modificare la forma:
\[
y \left( t \right) = k x \left( t - t_D \right)
\]
cioè la funzione di trasferimento $H \left( f \right)$ del sistema deve avere modulo costante e fase lineare:
\[
H \left( f \right) = k e^{-j 2 \pi t_D f}
\]

I blocchi fondamentali ideali, amplificatore e ritardatore, qualsiasi frequenza si consideri non introducono alcuna distorsione lineare. Nella realtà non è possibile realizzare un sistema non distorcente ideale, ma si possono realizzare dei sistemi che non introducono distorsione limitatamente a una certa banda.

Una distorsione lineare è eliminabile con un \textbf{equalizzatore} posto in serie. Ad esempio, se un segnale $x \left( t \right)$ viene distorto da un \textbf{canale}, cioè un sistema LTI la cui funzione di trasferimento $H_c \left( f \right)$ non è modificabile, è possibile porre in serie un equalizzatore che abbia una funzione di trasferimento $H_e \left( f \right)$ con a denominatore proprio $H_e \left( f \right)$ (che si semplifica nel prodotto delle funzioni di trasferimento nella serie):\footnote{Il canale non dev'essere nullo.}
\[
H_e \left( f \right) = \frac{ke^{-j2 \pi t_D f}}{H_e \left( f \right)}
\]

\section{Modulazione e demodulazione}
\subsection{Modulazione}
Si ricorda che la modulazione di un segnale $x \left( t \right)$, cioè la sua moltiplicazione con una cosinusoide\footnote{Non si considera la componente immaginaria sinusoidale.}, corrisponde nel dominio della frequenza a uno ``sdoppiamento'' dello spettro:
\[
\mathcal{F} \left\{ x \left( t \right) \cos { \left( 2 \pi f_0 t \right) } \right\} = \frac{1}{2} \left[ X \left( f - f_0 \right) + X \left( f + f_0 \right) \right]
\]

Grazie alla modulazione è possibile trasferire un segnale $x \left( t \right)$ in banda base attraverso un canale (ad esempio radio) avente una funzione di trasferimento $H_c \left( f \right)$ di tipo passa-banda.

Il segnale modulato $X' \left( f \right)$:
\[
X ' \left( f \right) = \frac{1}{2} \left[ X \left( f - f_0 \right) + X \left( f + f_0 \right) \right]
\]
attraversa il canale e viene ricevuto amplificato (o attenuato) dal filtro passa-banda:
\[ Y \left( f \right) = \frac{A}{2} \left[ X \left( f - f_0 \right) + X \left( f + f_0 \right) \right] \]

\subsection{Demodulazione}
Il segnale originario può essere ora ricostruito tramite la \textbf{demodulazione}:
\begin{enumerate}
\item il segnale ricevuto $Y \left( f \right)$ viene modulato di nuovo in base alla stessa frequenza $f_0$ di prima:
\[
Y' \left( f \right) = \frac{A}{2} X \left( f \right) + \frac{A}{4} \left[ X \left( f - 2 f_0 \right) + X \left( f + 2 f_0 \right) \right]
\]
\begin{framed}
\paragraph{Dimostrazione}
Assumendo per semplicità che l'ampiezza del segnale $x \left( t \right)$ sia pari a 1, il segnale modulato vale:
\[
\mathcal{F} \left\{ \cos{\left( 2 \pi f_0 t \right)} \right\} = \frac{1}{2} \left[ \delta \left( f - f_0 \right) + \delta \left( f + f_0 \right) \right]
\]

Con un'ulteriore modulazione si ottiene:
\[
\mathcal{F} \left\{ \cos{\left( 2 \pi f_0 t \right) \cos{\left( 2 \pi f_0 t \right)}} \right\} =
\]
\[
= \left\{ \frac{1}{2} \left[ \delta \left( f - f_0 \right) + \delta \left( f + f_0 \right) \right] \right\} * \left\{ \frac{1}{2} \left[ \delta \left( f - f_0 \right) + \delta \left( f + f_0 \right) \right] \right\} =
\]
\[
=\frac{1}{4} \left[ \delta \left( f - f_0 \right) * \delta \left( f - f_0 \right) + 2 \delta \left( f - f_0 \right) * \delta \left( f + f_0 \right) + \delta \left( f + f_0 \right) * \delta \left( f + f_0 \right) \right]=
\]
\begin{itemize}
\item $\displaystyle \delta \left( f - f_0 \right) * \delta \left( f - f_0 \right) = \delta \left( f - 2 f_0 \right)$
\item $\displaystyle \delta \left( f - f_0 \right) * \delta \left( f + f_0 \right) = \delta \left( f \right)$
\item $\displaystyle  \delta \left( f + f_0 \right) * \delta \left( f + f_0 \right) = \delta \left( f + 2 f_0 \right)$
\end{itemize}
\[ = \frac{1}{4} \left[ \delta \left( f - 2 f_0 \right) + 2 \delta \left( f \right) + \delta \left( f + 2 f_0 \right) \right] \]
\end{framed}
ottenendo nel dominio della frequenza tre repliche, di cui quella centrale è proprio il segnale originario solo amplificato (o attenuato):
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.55]{pic/05/Demodulazione.png}
\end{figure}

\item le componenti ad alta frequenza vengono eliminate tramite un opportuno filtro passa-basso, riottenendo così il segnale originario.
\end{enumerate}

Perché il sistema funzioni devono essere rispettate le seguenti condizioni:
\begin{itemize}
 \item la banda passante del canale deve essere più grande della banda del segnale da trasmettere;
 \item la banda del filtro in ricezione deve essere più grande della banda del segnale trasmesso;
 \item la frequenza centrale $f_0$ deve essere maggiore della banda unilatera del segnale da trasmettere.
\end{itemize}

Se la funzione di trasferimento del canale non è piatta, cioè è distorcente, nell'intervallo di frequenze del segnale trasmesso il filtro di ricezione può, oltre a fare da filtro passa-banda, anche compensare la distorsione e quindi servire da filtro di equalizzazione.

\subsection{Multiplazione in frequenza}
Più segnali con occupazione di banda sovrapposta possono essere multiplati, cioè trasmessi in contemporanea, su un singolo canale modulandoli in base a frequenze $f_i$ diverse, in modo che gli spettri non si sovrappongano l'un l'altro.