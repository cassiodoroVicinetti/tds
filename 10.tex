\chapter{Processi casuali}
\label{cap10}
Un \textbf{processo casuale} è un'espressione matematica che descrive una classe di segnali (voce, video, dati\textellipsis), tramite una o più \textbf{variabili casuali} che corrispondono alle caratteristiche della classe di segnali. Nel caso dei segnali vocali, teoricamente si dovrebbe registrare un certo numero statistico di parlatori e cercare di capire quali caratteristiche (come la frequenza) sono proprie di un segnale vocale, associando a ciascuna caratteristica di ciascun parlatore una probabilità.

Un processo casuale $X \left( t \right)$ è \textbf{quasi determinato} se è esprimibile in funzione di un insieme \ul{numerabile} di variabili casuali.

Il verificarsi dell'evento $s_i$ produce la \textbf{realizzazione} $x \left( t ; s_i \right)$; fissato un certo $t_0$, si ottiene una serie di \textbf{campioni} $x \left( t_0, s_i \right)$.

\begin{itemize}
\item processi quasi determinati: segnale numerico, sinusoide, segnale sample \&\ hold
\item processi non quasi determinati: rumore termico, segnale vocale, segnale audio
\end{itemize}

\afterpage{
\begin{landscape}
\begin{table}[p]
 \centering
 \begin{tabular}{|c|c|c|}
  \hline
  \textbf{Segnale numerico} & \textbf{Segnale sample \&\ hold} & \textbf{Sinusoide} \\
  \hline
  \includegraphics[scale=0.55]{pic/10/Processo_casuale_1.png} & \includegraphics[scale=0.55]{pic/10/Processo_casuale_2.png} & \includegraphics[scale=0.55]{pic/10/Processo_casuale_3.png} \\
  \hline
  $\displaystyle X \left( t \right) = \sum_{i = - \infty}^{+ \infty} {\alpha}_i r \left( t - i T \right)$ & $\displaystyle X_S \left( t \right) = \sum_{n = - \infty}^{+ \infty} X \left( n T \right) h \left( t - n T \right)$ & $\displaystyle X \left( t \right) = A \cos{\left( 2 \pi f t + \varphi \right)}$ \\
  \hline
 \end{tabular}
 \caption{Esempi di segnali quasi determinati.}
\end{table}
\end{landscape}
}

I segnali determinati sono dei casi degeneri dei processi casuali, perché un segnale determinato è la manifestazione di un'unica realizzazione avente probabilità 1 (non ci sono variabili casuali).

\section{Descrizione probabilistica}
\subsection{Statistiche}
Consideriamo un processo casuale $X \left( t ; s \right)$ contenente la sola variabile casuale $s$ $\Rightarrow$ a ogni valore $s_j$ è associata una realizzazione $x \left( t ; s_j \right)$ (con $j = 1, \ldots , M$).

\paragraph{Statistica di ordine 1}
Fissato un tempo $t_1$, l'insieme dei campioni $x_1 = x \left( t_1, s_j \right)$ costituisce l'insieme dei valori per la variabile casuale $X_1 = x \left( t_1 \right)$, per la quale è possibile definire la distribuzione cumulativa $F_{X_1} \left( x_1 ; t_1 \right)$ e la densità di probabilità $f_{X_1} \left( x_1 ; t_1 \right)$:
\[
F_{X_1} \left( x_1 ; t_1 \right) = P \left( X_1 < x_1 \right)
\]
\[
f_{X_1} \left( x_1 ; t_1 \right) = \frac{\partial}{\partial x_1} F_{X_1} \left( x_1 ; t_1 \right)
\]

\paragraph{Statistica di ordine $n$}
Considerando $n$ istanti di tempo $t_i$, si introduce una probabilità congiunta tra le $n$ variabili casuali $X_i$:
\[
F_{\mathbf{X}} \left( \mathbf{x} ; \mathbf{t} \right) = F_{X_1 ,  \ldots , X_n  } \left( x_1 , \ldots , x_n ; t_1 , \ldots , t_n \right) = P \left( X_1 < x_1 , \ldots , X_n < x_n \right)
\]
\[ f_{\mathbf{X} } \left( \mathbf{x}  ; \mathbf{t} \right) = f_{X_1 ,  \ldots , X_n } \left( x_1 , \ldots , x_n ; t_1 , \ldots , t_n \right) = \frac{{\partial}^n}{\partial x_1 \cdots \partial x_n} F_{\mathbf{X}} \left( \mathbf{x} ; \mathbf{t} \right) \]

\begin{figure}[H]
	\centering
	\includegraphics[width=\linewidth]{pic/10/Descrizione_probabilistica_processo_casuale.png}
\end{figure}

Un processo casuale è completamente caratterizzato/descritto se si conoscono le statistiche di qualsiasi ordine ($n \to \infty$).

\subsection{Media e autocorrelazione}
\label{sez:media_e_autocorrelazione}
\paragraph{Media d'insieme}
\[ m_X \left( t \right) \triangleq E \left( X \left( t \right) \right) = \int x f_X \left( x ; t \right) dx \]

\paragraph{Funzione di autocorrelazione}
\[ R_X \left( t_1 , t_2 \right) \triangleq E \left( X \left( t_1 \right) X^* \left( t_2 \right) \right) = \iint x_1 x_2^* f_{X_1 , X_2} \left( x_1 , x_2 ; t_1 , t_2 \right) d x_1 d x_2 \]

\paragraph{Autocovarianza}
\[ K_X \left( t_1 , t_2 \right) \triangleq E \left\{ \left[ X \left( t_1 \right) - m_X \left( t_1 \right) \right] \left[ X \left( t_2 \right) - m_X \left( t_2 \right) \right]^* \right\} = R_X \left( t_1 , t_2 \right) - m_X \left( t_1 \right) m_X^* \left( t_2 \right) \]

\paragraph{Coefficiente di correlazione}
\[ \rho_X \left( t_1 , t_2 \right) \triangleq \frac{K_X \left( t_1 , t_2 \right)}{\sqrt{K_X \left( t_1 , t_1 \right) K_X \left( t_2 , t_2 \right)}} \]