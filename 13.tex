\chapter{Medie temporali ed ergodicità}
Se il processo è \textbf{ergodico}, è sufficiente una sua qualunque realizzazione per estrarne le statistiche. Il sistema che genera il processo può evolvere attraverso tutti i suoi possibili stati partendo da una qualsiasi condizione iniziale.

\section{Media}
\subsection{Media d'insieme di un processo casuale}
Dato un processo casuale $X \left( t \right)$ ed una qualsiasi funzione $g$, la media d'insieme\footnote{Si veda il capitolo~\ref{cap10}.}:
\[
E \left[ g \left( X \left( t \right) \right) \right] = \int_{- \infty}^{+ \infty} g \left( x \left( t \right) \right) f_X \left( x \left( t \right) \right) d x
\]
su un insieme ``discreto'' di realizzazioni si può interpretare come la media pesata delle realizzazioni del processo $X \left( t \right)$, e a differenza della media temporale restituisce un valore dipendente dal tempo:
\[
E \left[ g \left( X \left( t \right) \right) \right] = \sideset{}{_i}\sum{g \left[ x \left( t ; s_i \right) \right] P \left[ x \left( t ; s_i \right) \right]}
\]

\subsection{Media temporale}
\subsubsection{Media temporale di un segnale determinato}
Dato un segnale determinato $x \left( t \right)$ ed una qualsiasi funzione $g$, l'operatore di \textbf{media temporale} è definito:\footnote{Da non confondere con l'operatore di prodotto scalare.}
\[
\langle g \left[ x \left( t \right) \right] \rangle \triangleq \lim_{T \to + \infty} \frac{1}{T} \int_{- \frac{T}{2}}^{+ \frac{T}{2}} g \left[ x \left( t \right) \right] dt
\]

\paragraph{Valor medio}
\[
\overline{x} = \lim_{T \to + \infty} \frac{1}{T} \int_{- \frac{T}{2}}^{+ \frac{T}{2}} x \left( t \right) dt = \langle x \left( t \right) \rangle
\]

\paragraph{Potenza media}
\[ P \left( x \right) = \lim_{T \to + \infty} \frac{1}{T} \int_{- \frac{T}{2}}^{+ \frac{T}{2}} {\left| x \left( t \right) \right|}^2 dt = \langle {\left| x \left( t \right) \right|}^2 \rangle \]

\subsubsection{Media temporale di più segnali determinati}
La media temporale di una funzione $g$ di $n$ segnali $x_1, x_2, \ldots, x_n$, valutati a istanti di tempo anche differenti, è una funzione di $n - 1$ variabili $\tau_1, \tau_2, \ldots, \tau_{n-1}$ (la variabile $t$ viene integrata):
\[
\langle g \left[ x_1 \left( t \right) , x_2 \left( t + \tau_1 \right) , \ldots , x_n \left( t + \tau_{n-1} \right) \right]  \rangle = \lim_{T \to + \infty} \frac{1}{T} \int_{- \frac{T}{2}}^{+ \frac{T}{2}} g \left[ x_1 \left( t \right) , x_2 \left( t + \tau_1 \right) , \ldots , x_n \left( t + \tau_{n-1} \right) \right] dt
\]

\subsubsection{Media temporale di una realizzazione}
Siccome una specifica realizzazione $g \left[ x \left( t ; s_0 \right) \right]$ di un processo casuale $X \left( t \right)$ è un segnale determinato, anche ad esso è possibile applicare l'operatore di media temporale:
\[
\langle g \left[ x \left( t ; s_0 \right) \right] \rangle = \lim_{T \to + \infty} \frac{1}{T} \int_{- \frac{T}{2}}^{+ \frac{T}{2}} g \left[ x \left( t ; s_0 \right) \right] dt
\]

Il processo è \textbf{stazionario per la sua media temporale} se questa non dipende dalla realizzazione.

\paragraph{Esempio: Potenza}
La media temporale è la potenza istantanea di una certa realizzazione:
\[
\langle g \left[ x \left( t \right) \right] \rangle = \langle {\left| x \left( t ; s_0 \right) \right| }^2 \rangle = \lim_{T \to + \infty} \frac{1}{T} \int_{- \frac{T}{2}}^{\frac{T}{2}} {\left| x \left( t ; s_0 \right) \right|}^2 dt
\]

La media d'insieme è legata alla potenza media del processo:\marginpar{non chiaro}
\[ \left. E \left[ g \left( X \left( t \right) \right) \right] \right \vert_{t = t_i} = E \left( {\left| X \left( t_i \right) \right|}^2 \right) \]

\subsection{Ergodicità per la media}
Un processo $X \left( t \right)$ è \textbf{ergodico per la media} se la sua media d'insieme coincide con la media temporale di una sua qualsiasi realizzazione:
\[
E \left[ g \left( X \left( t \right) \right) \right] = \langle g \left[ x \left( t ; s_i \right) \right] \rangle \quad \forall i
\]

Nel caso di $g$ funzione identità, se l'autocovarianza $K_X \left( \tau \right)$ è modulo integrabile il processo $X \left( t \right)$ è ergodico per la media.

\subsubsection{Ergodicità per la media e stazionarietà}
\begin{itemize}
\item Il processo è stazionario per la sua media temporale se questa non dipende dalla realizzazione.
\item Il processo è stazionario in senso stretto di ordine 1\footnote{Si veda la sezione~\ref{sez:wss}.} se la sua media d'insieme è costante nel tempo.
\end{itemize}

L'ergodicità per la media implica la stazionarietà per la media, ma la stazionarietà per la media \ul{non} implica l'ergodicità per la media.

\paragraph{Esempi}
\begin{itemize}
\item se il processo $\mathcal{P}$ contiene tutte le traslazioni di un segnale $x \left( t \right)$, e tutte le traslazioni hanno la stessa probabilità, allora il processo è ergodico:
\[
\mathcal{P} = \left\{ x \left( t + t_1 \right)  \quad \forall t_1 \right\}
\]
\item se il processo $\mathcal{P}$ contiene tutte le traslazioni di due segnali diversi $x \left( t \right)$ e $y \left( t \right)$, e tutte le traslazioni hanno la stessa probabilità, allora il processo non è ergodico perché una qualsiasi realizzazione può essere la traslazione o di $x \left( t \right)$ o di $y \left( t \right)$:
\[
\mathcal{P} = \left\{ x \left( t + t_1 \right) , y \left( t + t_2 \right) \quad \forall t_1, t_2 \right\}
\]
\item segnale vocale:\footnote{Un \textbf{segnale vocale} è l'insieme dei segnali generabili dall'apparato fonatorio di un umano.} non è ergodico perché una persona non può fisicamente generare tutti i segnali generabili da un qualunque essere umano;
\item rumore termico\footnote{Il \textbf{rumore termico} è l'insieme dei segnali generabili da una qualsiasi resistenza posta a temperatura $T$.} a una temperatura data: è ergodico perché non dipende dalla resistenza scelta.
\end{itemize}

\section{Autocorrelazione}
Si ricorda che esistono due diverse definizioni per l'autocorrelazione a seconda se si parli di segnali determinati o di processi casuali:
\begin{itemize}
\item autocorrelazione per segnali determinati a potenza finita:\footnote{Si veda la sezione~\ref{sez:funzione_di_autocorrelazione_3}.}
\[
\Phi_x \left( \tau \right) = {\mathcal{F}}^{-1} \left\{ G_x \left( f \right) \right\} = \lim_{T \to + \infty} \frac{1}{T} \int_{- \frac{T}{2}}^{\frac{T}{2}} x \left( t + \tau \right) x^* \left( t \right) dt = \langle x \left( t + \tau \right) x^* \left( t \right) \rangle
\]
\item autocorrelazione per processi casuali:\footnote{Si veda la sezione~\ref{sez:media_e_autocorrelazione}.}
\[
R_X \left( \tau \right) \triangleq E \left( X \left( t \right) X^* \left( t + \tau \right) \right)
\]
\end{itemize}

\subsection{Ergodicità per l'autocorrelazione}
Un processo $X \left( t \right)$ è \textbf{ergodico per l'autocorrelazione} se la sua autocorrelazione $R_X \left( \tau \right)$ coincide con l'autocorrelazione $\Phi_x \left( \tau \right)$ di una sua qualsiasi realizzazione:
\[
R_X \left( \tau \right) = \Phi_x \left( \tau \right) \quad \forall i
\]
dove $\Phi_x \left( \tau \right)$ è l'autocorrelazione della realizzazione $X \left( t ; s_i \right)$:
\[
\Phi_x \left( \tau \right) = \lim_{T \to + \infty} \frac{1}{T} \int_{- \frac{T}{2}}^{\frac{T}{2}} X \left( t + \tau ; s_i \right) X^* \left( t ; s_i \right) dt
\]

Se il processo $X \left( t \right)$ è ergodico per l'autocorrelazione, allora lo spettro di potenza $S_X \left( f \right)$ può essere valutato a partire da una sua qualsiasi realizzazione:
\[
S_X \left( f \right) \triangleq \mathcal{F} \left\{ R_X \left( \tau \right) \right\} = G_x \left( f \right) = \mathcal{F} \left\{ \Phi_x \left( \tau \right) \right\}
\]

\subsubsection{Ergodicità per l'autocorrelazione e stazionarietà}
\begin{itemize}
\item Il processo è stazionario per la sua autocorrelazione $\Phi_x \left( \tau \right)$ se questa non dipende dalla realizzazione.
\item Il processo è stazionario in senso stretto di ordine 2\footnote{Si veda la sezione~\ref{sez:wss}.} se la sua autocorrelazione $R_X \left( \tau \right)$ dipende solo da $\tau$.
\end{itemize}

L'ergodicità per l'autocorrelazione implica la stazionarietà per l'autocorrelazione, ma la stazionarietà per l'autocorrelazione \ul{non} implica l'ergodicità per l'autocorrelazione.