\chapter{Segnali e vettori}
Il \textbf{segnale} è una funzione complessa in funzione del tempo che definisce la "forma" del segnale.

\paragraph{Operazioni sui segnali}
\begin{itemize}
\item trasmissione: il trasporto da un punto all'altro dello spazio del segnale;
\item memorizzazione: il segnale è fruibile anche a distanza di tempo;
\item elaborazione: eliminazione del rumore, combinazione di più segnali\textellipsis
\end{itemize}

\paragraph{Esempi di segnali}
\begin{itemize}
\item \textbf{segnale elettrico:} costituito da una tensione o una corrente variante nel tempo, spesso generate da trasduttori, ossia dispositivi che permettono di misurare una grandezza scalare (es. temperatura, altezza, velocità) convertendola in un segnale elettrico;
\item \textbf{segnale vocale}: si misura fisicamente come variazione della pressione dell'aria in funzione del tempo;
\item \textbf{segnale video:} è più complesso perché è necessario \textbf{discretizzare} due delle tre variabili indipendenti $x$, $y$ e $t$ e definire le informazioni sul colore (o sulla luminosità se in bianco e nero).
\end{itemize}

Nel caso di un segnale video, discretizzare il tempo $t$ corrisponde a considerare i singoli fotogrammi, e discretizzare le coordinate $y$ significa suddividere il fotogramma in righe orizzontali.

In generale, una \textbf{sequenza} $f_n$ o $f \left( n \right)$ è la rappresentazione matematica discretizzata nel tempo del segnale di funzione $f$.

Il \textbf{convertitore A/D} serve per digitalizzare un segnale analogico:
\begin{itemize}
\item \textbf{campionamento:} il segnale viene campionato in base all'intervallo di campionamento $\Delta t$ scelto;
\item \textbf{quantizzazione:} il quantizzatore traduce ogni valore scalare campionato in un simbolo che appartiene a un alfabeto di cardinalità finita, cioè lo approssima al valore più vicino tra quelli scelti da un insieme finito.
\end{itemize}

Il \textbf{processo casuale}\footnote{Si veda il capitolo~\ref{cap10}.} è lo strumento matematico che definisce le caratteristiche di una certa classe di segnale (vocali, video, dati\textellipsis). Nel caso dei segnali vocali, teoricamente si dovrebbe registrare un certo numero statistico di parlatori e cercare di capire quali caratteristiche (come la frequenza) sono proprie di un segnale vocale, associando a ciascuna caratteristica di ciascun parlatore una probabilità.

\section{Segnali analogici tempo-continui}
Un segnale analogico tempo-continuo è descritto da una funzione complessa $x \left( t \right)$, che si rappresenta graficamente nelle due parti reale $x_R \left( t \right)$ e immaginaria $x_I \left( t \right)$.

Un segnale è \textbf{a supporto limitato} se la sua funzione è nulla al di fuori di un intervallo finito $\left( a , \, b \right)$ detto supporto.

Un segnale è \textbf{ad ampiezza limitata} se la funzione assume valori compresi in un intervallo finito.

Un \textbf{segnale fisico} si distingue dal segnale matematico per il fatto che è sia ad ampiezza limitata sia a supporto limitato.

I \textbf{segnali impulsivi} divergono ad un'ampiezza illimitata all'interno di un supporto infinitesimo.

\subsection{Energia e potenza media}
L'\textbf{energia} di un segnale $x \left( t \right)$ vale:
\[
 E \left( x  \right) \triangleq \int_{- \infty}^{+\infty} { \left| x  \right| }^2 dt
\]

Se l'integrale nella definizione di energia diverge, si prende in considerazione la \textbf{potenza media} di un segnale:
\[
 P \left( x  \right) \triangleq \lim_{a \rightarrow + \infty} \frac {1} {2a} \int_{-a}^{a} { \left| x  \right| }^2 dt
\]

In questo caso ${\left| x  \right|}^2 = x  x^*$ è detta \textbf{potenza istantanea}.

Un segnale fisico ha energia finita. I segnali a energia finita hanno potenza media nulla.

\subsection{Periodicità}
\label{sez:periodicita}
Un segnale è \textbf{periodico} di periodo $T$ e funzione $x \left( t \right)$:
\[
 x \left( t \right) = \sum_{n = - \infty}^{+ \infty} x_T \left( t - n T \right)
\]
se vale la proprietà seguente:
\[
 \exists T : \, x \left( t \right) = x \left( t + T \right) \; \forall t
\]

Un segnale \textbf{aperiodico} si può pensare come come un segnale periodico di periodo $T \rightarrow + \infty$.

\paragraph{Energia} L'energia $E \left( x \left( t \right) \right)$ di un segnale periodico è infinita.\footnote{Nel caso ultraparticolare di un segnale identicamente nullo, l'energia converge a 0.}
\begin{framed}
\paragraph{Dimostrazione}
\[
 E \left( x \left( t \right) \right) \triangleq \int_{- \infty}^{+\infty} { \left| x \left( t \right) \right| }^2 dt = \int_{- \infty}^{+ \infty} {\left| \sum_{n = - \infty}^{+ \infty} x_T \left( t - n T \right) \right|}^2 dt =
\]
\[
= \int_{- \infty}^{+ \infty} \sum_{n = - \infty}^{+ \infty} x_T \left( t - nT \right) \sum_{m = - \infty}^{+ \infty} x_T^* \left( t - mT \right) = \sum_{n = - \infty}^{+ \infty} \sum_{m = - \infty}^{+ \infty} \int_{- \infty}^{+ \infty} x_T \left( t - nT \right) x_T^* \left( t - mT \right) =
\]
\[
= \sum_{n = m = - \infty}^ {+ \infty} \int_{- \infty}^{+ \infty} x_T \left( t - n T \right) x_T^* \left( t - m T \right) dt + \sum_{n = - \infty}^{+ \infty} \sum_{m \neq n} \int_{- \infty}^{+ \infty} x_T \left( t - n T \right) x_T^* \left( t - mT \right) dt =
\]

Siccome il prodotto nel secondo integrale è sempre zero perché le due funzioni hanno supporto disgiunto:
\[
= \sum_{n = - \infty}^ {+ \infty} \int_{- \infty}^{+ \infty} x_T \left( t - n T \right) x_T^* \left( t - n T \right) dt + 0 = \sum_{n = - \infty}^{+ \infty} \int_{- \infty}^{+ \infty} {\left| x_T \left( t - n T \right) \right|}^2 dt =
\]

Poiché $x \left( t \right)$ è un segnale periodico:
\[
= \sum_{n = - \infty}^{+ \infty} E \left( x_T \left( t \right) \right)  \rightarrow + \infty
\]
\end{framed}

\paragraph{Potenza media}
La potenza media $P \left( x \left( t \right) \right)$ di un segnale periodico dipende dall'energia del segnale all'interno di un singolo periodo $E \left( x_T \left( t \right) \right)$:
\[
 P \left( x \left( t \right) \right) = \frac{1}{T} E \left( x_T \left( t \right) \right)
\]
\begin{framed}
\paragraph{Dimostrazione}
\[
 P \left( x \left( t \right) \right) = \lim_{m \rightarrow + \infty} \frac{1}{2mT} \int_{- m T}^{m T} {\left| \sum_{n = - \infty}^{+ \infty} x_T \left( t - n T \right) \right|}^2 dt =
\]
\[
= \lim_{m \rightarrow + \infty} \frac{1}{2mT} \sum_{n = -m}^{m - 1} \int_{-mT}^{mT} {\left| x_T \left( t - n T \right) \right|}^2 dt = \lim_{m \rightarrow + \infty} \frac{1}{2mT} \sum_{n = -m}^{m - 1} E \left( x_T \left( t \right) \right) =
\]

Poiché se per esempio $m = 1$ si ha $\sum_{n = -1}^{0} E \left( x_T \left( t \right) \right) = 2 E \left( x_T \left( t \right) \right)$:
\[
= \lim_{m \rightarrow + \infty} \frac{2m E \left( x_T \left( t \right) \right)}{2mT} = \frac{1}{T} E \left( x_T \left( t \right) \right)
\]
\end{framed}

La presenza di uno o più impulsi non fa diventare infinita la potenza.

\section{Spazio dei segnali}
Lo spazio dei segnali può essere visto come uno spazio vettoriale: un segnale può essere costruito a partire da più segnali elementari così come un vettore può essere costruito a partire da più vettori.

\subsection{Distanza}
Uno \textbf{spazio metrico} è un insieme di elementi su cui è possibile definire una distanza. La \textbf{distanza} ha le seguenti proprietà:
\begin{itemize}
\item non negativa: $d \left( x , y \right) \geq 0 \; \forall x , \, y$
\item simmetrica: $d \left( x , y \right) = d \left( y , x \right)$
\item $d \left( x , y \right) = 0 \Rightarrow x = y$
\item disuguaglianza triangolare: $d \left( x , z \right) \leq d \left( x , y \right) + d \left( y , z \right)$
\end{itemize}

Lo spazio dei segnali è uno spazio metrico.

La distanza è utile nel confronto di due segnali $x \left( t \right)$ e $y \left( t \right)$:
\[
 d \left( x , y \right) = \int_{- \infty}^{+ \infty} \left| x  - y \right| dt
\]

Si usa di solito la distanza euclidea:
\[
d \left( x , y \right) = \left \|  x - y \right \|_{2} = \sqrt{\int_{- \infty}^{+ \infty} {\left| x - y \right|}^2 dt}
\]

\subsection{Prodotto scalare}
Nello spazio dei numeri complessi il \textbf{prodotto scalare} è così definito:
\[
\langle \mathbf x , \mathbf y \rangle = \langle \left( x_1 , \ldots , x_n \right) , \left( y_1 , \ldots , y_n \right) \rangle \triangleq \sum_{i = 1}^n x_i y_i^* \in \mathbb{C} , \quad \mathbf x , \mathbf y \in {\mathbb{C}}^n
\]

Nello spazio dei segnali il prodotto scalare è così definito:
\[
\langle x , y \rangle \triangleq \int_{- \infty}^{+ \infty} x y^* dt
\]

\subsection{Norma}
La \textbf{norma} nello spazio dei segnali è così definita:
\[
 \| x \| \triangleq \sqrt{\langle x , x \rangle} = \sqrt{\int_{- \infty}^{+ \infty} x x^* dt}
\]
e ricordando che nei numeri complessi vale ${\left| z \right|}^2 = z z^*$:
\[
 \| x \| = \sqrt{\int_{- \infty}^{+ \infty} {\left| x \right|}^2 dt} = \sqrt{E \left( x \right)} \Rightarrow E \left( x \right) = {\| x \|}^2
\]

\subsection{Ortogonalità}
Secondo la \textbf{disuguaglianza di Schwarz}, il modulo del prodotto scalare tra due vettori al quadrato è sempre minore o uguale del prodotto delle loro energie:
\[
{\left| \langle \mathbf x , \mathbf y \rangle \right|}^2 \leq {\| \mathbf x \|}^2 {\| \mathbf y \|}^2
\]
da cui deriva:
\[
 0 \leq \frac{\langle \mathbf x , \mathbf y \rangle}{\| \mathbf x \| \| \mathbf y \|} \leq 1
\]

L'uguaglianza vale quando $\mathbf x$ e $\mathbf y$ sono proporzionali:
\[
{\left| \langle \mathbf x , \mathbf y \rangle \right|}^2 = {\| \mathbf x \|}^2 {\| \mathbf y \|}^2 \Leftrightarrow \mathbf x = \alpha \mathbf y
\]

L'\textbf{angolo} $\theta$ tra due segnali $x \left( t \right)$ e $y \left( t \right)$ è così definito:
\[
\cos{\theta} \triangleq \frac{\langle x , y \rangle}{\| x \| \| y \|}
\]

Due segnali $x \left( t \right)$ e $y \left( t \right)$ si dicono \textbf{ortogonali} tra loro se l'angolo $\theta$ è nullo, cioè se il loro prodotto scalare è nullo:\footnote{Si suppone che le energie di $x \left( t \right)$ e $y \left( t \right)$ non siano identicamente nulle.}
\[
 \theta = 0 \Rightarrow \langle x , y \rangle = 0
\]

L'energia della somma di due segnali $x \left( t \right)$ e $y \left( t \right)$ è data da:
\[
E \left( x + y \right) = E \left( x \right) + E \left( y \right) + 2 \Re{\langle x , y \rangle}
\]
\begin{framed}
\paragraph{Dimostrazione}
\[
{\| \mathbf x + \mathbf y \|}^2 = \langle \mathbf x + \mathbf y , \mathbf x + \mathbf y \rangle = \langle \mathbf x , \mathbf x \rangle + \langle \mathbf y , \mathbf y \rangle + \langle \mathbf x , \mathbf y \rangle + \langle \mathbf y , \mathbf x \rangle = {\| \mathbf x \|}^2 + {\| \mathbf y \|}^2 + \langle \mathbf x , \mathbf y \rangle + {\langle \mathbf x , \mathbf y \rangle}^* =
\]
\[
= {\| \mathbf x \|}^2 + {\| \mathbf y \|}^2 + 2 \Re{\left[ \langle \mathbf x , \mathbf y \rangle \right]}
\]

\end{framed}

Se i due segnali sono ortogonali:
\[
\langle x , y \rangle = 0 \Rightarrow E \left( x + y \right) = E \left( x \right) + E \left( y \right)
\]

\subsection{Basi ortonormali}
Una coppia di vettori $\left( {\mathbf w}_i , {\mathbf w}_j \right)$ appartiene a una \textbf{base ortonormale} se e solo se:
\begin{itemize}
\item ${\mathbf w}_i$ e ${\mathbf w}_j$ sono ortogonali tra loro:
\[
\langle {\mathbf w}_i , {\mathbf w}_j \rangle = 0 \quad \forall i \neq j
\]

\item ${\mathbf w}_i$ e ${\mathbf w}_j$ hanno entrambi norma unitaria:
\[
\| {\mathbf w}_i \| = 1 \quad \forall i
\]
\end{itemize}

Queste due condizioni possono essere riassunte da questa relazione:
\[
\langle {\mathbf w}_i , {\mathbf w}_j \rangle = \delta_{ij}
\]
dove $\delta_{ij}$ è la \textbf{delta di Kronecker}:
\[
\delta_{ij} = \begin{cases} 1 \quad \text{se} \; i = j \\
0 \quad \text{altrimenti} \end{cases}
\]

Data una base ortonormale $\left( {\mathbf w}_1, \ldots, {\mathbf w}_n \right)$, un generico vettore $\mathbf x$ può essere rappresentato come combinazione lineare degli elementi della base:
\[
\mathbf x = \left( x_1 , \ldots , x_n \right) = \sum_{i = 1}^n \langle \mathbf x , {\mathbf w}_i \rangle {\mathbf w}_i = \sum_{i = 1}^n x_i {\mathbf w}_i
\]

Nello spazio dei segnali esistono infinite basi ortonormali: a partire da una qualsiasi base ortonormale, è possibile ottenere un'altra base ortonormale applicando una rotazione di un certo angolo $\theta$ a tutti gli elementi della base. Ad esempio, nello spazio euclideo a 2 dimensioni si applica la trasformazione unitaria partendo dalla base canonica:
\[
\left\{ \begin{pmatrix} 1 \\ 0 \end{pmatrix} , \begin{pmatrix} 0 \\ 1 \end{pmatrix} \right\} \times \begin{pmatrix} \cos{\theta} & \sin{\theta} \\ - \sin{\theta} & \cos{\theta} \end{pmatrix} = \left\{ \begin{pmatrix}  \cos{\theta} \\ \sin{\theta} \end{pmatrix} , \begin{pmatrix} - \sin{\theta} \\ \cos{\theta} \end{pmatrix} \right\}
\]

Fissata una delle possibili basi ortonormali, si può stabilire una corrispondenza biunivoca tra i segnali ed uno spazio vettoriale euclideo a $n$ dimensioni, associando a ogni segnale $x \left( t \right)$ il vettore $\mathbf x$ a $n$ dimensioni costituito dai suoi coefficienti:
\[
x \left( t \right) = \sum_{i = 1}^n x_i w_i \left( t \right) \longleftrightarrow \mathbf x = \left( x_1 , \ldots , x_n \right)
\]

\section{Approssimazione di un segnale}
Lo spazio dei segnali in realtà ha dimensione infinita, cioè per rappresentare tutti i segnali possibili sarebbe necessaria una base costituita da infiniti versori $\Rightarrow$ si può semplificare approssimando un segnale generico $\mathbf x$ a un segnale $\mathbf{\hat{x}}$, formato dalla combinazione lineare dei versori ${\mathbf w}_i$ che sono basi ortonormali di uno spazio ridotto di dimensioni finite $n$. Si dimostra che la migliore approssimazione, corrispondente alla minima distanza euclidea dal segnale di partenza, si ottiene se i coefficienti della combinazione lineare coincidono con i prodotti scalari tra il segnale generico $\mathbf x$ stesso e i versori ${\mathbf w}_i$ della base:
\[
\mathbf{\hat{x}} = \sum_{i = 1}^n \langle \mathbf x , {\mathbf w}_i \rangle {\mathbf w}_i
\]

\subsection[Semplificazione formule]{Semplificazione formule\footnote{In questa sezione si ritorna temporaneamente per comodità alla vecchia notazione per i segnali.}}
Definendo una base ortonormale di $n$ elementi è possibile semplificare il calcolo del prodotto scalare, della distanza e dell'energia.

\paragraph{Prodotto scalare}
\[
\langle x , y \rangle \triangleq \int_{- \infty}^{+ \infty} x y^* dt = \sum_{i = 1}^n x_i y_i^*
\]
\begin{framed}
\paragraph{Dimostrazione}
\[
\langle x \left( t \right), y \left( t \right) \rangle = \langle \sum_{i = 1}^n x_i w_i \left( t \right) , \sum_{j = 1}^n y_j w_j \left( t \right) \rangle = \int \sum_{i = 1}^n x_i^* w_i^* \left( t \right) \sum_{j = 1}^n y_j^* w_j^* \left( t \right) dt =
\]

Per linearità:
\[
= \sum_{i = 1}^n \sum_{j = 1}^n x_i y_j^* \int w_i \left( t \right) w_j^* \left( t \right) dt = \sum_{i = 1}^n \sum_{j = 1}^n x_i y_j^* \delta_{ij} = \sum_{i = 1}^n x_i y_i^*
\]
\end{framed}

\paragraph{Energia}
\[
E \left( x \right) \triangleq \int_{- \infty}^{+\infty} { \left| x  \right| }^2 dt = \sum_{i = 1}^n {\left| x_i \right|}^2
\]
\begin{framed}
\[
E \left( x \right) = E \left( \sum_{i = 1}^n \langle x , w_i \rangle w_i \right) =
\]

Ricordando la proprietà dell'energia della somma di segnali ortogonali:
\[
= \sum_{i = 1}^n E \left( \langle x , w_i \rangle w_i \right) =
\]

Portando fuori una costante dall'integrale della definizione di energia:
\[
= \sum_{i = 1}^n {\left| \langle x , w_i \rangle \right|}^2 E \left( w_i \right) =
\]

Ricordando che i segnali $w_i \left( t \right)$ hanno norma unitaria:
\[
= \sum_{i = 1}^n {\left| \langle x , w_i \rangle \right|}^2 = \sum_{i = 1}^n {\left| x_i \right|}^2
\]
\end{framed}

\paragraph{Distanza}
\[
d \left( x , y \right) \triangleq \sqrt{\int_{- \infty}^{+ \infty} {\left| x - y \right|}^2 dt} = \sqrt{\sum_{i = 1}^n {\left| x_i - y_i \right|}^2}
\]

\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|}
  \hline
  & \textbf{Definizione} & \textbf{Segnale} & \textbf{Vettore} \\
  \hline
  \textbf{Prodotto scalare} & $\displaystyle \langle \mathbf x , \mathbf y \rangle$ & $\displaystyle \int_{- \infty}^{+ \infty} x \left( t \right) y^* \left( t \right) dt$ & $\displaystyle \sum_{i=1}^n x_i y_i^*$ \\
  \hline
  \textbf{Energia} & $\displaystyle E \left( \mathbf x \right) \triangleq \langle \mathbf x , \mathbf x \rangle$ & $\displaystyle \int {\left| x \left( t \right) \right|}^2 dt$ & $\displaystyle \sum_{i=1}^n {\left| x_i \right|}^2$ \\
  \hline
  \textbf{Norma} & $\displaystyle \left\| \mathbf x \right\| \triangleq \sqrt{E \left( \mathbf x \right)} = \sqrt{\langle \mathbf x,  \mathbf x \rangle}$ & $\displaystyle \sqrt{\int {\left| x \left( t \right) \right|}^2 dt}$ & $\displaystyle \sqrt{\sum_{i=1}^n {\left| x_i \right|}^2}$ \\
  \hline
  \textbf{Distanza} & $\displaystyle d \left( \mathbf x, \mathbf y \right) \triangleq \left\| \mathbf x - \mathbf y \right\|$ & $\displaystyle \sqrt{\int {\left| x \left( t \right) - y \left( t \right) \right|}^2 dt}$ & $\displaystyle \sqrt{\sum_{i=1}^n {\left| x_i - y_i \right|}^2}$ \\
  \hline
 \end{tabular}}
\end{table}

\subsection{Procedura di Gram-Schmidt}
\begin{figure}
	\centering
	\includegraphics[scale=0.4]{pic/01/Proiezione_segnale_approssimato}
	\caption{In questo esempio il segnale $\mathbf x$ viene approssimato in uno spazio bidimensionale generato dai due versori ${\mathbf w}_1$ e ${\mathbf w}_2$.}
\end{figure}

\noindent
Il segnale $\mathbf x$ perde un po' di energia nella proiezione su $\mathbf{\hat{x}}$:
\[
E \left( \mathbf e \right) = E \left( \mathbf x \right) - \sum_{i = 1}^n {\left| \langle \mathbf x , {\mathbf w}_i \rangle \right|}^2 \geq 0 \, , \quad \mathbf e = \mathbf x - \mathbf{\hat{x}}
\]
\begin{framed}
\[
E \left( \mathbf x \right) = E \left( \mathbf e + \mathbf {\hat{x}} \right) =
\]

Poiché il segnale errore $\mathbf e$, che è il segnale differenza, è ortogonale al segnale approssimante $\mathbf {\hat{x}}$:
\[
= E \left( \mathbf e \right) + E \left( \mathbf{\hat{x}} \right) = E \left( \mathbf e \right) + \sum_{i = 1}^n {\left| \langle \mathbf x , {\mathbf w}_i \rangle \right|}^2
\]
\end{framed}

Si ricava la \textbf{diseguaglianza di Bessel}:
\[
E \left( \mathbf x \right) \geq \sum_{i = 1}^n {\left| \langle \mathbf x , {\mathbf w}_i \rangle \right|}^2
\]

Se il segnale $\mathbf x$ è descritto da una base completa, vale l'\textbf{uguaglianza di Parseval}:
\[
E \left( \mathbf x \right) = \sum_{i = 1}^n {\left| \langle \mathbf x , {\mathbf w}_i \rangle \right|}^2
\]

In uno spazio vettoriale a $M$ dimensioni, cioè di cardinalità $M$, si ha un insieme finito di vettori $\left\{ {\mathbf x}_1 , {\mathbf x}_2 , \ldots , {\mathbf x}_M \right\}$. La \textbf{procedura di Gram-Schmidt} permette di trovare il minimo numero $N \leq M$, detto dimensionalità, di versori $\left\{ {\mathbf w}_1 , {\mathbf w}_2 , \ldots , {\mathbf w}_N \right\}$, ortonormali tra di loro, necessario per formare una base per questi vettori:
\[
\begin{array}{l}
{\mathbf w}_1 \cdot \| {\mathbf{\hat{w}}}_1 \| = {\mathbf{\hat{w}}}_1 = {\mathbf x}_1 \\
{\mathbf w}_2 \cdot \| {\mathbf{\hat{w}}}_2 \| = {\mathbf{\hat{w}}}_2 = {\mathbf x}_2 - {\mathbf{\hat{x}}}_2 = {\mathbf x}_2 - \langle {\mathbf x}_2 , {\mathbf w}_1 \rangle {\mathbf w}_1 \\
\vdots \\
{\mathbf w}_i \cdot \| {\mathbf{\hat{w}}}_i \| = {\mathbf{\hat{w}}}_i = {\mathbf x}_i - {\mathbf{\hat{x}}}_i = {\mathbf x}_i - \sum_{k=1}^{i - 1} \langle {\mathbf x}_i , {\mathbf w}_k \rangle {\mathbf w}_k \\
\vdots \\
{\mathbf w}_N \cdot \| {\mathbf{\hat{w}}}_N \| = {\mathbf{\hat{w}}}_2 = {\mathbf x}_N - {\mathbf{\hat{x}}}_N = {\mathbf x}_N - \sum_{k=1}^{N - 1} \langle {\mathbf x}_N , {\mathbf w}_k \rangle {\mathbf w}_k
\end{array}
\]

L'algoritmo termina alla $N$-esima iterazione quando il vettore errore $\mathbf e$ è nullo, ovvero quando il vettore proiezione $\mathbf{\hat{x}}_N$ è linearmente dipendente rispetto al vettore ${\mathbf{x}}_N$ e non si genera un nuovo versore. Se $N < M$ significa che si è riusciti a introdurre una semplificazione. Cambiando l'ordine dei vettori considerati si possono ottenere versori diversi, ma la dimensionalità $N$ non varia.
\FloatBarrier

\paragraph{Esempio}
Si considerano due vettori ${\mathbf x}_1$ e ${\mathbf x}_2$ nello spazio bidimensionale ($M = 2$):
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.4]{pic/01/Gram-Schmidt1}
\end{figure}
\begin{enumerate}
\item viene scelto per primo il vettore ${\mathbf x}_1$:
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.4]{pic/01/Gram-Schmidt2}
\end{figure}

\item viene scelto per primo il vettore ${\mathbf x}_2$:
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.4]{pic/01/Gram-Schmidt3}
\end{figure}
\end{enumerate}
