\chapter{Stazionarietà}
\section{Stazionarietà}
Un processo è \textbf{stazionario} se la traslazione di una sua realizzazione, per un qualsiasi intervallo di tempo, produce un'altra sua possibile realizzazione, e se tutte queste realizzazioni hanno la stessa probabilità:
\[
\text{se } x \left( t \right) \in \mathcal{P} \Rightarrow \begin{cases} x \left( t - t_0 \right) \in \mathcal{P} \\
P \left( x \left( t \right) \right) = P \left( x \left( t - t_0 \right) \right) \end{cases} \quad \forall t_0
\]

\paragraph{Esempi}
\begin{itemize}
\item processi stazionari: sinusoide con fase variabile casuale, rumore termico
\item processi non stazionari: segnale numerico, segnale sample \&\ hold, sinusoide con ampiezza variabile casuale, segnale determinato
\end{itemize}

\subsection{Stazionarietà in senso stretto}
Siccome per la densità di probabilità congiunta $f_{\mathbf{X}}$ vale:
\[
f_{\mathbf{X}} \left( x_1 , \ldots , x_n ; t_1 , \ldots , t_n \right) = f_{\mathbf{X}} \left( x_1 , \ldots , x_n ; t_1 + t_0 , \ldots , t_n + t_0 \right) \quad \forall t_0
\]
si può far coincidere il primo istante di tempo con l'origine ($t_0 = - t_1$) eliminando una variabile temporale $\Rightarrow$ le statistiche di ordine $n$ dipendono da $n - 1$ variabili, che rappresentano la differenza di tempo rispetto al primo campione, che si può sempre assumere nell'origine. Ad esempio, per un processo \textbf{stazionario in senso stretto} di ordine $n$ vale:
\[
\begin{array}{l}
f_{X_1} \left( x_1 ; t_1 \right) = f_{X_1} \left( x_1 ; t_1 - t_1 \right) = f_{X_1} \left( x_1 ; 0 \right) \\
f_{\mathbf{X}} \left( x_1, x_2 ; t_1, t_2 \right) = f_{\mathbf{X}} \left( x_1, x_2 ; 0 , t_2 - t_1 \right) = f_{\mathbf{X}} \left( x_1 , x_2 ; 0 , \tau \right) \\
\vdots \\
f_{\mathbf{X}} \left( x_1, \ldots , x_n ; t_1, \ldots , t_n \right) = f_{\mathbf{X}} \left( x_1 , \ldots , x_n ; \tau_1 , \ldots , \tau_{n - 1} \right) \\
\end{array}
\]

\subsection{Stazionarietà in senso lato (Wide Sense Stationary [WSS])}
\label{sez:wss}
\begin{itemize}
\item Se il processo è stazionario in senso stretto di ordine 1, la media $m_X \left( t \right)$ è una costante e non dipende dal tempo:
\[
m_X \left( t \right) = m_X
\]
\begin{framed}
\paragraph{Dimostrazione}
\[
m_X \left( t \right)  = \int x f_{X} \left( x ; t \right) dx = \int x f_X \left( x ; 0 \right) dx = m_X
\]
\end{framed}
\item Se il processo è stazionario in senso stretto di ordine 2, la funzione di autocorrelazione $R_X \left( t_1 , t_2 \right)$ dipende solo dalla differenza $\tau = t_2 - t_1$:
\[
R_X \left( t_ 1 , t_2 \right) = R_X \left( \tau \right) = E \left[ X \left( t \right) X^* \left( t + \tau \right) \right]
\]
\begin{framed}
\paragraph{Dimostrazione}
\[ R_X \left( t_1 , t_2 \right) = \int x_2 x_2^* f_X \left( x_1 , x_2 ; t_1 , t_2 \right) dx_1 dx_2 = \int x_1 x_2^* f_X \left( x_1 , x_2 ; 0 , \tau \right) dx_1 dx_2 = R_X \left( \tau \right) \]
\end{framed}
\end{itemize}

Un processo è \textbf{stazionario in senso lato} se la media $m_X \left( t \right)$ è una costante, e la funzione di autocorrelazione $R_X \left( t_1 , t_2 \right)$ dipende solo da $\tau$:
\[ \begin{cases} m_X \left( t \right) = m_X \\ R_X \left( t_ 1 , t_2 \right) = R_X \left( \tau \right) \end{cases} \]

La stazionarietà in senso lato \ul{non} implica la stazionarietà in senso stretto.

\section{Ciclostazionarietà}
Un processo è \textbf{ciclostazionario} se la traslazione di una sua realizzazione, per multipli di una costante finita $T$ detta \textbf{periodo di stazionarietà}, produce un'altra sua possibile realizzazione, e se tutte queste realizzazioni hanno la stessa probabilità:
\[
\exists T : \text{se } x \left( t \right) \in \mathcal{P} \Rightarrow \begin{cases} x \left( t - iT \right) \in \mathcal{P} \\
P \left( x \left( t \right) \right) = P \left( x \left( t - iT \right) \right) \end{cases} \quad \forall i \in \mathbb{Z}
\]

\paragraph{Esempi}
\begin{itemize}
\item processi ciclostazionari: segnale numerico, segnale sample \&\ hold, sinusoide con ampiezza variabile casuale, segnale determinato periodico, processi stazionari
\item processi non ciclostazionari: segnale determinato non periodico
\end{itemize}

\subsection{Ciclostazionarietà in senso stretto}
Un processo è \textbf{ciclostazionario in senso stretto} se la densità di probabilità congiunta $f_{\mathbf{X}}$ è periodica di periodo $T$:
\[
f_{\mathbf{X}} \left( x_1 , \ldots , x_n ; t_1 , \ldots , t_n \right) = f_{\mathbf{X}} \left( x_1 , \ldots , x_n ; t_1 - iT , \ldots , t_n - iT \right) \quad \forall i \in \mathbb{Z}
\]

\subsection{Ciclostazionarietà in senso lato}
Un processo è \textbf{ciclostazionario in senso lato} se la media $m_X \left( t \right)$ è periodica, e la funzione di autocorrelazione $R_X \left( t_1 , t_2 \right)$ è periodica rispetto a $t_1$ e $t_2$:
\[
\begin{cases} m_X \left( t \right) = m_X \left( t - iT \right) \\
R_X \left( t_1 , t_2 \right) = R_X \left( t_1 - T , t_2 - iT \right) \end{cases} \quad \forall i \in \mathbb{Z}
\]

\section{Stazionarizzazione}
La stazionarietà implica la ciclostazionarietà, ma non viceversa $\Rightarrow$ l'operazione di \textbf{stazionarizzazione} serve per trasformare un processo ciclostazionario in un processo stazionario: si aggiungono tutte le replice mancanti all'interno del periodo di ciclostazionarietà $T$, tramite la nuova variabile casuale $\theta$ che corrisponde al ritardo casuale uniforme. Questa operazione modifica il processo stesso e può essere fatta solo se ha senso nel sistema considerato: tipicamente si può fare se il sistema che lo processa è stazionario (tempo invariante).