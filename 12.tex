\chapter{Trasformazioni e spettro di potenza}
\section{Trasformazioni lineari di processi casuali}
Un sistema\footnote{Si veda il capitolo~\ref{cap4}.} deterministico che elabora un processo casuale $X \left( t \right)$ fornisce alla sua uscita un processo casuale $Y \left( t \right)$.

Se il sistema è lineare, la media:
\[
m_Y \left( t \right) \triangleq E \left( Y \left( t \right) \right)
\]
e la funzione di autocorrelazione:
\[ R_Y \left( t_1 , t_2 \right) \triangleq E \left( Y \left( t_1 \right) Y^* \left( t_2 \right) \right) \]
dell'uscita sono esprimibili rispettivamente in funzione della media e dell'autocorrelazione dell'ingresso $X \left( t \right)$.\footnote{Sotto certe condizioni sul processo di ingresso e sulle sue statistiche del secondo ordine.}

\subsection{Esempi}
\paragraph{Integrale}
\[ Y \left( t \right) = \int_{T_1}^{t} X \left( \tau \right) d \tau \Rightarrow \begin{cases} \displaystyle m_Y \left( t \right) = \int_{T_1}^t m_X \left( a \right) da \\
\displaystyle R_Y \left( t_1 , t_2 \right) = \int_{T_1}^{t_1} \int_{T_1}^{t_2} R_X \left( a , b \right) da db \end{cases} \]

\paragraph{Derivata}
\[ Y \left( t \right) = \frac{d}{dt} X \left( t \right) \Rightarrow \begin{cases} \displaystyle m_Y \left( t \right) = \frac{d}{dt} m_X \left( t \right) \\
 \displaystyle R_Y \left( t_1 , t_2 \right) = \frac{\partial^2}{\partial t_1 \partial t_2} R_X \left( t_1 , t_2 \right) \end{cases} \]

\noindent\rule{\linewidth}{0.2mm}
\vspace*{\dimexpr-\parskip-.5\baselineskip}

È spesso molto difficile determinare la distribuzione di probabilità del processo in uscita. Se il processo $X \left( t \right)$ in ingresso è gaussiano, allora il processo $Y \left( t \right)$ in uscita da un qualsiasi sistema lineare è ancora gaussiano.

\subsection{Trasformazioni lineari tempo-invarianti}
Se il sistema è LTI\footnote{Si veda la sezione~\ref{sez:sistemi_lineari_tempo_invarianti_LTI}.}, di funzione di trasferimento $H \left( f \right)$, e il processo di ingresso $X \left( t \right)$ è stazionario in senso lato, valgono le seguenti espressioni:
\begin{itemize}
\item $ \displaystyle m_Y = m_X H \left( 0 \right) = m_X \int_{- \infty}^{+ \infty} h \left( \tau \right) d \tau$
\item $\displaystyle R_Y \left( \tau \right) = R_X \left( \tau \right) * R_h \left( \tau \right)$
\item $\displaystyle R_{XY} \left( \tau \right) = R_X \left( \tau \right) * h \left( \tau \right)$
\end{itemize}

Si ricorda che la funzione di autocorrelazione\footnote{Si veda la sezione~\ref{sez:funzione_di_autocorrelazione}.} $R$ è definita diversamente per i segnali determinati:
\[
R_h \left( \tau \right) \triangleq \int_{- \infty}^{+ \infty} h \left( t + \tau \right) h^* \left( t \right) dt
\]

\subsection{Trasformazioni lineari tempo-varianti}
\subsubsection{Modulazione di ampiezza}
Il segnale analogico è rappresentato dal processo in ingresso $M \left( t \right)$ (messaggio). Nella \textbf{modulazione di ampiezza}, il messaggio modula l'ampiezza della sinusoide portante (segnale determinato):
\[
Y \left( t \right) = \left[ a_0 + a_1 M \left( t \right) \right] \cos{\left( 2 \pi f_0 t + \theta \right)}
\]

Sotto le ipotesi:
\begin{itemize}
\item il processo $M \left( t \right)$ è stazionario in senso lato;
\item il valor medio $E \left[ M \left( t \right) \right]$ è nullo;
\end{itemize}
si ottiene:
\[
\begin{cases} \displaystyle m_Y \left( t \right) = a_1 E \left[ M \left( t \right) \right] \cos{ \left( 2 \pi f_0 t + \varphi \right) } + a_0 \cos {\left( 2 \pi f_0 t + \varphi \right) } \\
\displaystyle R_Y \left( t_1 , t_2 \right) = \left[ a_0^2 + a_1^2 R_M \left( t_1 - t_2 \right) \right] \cos{\left( 2 \pi f_0 t_1 + \varphi \right) \cos{\left( 2 \pi f_0 t_2 + \varphi \right)}} \end{cases}
\]

Per stazionarizzare il processo in uscita $Y \left( t \right)$, la fase $\varphi$ diventa una variabile casuale uniforme in $\left[ - \pi , \pi \right]$:
\[ \begin{cases} \displaystyle m_{Y'} \left( t \right) = 0 \\
\displaystyle R_{Y'} \left( t_1 , t_2 \right) = \frac{1}{2} \left[ a_0^2 + a_1^2 R_M \left( \tau \right) \right] \cos{\left( 2 \pi f_0 \tau \right)} \end{cases} \]

\subsubsection{Modulazione di fase}
Nella \textbf{modulazione di fase}, il messaggio modula la fase della sinusoide portante:
\[
Y \left( t \right) = a_0 e^{j \left[ a_1 t + a_2 M \left( t \right) + \theta \right]} \Rightarrow  \begin{cases} \displaystyle m_Y \left( t \right) = a_0 E \left[ a_0 e^{j a_2 M \left( t \right)} e^{j \left( a_1 t + \theta \right)} \right] \\ \displaystyle R_Y \left( t_1 , t_2 \right) = a_0^2 e^{j a_1 \left( t_1 - t_2 \right)} C_{\xi} \left( a_2 \right) , \quad \xi = M \left( t_1 \right) - M \left( t_2 \right) \end{cases}
\]
dove $C_{\xi} \left( a \right)$ è la \textbf{funzione caratteristica}:
\[
C_{\xi} \left( a \right) \triangleq E \left( e^{j a \xi} \right) = \int_{- \infty}^{+ \infty} e^{jax} f_{\xi} \left( x \right) dx
\]

Nel caso di un processo gaussiano, stazionarizzando con $\theta$ variabile casuale uniforme in $\left[ - \pi , \pi \right]$:
\[
\begin{cases} \displaystyle m_{Y'} \left( t \right) = 0 \\
\displaystyle R_{Y'} \left( \tau \right) = a_0^2 e^{ja_1 \tau} e^{- a_2^2 \left[ R_M \left( 0 \right) - R_M \left( \tau \right) \right]} \end{cases}
\]
\begin{framed}
\paragraph{Dimostrazione}
La funzione caratteristica del processo gaussiano $\xi$ vale:
\[
\mathcal{F} \left\{ f_{\xi} \left( t \right) \right\} \triangleq \int_{- \infty}^{+ \infty} f_{\xi} \left( t \right) e^{-j 2 \pi f t} dt = \mathcal{F} \left\{ \frac{1}{\sqrt{2 \pi {\sigma}_{\xi}^2}} e^{- \frac{1}{2} {\left[ \frac{t - m_{\xi}}{{\sigma}_{\xi}} \right]}^2} \right\} =
\]
\[
= e^{- \frac{1}{2} {\left( 2 \pi f {\sigma}_{\xi} \right)}^2} e^{-j 2 \pi f m_{\xi}} \Rightarrow C_{\xi} \left( a_2 \right) = \int_{- \infty}^{+ \infty} e^{ja_2 x} f_{\xi} \left( x \right) dx = e^{- \frac{1}{2} {\left( a_2 {\sigma}_{\xi} \right)}^2} e^{-j a_2 m_{\xi}}
\]

Siccome $\xi$ è stazionario in senso lato:
\[
m_{\xi} = E \left( \xi \right) = 0
\]

Inoltre:
\[
{\sigma}_{\xi}^2 = E \left[ \xi^2 \right] = E \left[ {\left( M \left( t_1 \right) - M \left( t_2 \right) \right)}^2 \right] =
\]
\[
= E \left[ \left( M \left( t_1 \right) \right) \left( M \left( t_1 \right) \right) \right] + E \left[ \left( M \left( t_2 \right) \right) \left( M \left( t_2 \right) \right) \right] - E \left[ M \left( t_1 \right) M \left( t_2 \right) \right] =
\]
\[
= R_{M} \left( 0 \right) + R_{M} \left( 0 \right) - 2 R_{M} \left( t_1 - t_2 \right)
\]

\[
\mathcal{F} \left\{ f_{\xi} \left( t \right) \right\} = e^{- a_2^2 \left[ R_M \left( 0 \right) - R_M \left( \tau \right) \right]}
\]
\end{framed}

Se la sinusoide portante è reale:
\[
Y \left( t \right) = a_0 \cos{\left( a_1 t + a_2 M \left( t \right) + \theta \right)}
\]
la funzione di autocorrelazione (sempre nel caso gaussiano) vale:
\[
R_{Y'} \left( \tau \right) = \frac{1}{2} a_0^2 \cos{\left( a_1 \tau \right) e^{- a_2^2 \left[ R_M \left( 0 \right) - R_M \left( \tau \right) \right]}}
\]

\section{Densità spettrale di potenza}
La \textbf{densità spettrale di potenza}, o \textbf{spettro di potenza}, $S_X \left( f \right)$ di un processo $X \left( t \right)$ stazionario in senso lato è la trasformata di Fourier della funzione di autocorrelazione $R_X \left( \tau \right)$:
\[
S_X \left( f \right) \triangleq \mathcal{F} \left\{ R_X \left( \tau \right) \right\} = \int R_X \left( \tau \right) e^{-j2 \pi f \tau} d \tau
\]

\subsection{Proprietà}
\begin{itemize}
\item $S_X \left( f \right)$ è reale e pari;
\item $S_X \left( f \right)$ è sempre positiva;
\item lo spettro di potenza $S_Y \left( \tau \right)$ di un processo $Y \left( t \right)$ stazionario in senso lato in uscita da un sistema LTI è:
\[
R_Y \left( \tau \right) = R_X \left( \tau \right) * R_h \left( \tau \right) \Rightarrow S_Y \left( f \right) = {\left| H \left( f \right) \right|}^2 S_X \left( f \right)
\]
\item l'integrale di $S_X \left( f \right)$ coincide con la potenza media $P \left[ X \left( t \right) \right]$ del processo:
\[
P \left[ X \left( t \right) \right] = \int S_X \left( f \right) df = R_X \left( 0 \right) = E \left[ X^2 \left( t \right) \right]
\]
dove $E \left[ X^2 \left( t \right) \right]$ è il valore quadratico medio del processo.
\end{itemize}

\subsection{Interpretazione fisica}
Se il sistema LTI è un filtro $H \left( f \right)$ passabanda centrato alla frequenza $f_0$ con banda infinitesimale $\Delta$:
\begin{figure}[H]
	\centering
	\includegraphics[scale=0.65]{pic/12/Spettro_di_potenza.png}
\end{figure}
la densità spettrale $S_X \left( f_0 \right)$, centrata in $f_0$, del processo in ingresso $X \left( f \right)$ è proporzionale alla potenza media del processo in uscita $Y \left( f \right)$:
\[
E \left[ Y^2 \left( t \right) \right] = \int S_Y \left( f \right) df \approx 2 \Delta S_X \left( f_0 \right)
\]

\subsection{Rumore gaussiano bianco}
\begin{figure}
	\centering
	\includegraphics[scale=0.55]{pic/12/Rumore_gaussiano_bianco.png}
\end{figure}

\noindent
Il \textbf{rumore gaussiano bianco} (White Gaussian Noise [WGN]) è un modello teorico per il processo termico generato ai capi di una resistenza a temperatura $T$:
\begin{itemize}
\item il processo $X \left( t \right)$ è stazionario e gaussiano;
\item il valor medio è nullo;
\item la densità spettrale di potenza è costante con la frequenza:
\[
S_X \left( f \right) = \frac{N_0}{2} , \quad N_0 = k_B T
\]
\item la funzione di autocorrelazione $R_X \left( \tau \right)$:
\[
R_X \left( \tau \right) = {\mathcal{F}}^{-1} \left\{ S_X \left( f \right) \right\} = \frac{N_0}{2} \delta \left( \tau \right)
\]
\begin{itemize}
\item è nulla per $\tau = t_2 - t_1 \neq 0$ $\Rightarrow$ qualsiasi coppia di campioni non prelevati allo stesso istante è scorrelata, e quindi statisticamente indipendente;
\item diverge se la coppia di campioni è prelevata nello stesso istante di tempo ($\tau = t_2 - t_1 = 0$) $\Rightarrow$ il processo ha potenza media infinita.
\end{itemize}
\end{itemize}

Quando un rumore gaussiano bianco $X \left( t \right)$ entra in un sistema LTI $H \left( f \right)$, il processo di uscita $Y \left( t \right)$ è gaussiano colorato (CGN) con spettro di potenza non più costante:
\[
S_Y \left( f \right) = {\left| H \left( f \right) \right|}^2 S_X \left( f \right) = \frac{N_0}{2} {\left| H \left( f \right) \right|}^2
\]
%\FloatBarrier