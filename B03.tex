\chapter{Trasformata di Fourier a tempo discreto}
\section{Trasformata di Fourier a tempo discreto (DTFT)}
\subsection{Definizione di DTFT}
La \textbf{trasformata di Fourier a tempo discreto} (Discrete Time Fourier Transform [DTFT]) $X \left( e^{j2 \pi f} \right)$ è la trasformata di Fourier della sequenza $x \left( n \right)$:
\[
X \left( e^{j2 \pi f} \right) = \mathcal{F} \left\{ x \left( n \right) \right\} = \sum_{k = - \infty}^{+ \infty} x \left( k \right) e^{-j2 \pi f k} =  \sum_{k = - \infty}^{+ \infty} x \left( k \right) e^{-j \omega k}
\]
dove $\omega$ è la pulsazione discreta: $\omega = 2 \pi f$.
\begin{framed}
\paragraph{Dimostrazione}
Una sequenza $x \left( n \right)$ può essere espressa come la somma pesata dei campioni presi negli istanti di tempo $k$:
\[
x \left( n \right) = \sum_{k = - \infty}^{+ \infty} x \left( k \right) \delta \left( n - k \right)
\]

La trasformata di Fourier di $x \left( n \right)$ pertanto vale:
\[
\mathcal{F} \left\{ x \left( n \right) \right\} = \mathcal{F} \left\{ \sum_{k = - \infty}^{+ \infty} x \left( k \right) \delta \left( n - k \right) \right\} = \sum_{k = - \infty}^{+ \infty} x \left( k \right) \mathcal{F} \left\{\delta \left( n - k \right) \right\} = \sum_{k = - \infty}^{+ \infty} x \left( k \right) e^{-j2 \pi f k}
\]
\end{framed}

La DTFT viene indicata con $X \left( e^{j2 \pi f} \right)$ anziché con $X \left( f \right)$ per distinguerla dalla trasformata di Fourier di un segnale continuo, ma è a tutti gli effetti in funzione della variabile \ul{continua} $f$.

La DTFT è in realtà il caso particolare per $T_c = 1$ della trasformata di Fourier di un segnale analogico $x \left( t \right)$ campionato\footnote{Si veda la sezione~\ref{sez:campionamento_nel_tempo_e_periodicizzazione_in_frequenza}.} con frequenza di campionamento $f_c = \frac{1}{T_c}$:
\[
X_c \left( f \right) = \mathcal{F} \left\{ \sum_{k = - \infty}^{+ \infty} x \left( k T_c \right) \delta \left( t - k T_c \right) \right\} = \sum_{k = - \infty}^{+ \infty} x \left( k T_c \right) e^{-j2 \pi f k T_c}
\]
che è periodica di periodo $f_c$ $\Rightarrow$ la DTFT è periodica di periodo:
\[
\begin{cases} \displaystyle f_c = \frac{1}{T_c} = 1 \\
\omega = 2 \pi f_c = 2 \pi \end{cases}
\]

\subsection{Inversione della DTFT (IDTFT)}
Siccome la DTFT $X \left( e^{j \omega} \right)$ è periodica, i coefficienti $x \left( k \right)$ possono essere intepretati come i coefficienti $\mu_k$ dello sviluppo in serie di Fourier\footnote{Si veda la sezione~\ref{sez:serie_di_fourier}.} della DTFT:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|}
  \hline
  \multirow{2}{*}{\textbf{Tempo continuo}} & \multicolumn{2}{|c|}{\textbf{Tempo discreto}} \\
  \cline{2-3}
  & \textbf{in funzione di $f$} & \textbf{in funzione di $\omega$} \\
  \hline
  $\displaystyle x \left( t \right)  = \sum_{k = - \infty}^{+ \infty} \mu_k e^{j \frac{2 \pi}{T} kt}$ & $\displaystyle X \left( e^{j 2 \pi f} \right) = \sum_{k = - \infty}^{+ \infty} x \left( k \right) e^{-j 2 \pi f k}$ & $\displaystyle X \left( e^{j \omega} \right) = \sum_{k = - \infty}^{+ \infty} x \left( k \right) e^{-j \omega k}$ \\
  $\displaystyle \mu_k = \frac{1}{T} \int_{- \frac{T}{2}}^{+ \frac{T}{2}} x \left( t \right) e^{-j \frac{2 \pi}{T} k t} dt$ & $\displaystyle x \left( k \right) = \int_{- \frac{1}{2}}^{+ \frac{1}{2}} X \left( e^{j 2 \pi f} \right) e^{j2 \pi f k} df$ & $\displaystyle x \left( k \right) = \frac{1}{2 \pi} \int_{- \pi}^{+ \pi} X \left( e^{j \omega} \right) e^{j \omega k} d \omega$ \\
  \hline
 \end{tabular}}
 \caption{Trasformata di Fourier a tempo discreto inversa (IDTFT).}
\end{table}

\begin{framed}
\paragraph{Verifica dell'inversione}
\[ \int_{- \frac{1}{2}}^{+ \frac{1}{2}} X \left( e^{j 2 \pi f } \right) e^{j2 \pi f k} df = \int_{- \frac{1}{2}}^{+ \frac{1}{2}} \left[ \sum_{n = - \infty}^{+ \infty} x \left( n \right) e^{-j2 \pi f n} \right] e^{j2 \pi f k} df = \]
\[ = \sum_{n = - \infty}^{+ \infty} x \left( n \right) \int_{- \frac{1}{2}}^{+ {1 \over 2}} e^{-j 2 \pi f \left( n - k \right)} df = \sum_{n = - \infty}^{+ \infty} x \left( n \right) \frac{1}{-j2 \pi \left( n - k \right)} \left. e^{-j2 \pi f \left( n - k \right)} \right \vert_{- \frac{1}{2}}^{+ \frac{1}{2}} = \]
\[ = \sum_{n = - \infty}^{+ \infty} x \left( n \right) \frac{1}{j 2 \pi \left( n - k \right)} \left(  e^{j \pi \left( n - k \right)} - e^{-j \pi \left( n - k \right)} \right) = \]

Per la formula di Eulero:
\[
= \sum_{n = - \infty}^{+ \infty} x \left( n \right) \frac{\sin \left( \pi \left( n - k \right) \right)}{\pi \left( n - k \right)} = \sum_{n = - \infty}^{+ \infty } x \left( n \right) \mathrm{sinc} \left( n - k \right) = \sum_{n = - \infty}^{+ \infty} x \left( n \right) \delta \left( n - k \right) = x \left( k \right)
\]
\end{framed}

\subsection{Condizioni di esistenza}
Se la sequenza $x \left( k \right)$ è assolutamente sommabile, allora:
\begin{itemize}
\item esiste la sua DTFT:
\[
\sum_{k = - \infty}^{+ \infty} \left| x \left( k \right) \right| \in \mathbb{R} \Rightarrow \left| X \left( e^{j \omega} \right) \right| \in \mathbb{R} \quad \forall \omega
\]
\begin{framed}
\paragraph{Dimostrazione}
\[
\left| X \left( e^{j \omega} \right) \right| = \left| \sum_{k = - \infty}^{+ \infty} x \left( k \right) e^{- j \omega k} \right| \leq \sum_{k = - \infty}^{+ \infty} \left| x \left( k \right) e^{- j \omega k} \right| = \sum_{k = - \infty}^{+ \infty} \left| x \left( k \right) \right| \in \mathbb{R} \Rightarrow
\]
\[
\Rightarrow \left| X \left( e^{j \omega} \right) \right| \in \mathbb{R}
\]
\end{framed}
\item la sua energia è finita:
\[
\sum_{k = - \infty}^{+ \infty} \left| x \left( k \right) \right| \in \mathbb{R} \Rightarrow E_x = \sum_{k = - \infty}^{+ \infty} {\left| x \left( k \right) \right|}^2 \in \mathbb{R}
\]
\begin{framed}
\paragraph{Dimostrazione}
\[
E_x = \sum_{k = - \infty}^{+ \infty} {\left| x \left( k \right) \right|}^2 \leq {\left( \sum_{k = - \infty}^{+ \infty} \left| x \left( k \right) \right| \right)}^2 \in \mathbb{R} \Rightarrow E_x \in \mathbb{R}
\]
\end{framed}
\end{itemize}

\section{Proprietà della DTFT}
\subsection{Linearità}
La DTFT è un operatore \textbf{lineare}:
\[
z \left( n \right) =  a_1 \cdot x \left( n \right) + a_2 \cdot y \left( n \right) \Longleftrightarrow Z \left( e^{j 2 \pi f} \right) = a_1 \cdot X \left( e^{j 2 \pi f} \right) + a_2 \cdot Y \left( e^{j 2 \pi f} \right)
\]

\subsection{Ribaltamento}
Un \textbf{ribaltamento} della $x \left( n \right)$ corrisponde a calcolare la sua DTFT invertendo il segno della frequenza $f$:
\[
z \left( n \right) = x \left( - n \right) \Longleftrightarrow Z \left( e^{j 2 \pi f} \right) = X \left( e^{- j 2 \pi f} \right)
\]

\subsection{Ritardo}
Una \textbf{traslazione del tempo} della sequenza $x \left( n \right)$ corrisponde a moltiplicare la sua DTFT per un esponenziale complesso:
\[
z \left( n \right) = x \left( n - N \right) \Longleftrightarrow Z \left( e^{j 2 \pi f} \right) = X \left( e^{j 2 \pi f} \right) e^{- j 2 \pi f N}
\]

\subsection{Traslazione in frequenza (modulazione)}
Una \textbf{traslazione in frequenza} della DTFT di una sequenza $x \left( n \right)$ corrisponde a moltiplicare la sequenza per un esponenziale complesso:
\[
z \left( n \right) = x \left( n \right) \cdot e^{j 2 \pi f_0 n} \Longleftrightarrow Z \left( e^{j 2 \pi  f } \right) = X \left( e^{j 2 \pi \left( f - f_0 \right)} \right)
\]

\subsection{Derivazione in frequenza}
\[
z \left( n \right) = n \cdot x \left( n \right) \Longleftrightarrow - 2 \pi j \cdot Z \left( e^{j 2 \pi f} \right) = \frac{d }{df} X \left( e^{j 2 \pi f} \right)
\]

\subsection{Convoluzione lineare}
La \textbf{convoluzione} tra due sequenze $x \left( n \right)$ e $y \left( n \right)$ corrisponde al prodotto tra le singole DTFT:
\[
z \left(  n \right) = x \left ( n \right) * y \left( n \right) = \sum_{k = - \infty}^{+ \infty} x \left( k \right) y \left( n  - k \right) \Longleftrightarrow Z \left( e^{j 2 \pi f } \right) = X \left( e^{j2 \pi f} \right) \cdot Y \left( e^{j2 \pi f} \right)
\]

\subsection{Prodotto}
Il \textbf{prodotto} tra due sequenze $x \left( n \right)$ e $y \left( n \right)$ corrisponde alla convoluzione tra le singole DTFT, con estremi di integrazione $- \tfrac{1}{2}$ e $+ \tfrac{1}{2}$ grazie al fatto che la DTFT è periodica:
\[
z \left( n \right) = x \left( n \right) \cdot y \left( n \right) \Longleftrightarrow Z \left( e^{j 2 \pi f} \right) = X \left( e^{j 2 \pi f} \right) * Y \left( e^{j2 \pi f} \right) = \int_{- \frac{1}{2}}^{+ \frac{1}{2}} X \left( e^{j 2 \pi \eta} \right) Y \left( e^{j2 \pi \left( f - \eta \right) } \right) d \eta
\]

\subsection{Relazioni di parità}
Se la sequenza $x \left( n \right)$ è reale vale la simmetria hermitiana per le sue DTFT attorno alle frequenze $f = 0$ e $f = \tfrac{1}{2}$:
\[
\begin{cases} \displaystyle X \left( e^{j 2 \pi f} \right) = X^* \left( e^{-j2 \pi f} \right) \\
\displaystyle X \left( e^{j 2 \pi \left( f - \frac{1}{2} \right)} \right) = X^* \left( e^{- j 2 \pi \left( f + \frac{1}{2} \right)} \right) \end{cases} \Rightarrow
\]
\[
\Rightarrow  \begin{cases} X_R \left( e^{j2 \pi f} \right) + j X_I \left( e^{j 2 \pi f } \right) = X_R \left( e^{-j2 \pi f} \right) - j X_I \left( e^{-j2 \pi f} \right) \\
\displaystyle X_R \left( e^{j2 \pi \left( f - \frac{1}{2} \right)} \right) + j X_I \left( e^{j 2 \pi \left( f - \frac{1}{2} \right) } \right) = X_R \left( e^{-j2 \pi \left( f + \frac{1}{2} \right)} \right) - j X_I \left( e^{-j2 \pi \left( f + \frac{1}{2} \right)} \right) \end{cases}
\]
e quindi entrambe le DTFT hanno le seguenti relazioni di parità:
\begin{itemize}
\item la parte reale è pari:
\[
\begin{cases} \displaystyle X_R \left( e^{j 2 \pi f} \right) = X_R \left( e^{- j 2 \pi f} \right) \\ \displaystyle X_R \left( e^{j 2 \pi \left( f - \frac{1}{2} \right)} \right) = X_R \left( e^{- j 2 \pi \left( f + \frac{1}{2} \right)} \right) \end{cases}
\]
\item la parte immaginaria è dispari:
\[
\begin{cases} \displaystyle X_I \left( e^{j 2 \pi f} \right) = - X_I \left( e^{- j 2 \pi f} \right) \\ \displaystyle X_I \left( e^{j 2 \pi \left( f - \frac{1}{2} \right)} \right) = - X_I \left( e^{- j 2 \pi \left( f + \frac{1}{2} \right)} \right) \end{cases}
\]
\item il modulo è pari:
\[
\begin{cases} \displaystyle {\left| X \left( e^{j 2 \pi f} \right) \right|}^2 = X_R^2 \left( e^{j 2 \pi f} \right) + X_I^2 \left( e^{j 2 \pi f} \right) \\
\displaystyle {\left| X \left( e^{j 2 \pi \left( f - \frac{1}{2} \right)} \right) \right|}^2 = X_R^2 \left( e^{j 2 \pi \left( f - \frac{1}{2} \right)} \right) + X_I^2 \left( e^{j 2 \pi \left( f - \frac{1}{2} \right)} \right) \end{cases}
\]
\item la fase è dispari:
\[
\begin{cases} \displaystyle \arg{X_I \left( e^{j2 \pi f} \right)} = \mathrm{arctg}  \frac{X_I \left( e^{j2 \pi f} \right)}{X_R \left( e^{j2 \pi f} \right)}  \\
\displaystyle \arg{X_I \left( e^{j2 \pi \left( f - \frac{1}{2} \right)} \right)} = \mathrm{arctg}  \frac{X_I \left( e^{j2 \pi \left( f - \frac{1}{2} \right)} \right)}{X_R \left( e^{j2 \pi \left( f - \frac{1}{2} \right)} \right)}  \end{cases}
\]
\end{itemize}

\subsection{Valore iniziale e somma dei campioni}
\paragraph{Valore iniziale}
\[
\left. x \left( n \right) \right \vert_{n = 0} = x \left( 0 \right) = \int_{- \frac{1}{2}}^{+ \frac{1}{2}} X \left( e^{j 2 \pi f} \right) df
\]

\paragraph{Somma dei campioni}
\[
\left. X \left( e^{j2 \pi f} \right) \right \vert_{f = 0} = \sum_{k = - \infty}^{+ \infty} x \left( k \right)
\]

Ne consegue che sequenze a valor medio nullo hanno DTFT nulla in $f = 0$.

\subsection{Relazione di Parseval}
La relazione di Parseval nel dominio del tempo discreto ha estremi di integrazione finiti:
\[
E_x = \sum_{k = - \infty}^{+ \infty} {\left| x \left( k \right) \right|}^2 = \int_{- \frac{1}{2}}^{+ \frac{1}{2}} {\left| X \left( e^{j2 \pi f} \right) \right|}^2 df
\]

\paragraph{Relazione di Parseval generalizzata}
\[
\sum_{k = - \infty}^{+ \infty} x \left( k \right) y^* \left( k \right) = \int_{- \frac{1}{2}}^{+ \frac{1}{2}} X \left( e^{j2 \pi f} \right) Y^* \left( e^{j2 \pi f} \right) df
\]

\subsection{Spettro di energia}
Lo \textbf{spettro di energia} $S_x \left( f \right)$ dà informazioni sulla distribuzione dell'energia della sequenza $x \left( n \right)$ nel dominio della frequenza:
\[
S_x \left( f \right) = {\left| X \left( e^{j2 \pi f} \right) \right|}^2
\]

\paragraph{Proprietà}
Lo spettro di energia $S_x \left( f \right)$:
\begin{itemize}
\item non può essere negativo;
\item se $x \left( n \right)$ è reale, è reale e pari;
\item è periodico di periodo 1.
\end{itemize}

\section{DTFT notevoli}
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|}
  \hline
  & \textbf{Sequenza $x \left( n \right)$} & \textbf{DTFT $\displaystyle X \left( e^{j 2 \pi f} \right)$} \\
  \hline
  \textbf{Sequenza delta} & $\delta \left( n \right)$ & $1$ \\
  \hline
  \textbf{Sequenza costante} & $1$ & \begin{tabular}[c]{@{}c@{}}$\displaystyle \sum_{n = - \infty}^{+ \infty} \delta \left( f - n \right)$\\$\displaystyle \delta \left( f \right) \quad f \in \left[ - \frac{1}{2} , \frac{1}{2} \right]$\end{tabular} \\
  \hline
  \textbf{Sequenza segno} & $\text{sgn} \left( n \right)$ & $\displaystyle \frac{1 + e^{-j 2 \pi f}}{1 - e^{-j2 \pi f}}$ \\
  \hline
  \textbf{Sequenza gradino} & $u \left( n \right)$ & $\displaystyle \frac{1}{2} \delta \left( f \right) + \frac{1}{1 - e^{-j2 \pi f}}$ \\
  \hline
  \textbf{Sequenza esponenziale} & $\displaystyle e^{j2 \pi f_0 n}$ & $\delta \left( f - f_0 \right)$ \\
  \hline
  \textbf{Sequenza cosinusoidale} & $\cos \left( 2 \pi f_0 n \right)$ & $\displaystyle \frac{1}{2} \left[ \delta \left( f - f_0 \right) + \delta \left( f + f_0 \right) \right]$ \\
  \hline
  \textbf{Sequenza sinusoidale} & $\sin \left( 2 \pi f_0 n \right)$ & $\displaystyle \frac{1}{2j} \left[ \delta \left( f - f_0 \right) - \delta \left( f + f_0 \right) \right]$ \\
  \hline
  \textbf{Sequenza sinc} & $\displaystyle \mathrm{sinc} \left( \frac{n}{N} \right)$ & $\displaystyle N \cdot P_{\frac{1}{N}} \left( f \right)$ \\
  \hline
  \textbf{Sequenza porta} & $\displaystyle p_{2K+1} \left( n \right)$ & $\displaystyle \frac{\sin \left[ \pi f \left( 2K + 1 \right) \right]}{\sin \left( \pi f \right)}$ \\
  \hline
 \end{tabular}}
\end{table}

\section{Banda di un segnale a tempo discreto}
\subsection{Banda assoluta}
\begin{figure}
	\centering
	\includegraphics[scale=0.55]{pic/B03/ENS_Banda_1}
\end{figure}

\noindent
La \textbf{banda assoluta} della sequenza $x \left( n \right)$ è la frequenza $B_x \leq \frac{1}{2}$, quindi all'interno del singolo periodo della DTFT, per cui la DTFT $\left| X \left( e^{j2 \pi f} \right) \right|$ è identicamente nulla al di fuori dell'intervallo $\left[ - B_x, B_x \right]$. È l'equivalente della banda unilatera nel dominio del tempo discreto, però si considera solamente un periodo della funzione nel dominio della frequenza (la DTFT ha sempre supporto infinito).
\FloatBarrier

\subsection{Banda equivalente}
\begin{figure}
	\centering
	\includegraphics[scale=0.65]{pic/B03/ENS_Banda_2}
\end{figure}

\noindent
La larghezza di banda $B_{\text{eq}}$ è pari alla semilarghezza del rettangolo:
\begin{itemize}
\item la cui altezza è pari al massimo ${\left| X_M \right|}^2$ dello spettro di energia $S_x \left( f  \right)$;
\item la cui area è uguale all'energia complessiva $E \left( S_x \right)$ della DTFT, cioè all'area sottesa da $S_x \left( f \right)$ all'interno del singolo periodo della DTFT:
\[
2 B_{\text{eq}} {\left| X_M \right|}^2 = \int_{- \frac{1}{2}}^{+ {1 \over 2}} S_x \left( f \right) df = \int_{- \frac{1}{2}}^{+ {1 \over 2}} {\left| X \left( e^{2j \pi f} \right) \right|}^2 df
\]
che per la relazione di Parseval è anche uguale all'energia della sequenza $x \left( n \right)$, data dalla somma di tutti i suoi campioni:
\[
2 B_{\text{eq}} {\left| X_M \right|}^2 = E_x = \sum_{k  = - \infty}^{+ \infty} x \left( k \right)
\]
\end{itemize}

\subsection{Banda \texorpdfstring{$B_{x\%}$}{Bx}}
\begin{figure}
	\centering
	\includegraphics[scale=0.65]{pic/B03/ENS_Banda_3}
\end{figure}

\noindent

\noindent
La banda $B_{x\%}$ è la frequenza per cui l'intervallo $\left[ - B_{x\%} , B_{x\%} \right]$ corrisponde all'$x\%$ dell'energia complessiva della sequenza $y \left( n \right)$, ovvero all'$x\%$ dell'area sottesa dallo spettro di energia $S_y \left( f \right)$ all'interno del singolo periodo della DTFT:
\[
\int_{- B_{x\%}}^{+ B_{x\%}} S_y \left( f \right) df = \frac{x}{100} \int_{- {1 \over 2}}^{+ {1 \over 2}} S_y \left( f \right) df = \frac{x}{100} \sum_{k = - \infty}^{+ \infty} {\left| y \left( n \right) \right|}^2
\]
\FloatBarrier

\subsection{Banda a 3 dB}
La \textbf{banda a 3 dB} $B_{3 \text{ dB}}$ è la frequenza a cui l'ampiezza dello spettro di energia $S_x \left( f \right)$ si riduce di 3 dB rispetto al suo massimo:
\[
S_x \left( B_{3 \text{ dB}} \right) = \frac{{\left| X_M \right| }^2}{2}
\]