\chapter{Segnali periodici}
I segnali periodici:
\[
x \left( t \right) = \int_{- \infty}^{+ \infty} x_T \left( t - n T \right) = x \left( t + T \right)
\]
sono un caso particolare dei \textbf{segnali ciclici}:
\[
x_c \left( t \right) = \int_{n_1}^{n_2} x_T \left( t - n T \right) \neq x_c \left( t + T \right)
\]

\section{Trasformata di Fourier di un segnale periodico}
\label{sez:trasformata_di_fourier_di_un_segnale_periodico}
La serie di Fourier derivata per il segnale a supporto finito:
\[ x_T \left( t \right) = \int_{- \infty}^{+ \infty} {\mu}_n e^{j \frac{2 \pi}{T} nt} \, , \quad - \frac{T}{2} \leq t \leq \frac{T}{2} \, \quad {\mu}_n = \frac{1}{T} \int_{- \frac{T}{2}}^{\frac{T}{2}} x_T \left( t \right) e^{- j \frac{2 \pi}{T} nt} dt \]
quando interpretata su tutto l'asse dei tempi è anche la serie di Fourier del segnale periodico $x \left( t \right)$:
\[
x \left( t \right) = \int_{- \infty}^{+ \infty} {\mu}_n e^{j \frac{2 \pi}{T} nt} \, , \quad \forall t
\]
da cui si può ottenere la trasformata di Fourier del segnale periodico:
\[
X \left( f \right) = \sum_{n = - \infty}^{+ \infty} {\mu}_n \int_{- \infty}^{+ \infty} e^{j \frac{2 \pi}{T} n t} e^{- j 2 \pi f t} dt = \sum_{n = - \infty}^{+ \infty} {\mu}_n \delta \left( f - \frac{n}{T} \right)
\]
dove:
\[ {\mu}_n = \frac{1}{T} \int_{- \frac{T}{2}}^{\frac{T}{2}} x \left( t \right) e^{- j \frac{2 \pi}{T} nt } dt = \]
e poiché $x_T \left( t \right)$ è il segnale $x \left( t \right)$ troncato in $\left[ 0 , T \right]$:
\[
= \frac{1}{T} \int_{- \infty}^{+ \infty} x_T \left( t \right) e^{-j \frac{2 \pi}{T} nt} dt = \frac{1}{T} X_T \left( \frac{n}{T} \right)
\]

La trasformata di Fourier del segnale $x \left( t \right)$ è quindi una sommatoria dei campioni, presi a multipli di $\frac{1}{T}$, della trasformata di Fourier del segnale troncato $x_T \left( t \right)$:
\[ X \left( f \right) = \frac{1}{T} \sum_{n = - \infty}^{+ \infty} X_T \left( \frac{n}{T} \right) \delta \left( f - \frac{n}{T} \right) \]

\section{Treno di impulsi}
Considerando come segnale periodico il \textbf{segnale campionatore}, o \textbf{treno di impulsi}:
\[
c_T \left( t \right) = \sum_{n = - \infty}^{+ \infty} \delta \left( t - n T \right)
\]
secondo la formula appena ricavata la sua trasformata, poiché $\mathcal{F} \left\{ \delta \left( t \right) \right\} = 1$, è ancora un treno di impulsi:
\[
C_T \left( f \right) = \frac{1}{T} \sum_{- \infty}^{+ \infty} \delta \left( f - \frac{n}{T} \right)
\]

Siccome per definizione di trasformata di Fourier vale anche:
\[
C_T \left( f \right) = \mathcal{F} \left\{ c_T \left( t \right) \right\} = \sum_{- \infty}^{+ \infty} e^{-j2 \pi f nT}
\]
vale anche la seguente uguaglianza:
\[
\int_{- \infty}^{+ \infty} e^{-j2 \pi n f T} = \frac{1}{T} \sum_{n = - \infty}^{+ \infty} \delta \left( t - \frac{n}{T} \right)
\]

Aumentare il periodo del treno di impulsi nel periodo del tempo corrisponde a diminuire il suo periodo nel dominio della frequenza.

\subsection{Campionamento nel tempo e periodicizzazione in frequenza}
\label{sez:campionamento_nel_tempo_e_periodicizzazione_in_frequenza}
Moltiplicando un segnale $x \left( t \right)$ per un treno di impulsi si ottiene:
\begin{itemize}
\item nel dominio del tempo una sequenza equispaziata di suoi campioni:
\[
x \left( t \right) \cdot c_T \left( t \right) = \sum_{n = - \infty}^{+ \infty} x \left( t \right) \delta \left( t - nT \right) = \sum_{n = - \infty}^{+ \infty} x \left( nT \right) \delta \left( t - nT \right)
\]
\item nel dominio della frequenza una trasformata periodica di periodo $\frac{1}{T}$:
\[
X \left( f \right) * C_T \left( f \right) = X \left( f \right) * \frac{1}{T} \sum_{n = - \infty}^{+ \infty} \delta \left( f - \frac{n}{T} \right) = \frac{1}{T} \sum_{n = - \infty}^{+ \infty} X \left( f \right) * \delta \left( f - \frac{n}{T} \right) = \frac{1}{T} \sum_{n = - \infty}^{+ \infty} X \left( f - \frac{n}{T} \right)
\]
\end{itemize}

Quindi si ottiene la seguente relazione:
\[ \mathcal{F} \left\{ \sum_{n = - \infty}^{+ \infty} x \left( t \right) \delta \left( t - nT \right) \right\} = \frac{1}{T} \sum_{n = - \infty}^{+ \infty} X \left( f \right) * \delta \left( f - \frac{n}{T} \right) \]

\subsection{Periodicizzazione nel tempo e campionamento in frequenza}
Facendo il prodotto di convoluzione di un segnale $x \left( t \right)$ per un treno di impulsi si ottiene:
\begin{itemize}
\item nel dominio del tempo un segnale periodico di periodo pari alla spaziatura degli impulsi:
\[
x \left( t \right) * c_T \left( t \right) = \sum_{n = - \infty}^{+ \infty} x \left( t \right) * \delta \left( t - nT \right) = \sum_{n = - \infty}^{+ \infty} x \left( t - n T \right)
\]
\item nel dominio della frequenza una trasformata campionata con spaziatura $\frac{1}{T}$:
\[
X \left( f \right) \cdot C_T \left( t \right) = \frac{1}{T} \sum_{n = - \infty}^{+ \infty} X \left( f \right)  \delta \left( f - \frac{n}{T} \right) = \frac{1}{T} \sum_{n = - \infty}^{+ \infty} X \left( \frac{n}{T} \right) \delta \left(  f - \frac{n}{T} \right)
\]
\end{itemize}

Quindi si ottiene una relazione parallela alla precedente:
\[ \mathcal{F} \left\{ \sum_{n = - \infty}^{+ \infty} x \left( t \right) * \delta \left( t - nT \right) \right\} = \frac{1}{T} \sum_{n = - \infty}^{+ \infty} X \left( f \right)  \delta \left( f - \frac{n}{T} \right) \]

\section{Rappresentazioni di un segnale periodico}
Il segnale $x \left( t \right)$:
\[
x \left( t \right) = \sum_{n = - \infty}^{+ \infty} z \left( t - n T \right) = x \left( t + T \right)
\]
è periodico di periodo $T$ anche quando il segnale $z \left( t \right)$ non è a supporto limitato in $\left[ 0 , T \right]$, e quindi nella periodicizzazione di $z \left( t \right)$ alcune parti si vanno a sovrapporre. La sua trasformata di Fourier vale ancora:
\[
x \left( t \right) = \sum_{n = - \infty}^{+ \infty} z \left( t - n T \right) = z \left( t \right) * \sum_{n = - \infty}^{+ \infty} \delta \left( t - n T \right)
\]
\[ X \left( f \right) = Z \left( f \right) \frac{1}{T} \sum_{n = - \infty}^{+ \infty} \delta \left( t - \frac{n}{T} \right) = \frac{1}{T} \sum_{n = - \infty}^{+ \infty} Z \left( \frac{n}{T} \right) \delta \left( t - \frac{n}{T} \right) \]

La seguente rappresentazione di un segnale periodico $x \left( t \right)$:
\[
x \left( t \right) = \sum_{n = - \infty}^{ + \infty} z \left( t - n T \right) = x \left( t + T \right)
\]
non è univoca, ma possono essere utilizzati tutti i segnali $z \left( t \right)$ che:
\begin{itemize}
\item nel dominio del tempo: coincidono con il segnale troncato $x_T \left( t \right)$ all'interno del periodo $T$:
\[
z \left( t \right) : \, \sum_{n = - \infty}^{+ \infty} z \left( t - n T \right) = x_T \left( t \right) \quad \forall t \in \left[ 0 , T \right]
\]
\item nel dominio della frequenza: assumono gli stessi valori della trasformata di Fourier nelle frequenze $\tfrac{n}{T}$, le uniche che contano nel segnale periodico:
\[
X \left( f \right) = \frac{1}{T} \sum_{n = - \infty}^{+ \infty} X_T \left( \frac{n}{T} \right) \delta \left( f - \frac{n}{T} \right) = \frac{1}{T} \sum_{n = - \infty}^{+ \infty} Z \left( \frac{n}{T} \right) \delta \left( f - \frac{n}{T} \right)
\]
\end{itemize}

\subsection{Esempio: segnale periodico costante}
\begin{figure}
	\centering
	\includegraphics[scale=0.5]{pic/06/Segnali_periodici_esempio_1.png}
\end{figure}

\noindent
Il segnale periodico costante $x \left( t \right) = 1$ si può rappresentare come sommatoria di due diverse funzioni periodiche:
\[
\begin{cases} \displaystyle x \left( t \right) = \sum_{n = - \infty}^{+ \infty} {\Pi}_T \left( t - n T \right) \\ \displaystyle z \left( t \right) = \sum_{n = - \infty}^{+ \infty} {\Lambda}_T \left( t - n T \right) = x \left( t \right) \end{cases}
\]
che hanno due differenti supporti (rispettivamente $T$ e $2T$), ma che nella periodicizzazione (di egual periodo $T$) vengono a coincidere.

Nel dominio della frequenza i campioni di $X_T \left( f \right)$ e $Z \left( f \right)$ coincidono:
\[
\begin{cases} X \left( f \right) = \mathcal{F} \left\{ {\Lambda}_T \left( t \right) \right\} = T \mathrm{sinc} \left( f T \right) \\
Z \left( f \right) = \mathcal{F} \left\{ {\Pi}_T \left( t \right) \right\} = T {\mathrm{sinc}}^2 \left( f T \right) \end{cases}
\]
\FloatBarrier

\subsection{Esempio}
\begin{figure}
	\centering
	\includegraphics[scale=0.5]{pic/06/Segnali_periodici_esempio_2.png}
\end{figure}

\noindent
Considerando i segnali $x_{2T} \left( t \right)$, di supporto $2T$, e $z \left( t \right)$, di supporto $4T$:
\[
\begin{cases} \displaystyle x_{2T} \left( t \right) = {\Pi}_T \left( t + \frac{T}{2} \right) - {\Pi}_T \left( t - \frac{T}{2} \right) \\
\displaystyle z \left( t \right) = {\Pi}_T \left( t + \frac{T}{2} \right) - {\Pi}_T \left( t - \frac{5T}{2} \right) \end{cases}
\]
e campionando le loro trasformate di Fourier:
\[
\begin{cases} \displaystyle X_{2T} \left( f \right) = T \mathrm{sinc} \left( f T \right) \left( e^{j  \pi f T} - e^{-j \pi f T} \right) \\
\displaystyle Z \left( f \right) = T \mathrm{sinc} \left( f T \right) \left( e^{j \pi f T} - e^{- j \pi f 5 T} \right) \end{cases}
\]
nelle frequenze $\tfrac{n}{2T}$, i campioni coincidono:
\[
\begin{cases} \displaystyle X_{2T} \left( \frac{n}{2T} \right) = T \mathrm{sinc} \left( \frac{n}{2} \right) \left( e^{j n \frac{\pi}{2}} - e^{-j n \frac{\pi}{2}} \right) = jT \mathrm{sinc} \left( \frac{n}{2} \right) \sin{\frac{\pi n}{2}} \\
\displaystyle Z \left( \frac{n}{2T} \right) = T \mathrm{sinc} \left( \frac{n}{2} \right) \left( e^{j n \frac{\pi}{2}} - e^{- j \pi n \frac{5}{2}} \right) = jT \mathrm{sinc} \left( \frac{n}{2} \right) \sin{\frac{\pi n}{2}} =  X_{2T} \left( \frac{n}{2T} \right) \end{cases}
\]
%\FloatBarrier