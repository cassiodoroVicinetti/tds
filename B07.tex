\chapter{Trasformata zeta}
\paragraph{Vantaggio rispetto alla DTFT}
La trasformata zeta converge in modo uniforme per una classe di segnali più vasta.

\paragraph{Utilità}
La trasformata zeta trasforma le equazioni alle differenze in equazioni algebriche più semplici.

\section{Definizione}
La \textbf{trasformata zeta} $X \left( z \right)$ della sequenza $x \left( n \right)$ è un polinomio nella variabile $z^{-1}$ e con i campioni della sequenza $x \left( n \right)$ per coefficienti:
\[
X \left( z \right) = \sum_{n = - \infty}^{+ \infty} x \left( n \right) z^{-n}
\]
dove $z$ è una variabile complessa di modulo $\rho$ e fase $\omega = 2 \pi f$, che può assumere valori in tutto il piano complesso:
\[
z = \rho \cdot e^{j \omega}
\]

\subsection{Relazione con la DTFT}
La DTFT $X \left( e^{j \omega} \right)$ è un caso particolare della trasformata zeta perché, nel piano complesso, percorre al variare di $\omega$ una circonferenza di raggio unitario:
\[
X \left( e^{j \omega} \right) = \left. X \left( z \right) \right \vert_{z = e^{j \omega}} = \sum_{n = - \infty}^{+ \infty} x \left( n \right) e^{-j  \omega n}
\]

Come nel dominio del tempo continuo la trasformata di Laplace è la generalizzazione della trasformata di Fourier, così nel dominio del tempo discreto la trasformata zeta è la generalizzazione della DTFT.

\subsection{Relazione con la DFT}
A partire dai campioni della DFT di una sequenza $x \left( n \right)$ a supporto limitato $\left[ 0 , N - 1 \right]$ si può risalire alla sua trasformata zeta tramite interpolazione:
\[
X \left( z \right) = \frac{1}{N} \left( 1 - z^{-N} \right) \sum_{k =0}^{N-1} \frac{X \left( k \right)}{1 - e^{j \frac{2 \pi}{N} k} z^{-1}}
\]
\begin{framed}
\paragraph{Dimostrazione}
\[ X \left( z \right) = \sum_{n = 0}^{N-1} x \left( n \right) z^{-n} = \]

Sostituendo l'espressione della IDFT:
\[ = \sum_{n = 0}^{N-1} \left[ \frac{1}{N} \sum_{k = 0}^{N-1} X \left( k \right) e^{-j \frac{2 \pi}{N} kn} \right] z^{-n} = \frac{1}{N} \sum_{k = 0}^{N-1} X \left( k \right) \sum_{n = 0}^{N-1} {\left( e^{j \frac{2 \pi}{N} k} z^{-1} \right)}^n = \]
\[ \sum_{k = 0}^{N-1} x^k = \frac{1- x^N}{1-x} \Rightarrow = \frac{1}{N} \sum_{k = 0}^{N-1} X \left( k \right) \frac{1- e^{j \frac{2 \pi}{N} k N} z^{-N}}{1- e^{j \frac{2 \pi}{N} k} z^{-1}} = \frac{1}{N} \sum_{k = 0}^{N-1} X \left( k \right) \frac{1- e^{j2 \pi k} z^{-N}}{1- e^{j \frac{2 \pi}{N} k} z^{-1}} = \]
\[ e^{j2 \pi k} = 1 \Rightarrow = \frac{1}{N} \sum_{k =0}^{N-1} X \left( k \right) \frac{1- z^{-N}}{1- e^{j \frac{2 \pi}{N} k} z^{-1}} \]
\end{framed}

\section{Analisi della regione di convergenza}
L'espressione della trasformata zeta è detta serie di Cauchy-Laurent. La \textbf{regione di convergenza} (Region of Convergence [ROC]) della serie è il luogo dei punti per cui essa converge in modo uniforme. Nella regione di convergenza, la trasformata zeta $X \left( z \right)$ è una funzione analitica, ossia continua e infinitamente derivabile con derivate continue.

La serie della trasformata zeta converge se e solo se la sequenza $x \left( n \right) \rho^{-n}$ è assolutamente sommabile:
\[
\sum_{n = - \infty}^{+ \infty} \left| x \left( n \right) \right| {\rho}^{-n} \in \mathbb{R}
\]
\begin{framed}
\paragraph{Dimostrazione}
La trasformata zeta $X \left( z \right)$ della sequenza $x \left( n \right)$ equivale alla DTFT della sequenza $x \left( n \right) {\rho}^n$:
\[
z = \rho e^{j \omega} \Rightarrow X \left( z \right) = \sum_{n = - \infty}^{+ \infty} x \left( n \right) {\left[ \rho e^{j \omega} \right]}^{-n} = \sum_{n = - \infty}^{+ \infty} \left[ x \left( n \right) {\rho}^{-n} \right] e^{- j \omega n}
\]

Per la condizione di esistenza della DTFT:
\[
\sum_{n = - \infty}^{+ \infty} \left| x \left( n \right) {\rho}^{-n} \right| = \sum_{n = - \infty}^{+ \infty} \left| x \left( n \right) \right| {\rho}^{-n} \in \mathbb{R}
\]
\end{framed}

Le regioni di convergenza nel piano complesso sono delimitate da circonferenze (ossia luoghi dei punti a modulo costante), perché non dipendono dalla fase $\omega$ ma solo dal modulo $\rho$.

\paragraph{Esempio}
\[ x \left( n \right) = \delta \left( n - n_0 \right) \Rightarrow X \left( z \right) = \sum_{n = - \infty}^{+ \infty} \delta \left( n - n_0 \right) z^{-n} = z^{- n_0} \]
\begin{itemize}
\item se $n_0 = 0$ converge su tutto il piano complesso:
\[
X \left( z \right) = 1
\]
\item se $n_0 > 0$ diverge per $z = 0$:
\[
X \left( z \right) = \frac{1}{z^{n_0}}
\]
\item se $n_0 < 0$ diverge per $z \to + \infty$:
\[
X \left( z \right) = z^{\left| n_0 \right|}
\]
\end{itemize}

\subsection{Sequenze unilatere e sequenze bilatere}
\afterpage{
\begin{landscape}
\begin{figure}[p]
\centering
	\begin{subfigure}[b]{.35\linewidth}
		\centering
		\includegraphics[scale=0.66]{pic/B07/Region_of_convergence_0_5_anticausal}
		\caption{Sequenza unilatera anti-causale.}
		\label{fig:convergenza_anticausale}
	\end{subfigure}%
	\begin{subfigure}[b]{.35\linewidth}
		\centering
		\includegraphics[scale=0.66]{pic/B07/Region_of_convergence_0_5_causal}
		\caption{Sequenza unilatera causale.}
		\label{fig:convergenza_causale}
	\end{subfigure}%
	\begin{subfigure}[b]{.35\linewidth}
		\centering
		\includegraphics[scale=0.66]{pic/B07/Region_of_convergence_0_5_0_75_mixed-causal}
		\caption{Sequenza bilatera.}
		\label{fig:convergenza_mista}
	\end{subfigure}
	\caption[Sequenze unilatere e bilatere.]{Sequenze unilatere e bilatere.\footnotemark}
\end{figure}
\end{landscape}

\footnotetext{Queste immagini sono tratte da Wikimedia Commons (\href{https://commons.wikimedia.org/wiki/File:Region_of_convergence_0.5_anticausal.svg}{Region of convergence 0.5 anticausal.svg}, \href{https://commons.wikimedia.org/wiki/File:Region_of_convergence_0.5_causal.svg}{Region of convergence 0.5 causal.svg}, \href{https://commons.wikimedia.org/wiki/File:Region_of_convergence_0.5_0.75_mixed-causal.svg}{Region of convergence 0.5 0.75 mixed-causal.svg}), sono state realizzate da \href{https://en.wikipedia.org/wiki/User:Cburnett}{Colin M.L. Burnett}, e sono concesse sotto la \href{http://creativecommons.org/licenses/by-sa/3.0/deed.it}{licenza Creative Commons Attribuzione - Condividi allo stesso modo 3.0 Unported}.}
}

La ricerca della regione di convergenza di $X \left( z \right)$ equivale alla ricerca della regione in cui la serie $x \left( n \right) \rho^{-n}$ risulta assolutamente sommabile:
\[
\sum_{n = - \infty}^{+ \infty} \left| x \left( n \right) \right| \rho^{-n} = \overbrace{\sum_{n = - \infty}^{-1} \left| x \left( n \right) \right| \rho^{-n}}^{\text{anti-causale}} + \overbrace{\sum_{n = 0}^{+ \infty} \left| x \left( n \right) \right| \rho^{-n}}^{\text{causale}} = \sum_{n = 1}^{+ \infty} \left| x \left( - n \right) \right| \rho^n + \sum_{n=0}^{+ \infty} \frac{\left| x \left( n \right) \right|}{\rho^n}
\]
\begin{itemize}
\item la parte anti-causale converge all'interno della circonferenza avente un raggio $\rho_1$ sufficientemente piccolo;
\item la parte causale converge all'esterno di una circonferenza di raggio $\rho_2$ sufficientemente grande.
\end{itemize}

\paragraph{Sequenze unilatere anti-causali}
La parte causale è nulla $\Rightarrow$ la trasformata zeta $X \left( z \right)$ converge all'interno della circonferenza di raggio $\rho_1$ (figura~\ref{fig:convergenza_anticausale}).

\paragraph{Sequenze unilatere causali}
La parte anti-causale è nulla $\Rightarrow$ la trasformata zeta $X \left( z \right)$ converge all'esterno della circonferenza di raggio $\rho_2$ (figura~\ref{fig:convergenza_causale}).

\paragraph{Sequenze bilatere}
Esistono sia la parte causale sia la parte anti-causale:
\begin{itemize}
\item se $\rho_1 < \rho_2$, la trasformata zeta $X \left( z \right)$ non converge (l'intersezione tra le due regioni è nulla);
\item se $\rho_1 > \rho_2$, la trasformata zeta $X \left( z \right)$ converge nella corona circolare tra $\rho_2$ e $\rho_1$ (figura~\ref{fig:convergenza_mista}).
\end{itemize}

\subsection{Regione di convergenza delle sequenze gradino}
Il gradino e il gradino anti-causale hanno la stessa trasformata zeta:
\[
X \left( z \right) = \frac{1}{1- z^{-1}}
\]
ma regioni di convergenza differenti:
\begin{itemize}
\item il gradino $u \left( n \right)$ converge all'esterno della circonferenza unitaria: $\left| z \right| > 1$;
\begin{framed}
\paragraph{Calcoli}
\[ X \left( z \right) = \sum_{n = - \infty}^{+ \infty} u \left( n \right) z^{-n} = \sum_{n = 0}^{+ \infty} {\left( z^{-1} \right)}^n = \frac{1}{1- z^{-1}} \quad \left| z^{-1} \right| < 1 \]
\end{framed}

\item il gradino anti-causale $\displaystyle -u \left( - n - 1 \right) = \begin{cases} -1 & n \leq -1 \\
0 & n > -1 \end{cases}$ converge all'interno della circonferenza unitaria: $\left| z \right| < 1$.
\begin{framed}
\paragraph{Calcoli}
\[ X \left( z \right) = \sum_{n = - \infty}^{+ \infty} \left[ - u \left( - n - 1 \right) \right] z^{-n} = - \sum_{n = - \infty}^{-1} {\left( z^{-1} \right)}^n = - \sum_{m = 1}^{+ \infty} z^m = \]
\[ = - \sum_{m = 0}^{+ \infty} z^m + 1 =  - \frac{1}{1-z} + 1 = - \frac{z}{1-z} = \frac{1}{1- {z}^{-1}} \quad \left| z \right| < 1 \]
\end{framed}
\end{itemize}

\subsection{Regione di convergenza di sequenze a supporto finito}
La trasformata zeta $X \left( z \right)$ di sequenze a supporto finito $\left[ n_1 , n_2 \right]$:
\[
X \left( z \right) = \sum_{k = n_1}^{n_2} x \left( k \right) z^{-k}
\]
\begin{framed}
\paragraph{Dimostrazione}
\[
X \left( z \right) = \sum_{n - \infty}^{+ \infty} x \left( n \right) z^{-n} =  \sum_{n - \infty}^{+ \infty} \sum_{k = n_1}^{n_2} x \left( k \right) \delta \left( n - k \right) z^{-n} =
\]
\[
=\sum_{k = n_1}^{n_2} x \left( k \right) \sum_{n= - \infty}^{+ \infty} \delta \left( n - k \right) z^{-n} = \sum_{k = n_1}^{n_2} x \left( k \right) z^{-k}
\]
\end{framed}

\paragraph{Sequenze unilatere causali ($n_1 \geq 0$)}
La trasformata zeta $X \left( z \right)$ converge in qualunque punto del piano complesso \ul{eccetto} l'origine ($z =0$). La regione di convergenza è all'esterno di una circonferenza di raggio infinitesimo.

\paragraph{Sequenze unilatere anti-causali ($n_2 < 0$)}
La trasformata zeta $X \left( z \right)$ converge in qualunque punto nel piano complesso \ul{eccetto} l'infinito ($z \to \infty$). La regione di convergenza è all'interno di una circonferenza di raggio infinito.

\paragraph{Sequenze bilatere}
La trasformata zeta $X \left( z \right)$ converge in qualunque punto nel piano complesso \ul{eccetto} l'origine e l'infinito.

\subsection{Regione di convergenza di sequenze con trasformata zeta espressa come rapporto di polinomi}
Nella maggior parte dei casi la trasformata zeta è espressa come rapporto di polinomi:
\[
X \left( z \right) = \frac{N \left( z \right)}{D \left( z \right)} = \frac{\displaystyle \sum_{i=0}^{p_n} b_i z^{-i}}{\displaystyle \sum_{i=0}^{p_d} a_i z^{-i}} = z^{p_d - p_n} \frac{\displaystyle \sum_{i = 0}^{p_n} b_i z^{p_n-i}}{\displaystyle \sum_{i=0}^{p_d} a_i z^{p_d-i}} = \frac{b_0}{a_0} z^{p_d - p_n} \frac{\displaystyle \prod_{i=1}^{p_n} z - c_i}{\displaystyle \prod_{i=1}^{p_d} z-d_i}
\]
\begin{itemize}
\item le radici $c_1, \ldots , c_{p_n}$ del numeratore sono gli \textbf{zeri} di $X \left( z \right)$;
\item le radici $d_1, \ldots , d_{p_d}$ del denominatore sono i \textbf{poli} di $X \left( z \right)$.
\end{itemize}

\paragraph{Sequenze unilatere causali}
La trasformata zeta $X \left( z \right)$ converge all'esterno della circonferenza che racchiude tutti i poli, cioè tutti i poli devono stare all'interno della circonferenza di raggio pari al modulo del polo più vicino all'origine. La circonferenza esiste sempre perché non possono esistere poli per $z \to \infty$.

\paragraph{Sequenze unilatere anti-causali}
La trasformata zeta $X \left( z \right)$ converge all'interno della circonferenza che esclude tutti i poli, cioè tutti i poli devono stare all'esterno della circonferenza di raggio pari al modulo del polo più distante dall'origine.

\paragraph{Sequenze bilatere}
La trasformata zeta $X \left( z \right)$ converge nella corona circolare tra la circonferenza del polo più distante e quella del polo più vicino.

\section{Proprietà della trasformata zeta}
\afterpage{
\begin{landscape}
\begin{table}[p]
 \centering
 \centerline{\begin{tabular}{|c|c|c|c|}
  \cline{2-4}
  \multicolumn{1}{c|}{} & \textbf{Sequenza} & \textbf{Trasformata zeta} & \textbf{Regione di convergenza} \\
  \hline
  \textbf{Linearità} & $\displaystyle a_1 x \left( n \right) + a_2 y \left( n \right)$ & $\displaystyle a_1 X \left( z \right)  + a_2 X \left( z \right)$ & $\displaystyle R_x \cap R_y$ \\
  \hline
  \textbf{Ritardo nel tempo} & $\displaystyle x \left( n - N \right)$ & $\displaystyle z^{-N} X \left( z \right)$ & $\displaystyle R_x - \left\{  0 \right\}$ \\
  \hline
  \textbf{Anticipo nel tempo} & $\displaystyle x \left( n + N \right)$ & $\displaystyle z^N X \left( z \right)$ & $\displaystyle R_x \in \mathbb{R}$ \\
  \hline
  \textbf{Ribaltamento nel tempo} & $\displaystyle x \left( - n \right)$ & $\displaystyle X \left( z^{-1} \right)$ & $\displaystyle \frac{1}{R_x}$ \\
  \hline
  \textbf{Coniugazione complessa} & $\displaystyle x^* \left( n \right)$ & $\displaystyle X^* \left( z^* \right)$ & $\displaystyle R_x$ \\
  \hline
  \textbf{Scalamento nel dominio trasformato} & $\displaystyle a^n x \left( n \right)$ & $\displaystyle X \left( \frac{z}{a} \right)$ & $\displaystyle \left| a \right| R_x$ \\
  \hline
  \textbf{Derivata nel dominio trasformato} & $\displaystyle n x \left( n \right)$ & $\displaystyle -z \frac{d}{dz} X \left( z \right)$ & $\displaystyle R_x$ \\
  \hline
  \textbf{Convoluzione nel tempo} & $\displaystyle x \left( n \right) * y \left( n \right)$ & $\displaystyle X \left( z \right) Y \left( z \right)$ & $\displaystyle R_x \cap R_y$ \\
  \hline
  \textbf{Parte reale} & $\displaystyle \Re{\left\{ x \left( n \right) \right\}}$ & $\displaystyle \frac{1}{2} \left[ X \left( z \right)  + X^* \left( z^* \right) \right]$ & $\displaystyle R_x$ \\
  \hline
  \textbf{Parte immaginaria} & $\displaystyle \Im{\left\{ x \left( n \right) \right\}}$ & $\displaystyle \frac{1}{2j} \left[ X \left( z \right)  - X^* \left( z^* \right) \right]$ & $\displaystyle R_x$ \\
  \hline
  \textbf{Sequenza cosinusoidale} & $\displaystyle \cos \left( 2 \pi f n \right) x \left( n \right)$ & $\displaystyle \frac{1}{2} \left[ X \left( z e^{j2 \pi f} \right) + X \left( z e^{-j2 \pi f} \right) \right]$ & \textemdash \\
  \hline
  \textbf{Sequenza sinusoidale} & $\displaystyle \sin \left( 2 \pi f n \right) x \left( n \right)$ & $\displaystyle \frac{j}{2} \left[ X \left( z e^{j2 \pi f} \right) - X \left( z e^{-j2 \pi f} \right) \right]$ & \textemdash \\
  \hline
 \end{tabular}}
 \caption{Proprietà della trasformata zeta.}
\end{table}
\end{landscape}
}

\subsection{Trasformata zeta unilatera}
La trasformata zeta unilatera $X^+ \left( z \right)$ è la trasformata della parte causale della sequenza $x \left( n \right)$:
\[
X^+ \left( z \right) = \sum_{n=0}^{+ \infty} x \left( n \right) z^{-n}
\]

Per le sequenze unilatere causali, la trasformata zeta $X \left( z \right)$ coincide con la trasformata zeta unilatera $X^+ \left( z \right)$.

\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|}
  \cline{2-3}
  \multicolumn{1}{c|}{} & \textbf{Sequenza $y \left( n \right)$} & \textbf{Trasformata zeta $X^+ \left( z \right)$} \\
  \hline
  \textbf{Ritardo nel tempo} & $\displaystyle x \left( n - N \right)$ & $\displaystyle z^{-N} \left[ X^+ \left( z \right) + \sum_{n=1}^N x \left( - n \right) z^n \right]$ \\
  \hline
  \textbf{Anticipo nel tempo} & $\displaystyle x \left( n + N \right)$ & $\displaystyle z^N \left[ X^+ \left( z \right) - \sum_{n=0}^N x \left( n \right) z^{-n} \right]$ \\
  \hline
 \end{tabular}}
 \caption{Proprietà di traslazione nel tempo per trasformate zeta unilatere.}
\end{table}
\begin{framed}
\paragraph{Dimostrazione proprietà di ritardo}
\[ \sum_{n=0}^{+ \infty} x \left( n-N \right) z^{-N} = \sum_{m=-N}^{+ \infty} x \left( m \right) z^{-m-N} = z^{-N} \left[ \sum_{m=-N}^{-1} x \left( m \right) z^{-m} + \sum_{m = 0}^{+ \infty} x \left( m \right) z^{-m} \right] =  \]
\[ = z^{-N} \left[ X^+ \left( z \right) + \sum_{n=1}^N x \left( - n \right) z^n \right] \]
\end{framed}
\begin{framed}
\paragraph{Dimostrazione proprietà di anticipo}
\[ \sum_{n=0}^{+ \infty} x \left( n+N \right) z^{-n} = \sum_{m=N}^{+ \infty} x \left( m \right) z^{-m+N} = z^N \left[ \sum_{m=0}^{+ \infty} x \left( m \right) z^{-m} - \sum_{m=0}^{N-1} x \left( m \right) z^{-m} \right] =  \]
\[ = z^N \left[ X^+ \left( z \right) - \sum_{n=0}^N x \left( n \right) z^{-n} \right] \]
\end{framed}

\subsubsection{Teorema del valore iniziale}
Se $x \left( n \right)$ è una sequenza causale:
\[
\lim_{z \to + \infty} X \left( z \right) = x \left( 0 \right)
\]
\begin{framed}
\paragraph{Dimostrazione}
\[ \lim_{z \to + \infty} X \left( z \right) = \lim_{z \to + \infty} \left[ x \left( 0 \right) + \underbrace{x \left( 1 \right)}_{=0} z^{-1} + \underbrace{x \left( 2 \right)}_{=0} z^{-2} + \ldots \right] \]
\end{framed}

\begin{table}
 \centering
 \centerline{\begin{tabular}{|c|c|c|l|}
  \hline
  \textbf{Sequenza $x \left( n \right)$} & \textbf{Trasformata zeta $X \left( z \right)$} & \begin{tabular}[c]{@{}c@{}}\textbf{Regione di}\\\textbf{convergenza}\end{tabular} & \textbf{Poli e zeri} \\
  \hline
  $\delta \left( n \right)$ & $1$ & $\forall z$ & \\
  \hline
  $\delta \left( n - N \right) , \; N > 0$ & $z^{-N}$ & $z \neq 0$ & \\
  \hline
  $\delta \left( n + N \right) , \; N > 0$ & $z^{N}$ & $z \in \mathbb{R}$ & \\
  \hline
  $a^n u \left( n \right)$ & $\displaystyle \frac{1}{1- a z^{-1}}$ & $\left| z \right| > \left| a \right|$ & \textbullet\ $z=a$: polo \\
  \hline
  $- a^n u \left( -n - 1 \right)$ & $\displaystyle \frac{1}{1-a z^{-1}}$ & $\left| z \right|  < \left|  a \right|$ & \\
  \hline
  $n a^n u \left( n \right)$ & $\displaystyle \frac{a z^{-1}}{{\left( 1 - a z^{-1} \right)}^2}$ & $\left| z \right| > \left| a \right|$ & \\
  \hline
  $n a^{n-1} u \left( n \right)$ & $\displaystyle \frac{z^{-1}}{{\left( 1-a z^{-1} \right)}^2}$ & $\left| z \right| > \left| a \right|$ & \\
  \hline
  $\left( n - 1 \right) a^n u \left( n \right)$ & $\displaystyle \frac{2a z^{-1} - 1}{{\left( 1-a z^{-1} \right)}^2}$ & $\left| z \right| > \left| a \right|$ & \\
  \hline
  $n^2 a^n u \left( n \right)$ & $\displaystyle \frac{a z^{-1} \left( 1+a z^{-1} \right)}{{\left( 1-a z^{-1} \right)}^3}$ & $\left| z \right| > \left|  a \right|$ & \\
  \hline
  $-n a^n u \left( - n - 1 \right)$ & $\displaystyle \frac{a z^{-1}}{{\left( 1- a z^{-1} \right)}^2}$ & $\left| z \right| < \left| a \right|$ & \\
  \hline
  $a^n \sin{\left( \omega_0 n \right)} u \left( n \right)$ & $\displaystyle \frac{a \sin{\omega_0} z^{-1}}{1-2a \cos{\omega_0} z^{-1} + a^2 z^{-2}}$ & $\left| z \right| > a$ & \begin{tabular}[c]{@{}l@{}}\textbullet\ $z = 0$: zero\\\textbullet\ $z = a e^{j \omega_0}$: polo\\\textbullet\ $z = a e^{- j \omega_0}$: polo\end{tabular} \\
  \hline
  $a^n \cos{\left( \omega_0 n \right)} u \left( n \right)$ & $\displaystyle \frac{1-a \cos{\omega_0} z^{-1}}{1-2a \cos{\omega_0} z^{-1} + a^2 z^{-2}}$ & $\left| z \right| > a$ & \begin{tabular}[c]{@{}l@{}}\textbullet\ $z = a \cos{\omega_0}$: zero\\\textbullet\ $z = a e^{j \omega_0}$: polo\\\textbullet\ $z = a e^{-j \omega_0}$: polo\end{tabular} \\
  \hline
  $a^n \left[ u \left( n \right) - u \left( n -N \right) \right]$ & $\displaystyle \frac{1- a^N z^{-N}}{1-a z^{-1}}$ & $\left| z \right| > 0$ & \\
  \hline
 \end{tabular}}
 \caption{Trasformata zeta di sequenze elementari}
\end{table}

\section{Inversione della trasformata zeta}
L'antitrasformata zeta è definita attraverso un integrale di circuitazione:
\[
x \left( n \right) = \frac{1}{j2 \pi} \oint_{\gamma} X \left( z \right) z^{n-1} dz
\]
dove $\gamma$ è una \href{https://it.wikipedia.org/wiki/Curva_di_Jordan}{curva di Jordan}:
\begin{itemize}
\item percorsa in senso antiorario;
\item appartenente alla regione di convergenza della trasformata zeta $X \left( z \right)$;
\item comprendente l'origine;
\item comprendente $N_p$ poli della funzione $X \left( z \right) z^{n-1}$.
\end{itemize}
\begin{framed}
\paragraph{Dimostrazione}
\[ X \left( z \right) = \sum_{n = - \infty}^{+ \infty} x \left( n \right) z^{-n} ; \; \frac{1}{j 2 \pi} \oint_{\gamma} X \left( z \right) z^{n-1} dz = \frac{1}{j2 \pi} \oint_{\gamma} \sum_{k=- \infty}^{+ \infty} x \left( k \right) z^{-k} z^{n-1} dz = \]
\[ = \frac{1}{j2 \pi} \sum_{n=- \infty}^{+ \infty} x \left( k \right) \oint_{\gamma} z^{n-k-1} dz = \]

Per il teorema di Cauchy:
\[
\frac{1}{j2 \pi} \otimes_{\gamma} z^{n-1} dz = \delta \left( n \right) \Rightarrow = \sum_{n = - \infty}^{+ \infty} x \left( k \right) \delta \left( n - k \right) = x \left( n \right)
\]
\end{framed}

Per il teorema dei residui, l'integrale di linea si può esprimere come somma dei residui dovuti agli $N_p$ poli:
\[
x \left( n \right) = \frac{1}{j2 \pi} \oint_{\gamma} X \left( z \right) z^{n-1} dz = \sum_{i=1}^{N_p} \text{Res}_{X \left( z \right) z^{n-1}} \left( {z_0}_i \right)
\]
%\FloatBarrier