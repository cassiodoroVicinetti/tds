\chapter{Progetto di filtri FIR}
\section{Rotazione di fase lineare}
Un filtro FIR non causale può essere reso causale semplicemente ritardandone la risposta all'impulso $h \left( n \right)$ (che è a supporto finito) in modo che tutti i campioni siano a destra dell'asse delle ordinate ($n > 0$). Il ritardo temporale di $k$ campioni conduce ad una nuova sequenza $h \left( n - k \right)$ che introduce in frequenza solo una rotazione di fase lineare:
\[
\mathcal{F} \left\{ h \left( n - k \right) \right\} = H \left( e^{j \omega} \right) e^{-j \omega k}
\]
senza variare il modulo della risposta in frequenza, e di conseguenza senza variare la maschera del filtro $\left| H_a \left( j \Omega \right) \right|$.

La rotazione di fase lineare introdotta introduce un ritardo costante sulla sequenza $x \left( n \right)$ di ingresso:
\[
y \left( n \right) = x \left( n \right) * h \left( n - k \right) \Rightarrow Y \left( e^{j \omega} \right) =  X \left( e^{j \omega} \right) \cdot \left[ H \left( e^{j \omega} \right) e^{-j \omega k} \right] = \left[ X \left( e^{j \omega} \right) e^{-j \omega k} \right] \cdot H \left( e^{j \omega} \right) \Rightarrow
\]
\[
\Rightarrow y \left( n \right) = x \left( n-k \right) * h \left( n \right)
\]

Nel caso la risposta all'impulso $h \left( n \right)$ abbia simmetria pari o dispari rispetto all'asse delle ordinate, con conseguente simmetria pari della risposta in frequenza $H \left( e^{j \omega} \right)$, allora basta determinare il nuovo asse di simmetria della risposta all'impulso traslata $h \left( n - k \right)$.

\section{Tecniche a finestra}
La risposta all'impulso $h \left( n \right)$, ottenuta mediante IDTFT della risposta in frequenza $H \left( e^{j 2 \pi f} \right)$, oltre a non essere causale è solitamente di durata infinita $\Rightarrow$ affinché il filtro sia fisicamente realizzabile, la risposta all'impulso deve essere troncata tramite una \textbf{finestra}.

\subsection{Finestra rettangolare}
Alla risposta in frequenza $H_{\text{id}} \left( e^{j 2 \pi f} \right)$ del filtro passa-basso ideale (provvista di rotazione di fase lineare):
\[
H_{\text{id}} \left( e^{j 2 \pi f} \right) = \begin{cases} e^{-j 2 \pi \frac{M-1}{2} f} & \left| f \right| \leq f_c \\
0 & \left| f \right| > f_c \end{cases}
\]
corrisponde una risposta all'impulso $h_{\text{id}}$ a supporto infinito (traslata):
\[
h_{\text{id}} = 2 f_c \text{sinc} \left[ 2 f_c \left( n - \frac{M-1}{2} \right) \right]
\]

\begin{figure}
	\centering
	\includegraphics[scale=0.6]{pic/B10/Window_function_and_frequency_response_3}
\end{figure}

La risposta all'impulso $h_{\text{id}}$ viene troncata a un numero finito $M$ di campioni con la moltiplicazione per la porta $p_M \left( n \right)$ (\textbf{finestra rettangolare}):
\[ h \left( n \right) = h_{\text{id}} \left( n \right) \cdot p_M \left( n \right) = \begin{cases} h_{\text{id}} & n = 0 , \ldots , M-1 \\
0 & \text{altrimenti} \end{cases} \]
\FloatBarrier

\begin{figure}
\centering
	\begin{subfigure}[b]{.5\textwidth}
		\centering
		\includegraphics[width=\linewidth]{pic/B10/Finestra_rettangolare_Gibbs}
	\end{subfigure}%
	\begin{subfigure}[b]{.5\textwidth}
		\centering
		\includegraphics[scale=0.6]{pic/B10/Window_function_and_frequency_response_4}
	\end{subfigure}
\end{figure}

Una troncatura così brutale però si manifesta in frequenza con la comparsa di oscillazioni  (\textbf{effetto di Gibbs}):
\[ H \left( e^{j2 \pi f} \right) = H_{\text{id}} \left( e^{j 2 \pi f} \right) * P_M \left( e^{j2 \pi f} \right) \]
dove:
\[ P_M \left( e^{j2 \pi f} \right) = \frac{1- e^{-j2 \pi f M}}{1- e^{-j2 \pi f}} = e^{-j 2 \pi f \frac{M-1}{2}} \frac{\sin{\left( M \pi f \right)}}{\sin{\left( \pi f \right)}} \]

\paragraph{Osservazioni}
\begin{itemize}
\item L'ampiezza massima delle oscillazioni in banda attenuata coincide con l'ampiezza massima delle oscillazioni in banda passante.
\item All'aumentare di $M$ aumenta la frequenza di ripetizione delle oscillazioni, \ul{non} la loro ampiezza massima $\Rightarrow$ la riduzione dell'oscillazione massima nella risposta in frequenza può essere ottenuta solamente cambiando il tipo di finestra di troncamento, \ul{non} la sua lunghezza.
\end{itemize}
\FloatBarrier

\subsection{Finestra di Bartlett}
\begin{figure}
	\centering
	\includegraphics[scale=0.6]{pic/B10/Window_function_and_frequency_response_5}
\end{figure}

\noindent
La \textbf{finestra di Bartlett} riduce leggermente le oscillazioni:
\[
p \left( n \right) = \begin{cases} \displaystyle \frac{2n}{M-1} & \displaystyle 0 \leq n \leq \frac{M-1}{2} \\
\displaystyle 2 - \frac{2n}{M-1} & \displaystyle \frac{M-1}{2} \leq n \leq M-1 \\
0 & \text{altrimenti} \end{cases}
\]
\FloatBarrier

\subsection{Finestra di Hanning}
\begin{figure}
	\centering
	\includegraphics[scale=0.6]{pic/B10/Window_function_and_frequency_response_6}
\end{figure}

\noindent
La \textbf{finestra di Hanning} è una funzione triangolo, e riduce ulteriormente le oscillazioni:
\[
p \left( n \right) = \begin{cases} \displaystyle \frac{1}{2} \left[ 1 - \cos{\left( \frac{2 \pi n}{M-1} \right)} \right] & 0 \leq n \leq M-1 \\
0 & \text{altrimenti} \end{cases}
\]
\FloatBarrier

\subsection{Finestra di Hamming}
\begin{figure}
	\centering
	\includegraphics[scale=0.6]{pic/B10/Window_function_and_frequency_response_1}
\end{figure}

\noindent
La \textbf{finestra di Hamming} è una finestra di Hanning più attenuata verso gli estremi del supporto, e riduce ulteriormente le oscillazioni:
\[
p \left( n \right) = \begin{cases} \displaystyle 0,54 - 0,46 \cos{\left( \frac{2 \pi n}{M-1} \right)} & 0 \leq n \leq M-1 \\
0 & \text{altrimenti} \end{cases}
\]
\FloatBarrier

\subsection{Finestra di Blackman}
\begin{figure}[H]%WARNING
	\centering
	\includegraphics[scale=0.6]{pic/B10/Window_function_and_frequency_response_2}
\end{figure}

\noindent
La \textbf{finestra di Blackman} è una finestra di Hamming più attenuata verso gli estremi del supporto, e riduce ulteriormente le oscillazioni:
\[
p \left( n \right) = \begin{cases} \displaystyle 0,42 - \frac{1}{2} \cos{\left( \frac{2 \pi n}{M-1} \right)} + 0,08 \cos{\left( \frac{4 \pi n}{M-1} \right)} & 0 \leq n \leq M-1 \\
0 & \text{altrimenti} \end{cases}
\]
%\FloatBarrier

\noindent\rule{\linewidth}{0.2mm}
\vspace*{\dimexpr-\parskip-.5\baselineskip}

Un aumento della minima attenuazione in banda attenuata $A_s$ si paga però con l'aumento della banda di transizione $B_T$:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|}
  \hline
  \textbf{Finestra} & \begin{tabular}[c]{@{}c@{}}\textbf{Banda di}\\\textbf{transizione $B_T$}\end{tabular} & \begin{tabular}[c]{@{}c@{}}\textbf{Minima attenuazione}\\\textbf{in banda attenuata $A_s$}\end{tabular} \\
  \hline
  \textbf{Rettangolare} & $\displaystyle \frac{1,8 \pi}{M}$ & $\displaystyle 21 \; \text{dB}$ \\
  \hline
  \textbf{Bartlett} & $\displaystyle \frac{6,1 \pi}{M}$ & $25 \; \text{dB}$ \\
  \hline
  \textbf{Hanning} & $\displaystyle \frac{6,2 \pi}{M}$ & $44 \; \text{dB}$ \\
  \hline
  \textbf{Hamming} & $\displaystyle \frac{6,6 \pi}{M}$ & $53 \; \text{dB}$ \\
  \hline
  \textbf{Blackman} & $\displaystyle \frac{11 \pi}{M}$ & $74 \; \text{dB}$ \\
  \hline
 \end{tabular}}
\end{table}