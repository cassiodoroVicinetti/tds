\chapter{Analisi in frequenza di segnali a tempo continuo campionati}
\section{DTFT di un segnale analogico campionato}
La trasformata di Fourier $X_d \left( f_a \right)$ del segnale $x_d \left( t \right)$, cioè il segnale analogico $x \left( t \right)$ campionato a frequenza $f_c$:
\[
x_d \left( t \right) = x \left( t \right) \cdot \sum_{k = - \infty}^{+ \infty} \delta \left( t - k T_c \right) = \sum_{k = - \infty}^{+ \infty} x \left( k T_c \right) \delta \left( t - k T_c \right)
\]
si può esprimere in due modi del tutto equivalenti:
\[
X_d \left( f_a \right) = \mathcal{F} \left\{ \sum_{k = - \infty}^{+ \infty}  x \left( t \right) \delta \left( t - k T_c \right) \right\} =
\]
\[
= \begin{cases} \displaystyle \mathcal {F} \left\{ x \left( t \right) \right\} * \mathcal{F} \left\{ \sum_{k = - \infty}^{+ \infty} \delta \left( t - k T_c \right) \right\} = \ldots = \frac{1}{T_c} \sum_{k = - \infty}^{+ \infty} X \left( f_a - k \frac{1}{T_c} \right) \\
\displaystyle \mathcal{F} \left\{ \sum_{k = - \infty}^{+ \infty}  x \left( k T_c \right) \cdot \delta \left( t - k T_c \right) \right\} = \ldots = \sum_{k = - \infty}^{+ \infty} x \left( k T_c \right) e^{-j 2 \pi k T_c f_a} \end{cases}
\]

La DTFT $X \left( e^{j2 \pi f} \right)$ del segnale a tempo discreto $x \left( k \right)$ può essere ricondotta alla trasformata di Fourier $X_d \left( f_a \right)$ del corrispondente segnale analogico $x \left( k T_c \right)$, campionato con una frequenza $f_c = \tfrac{1}{T_c}$ che rispetta il teorema del campionamento:
\[
\begin{cases} \displaystyle X \left( e^{j2 \pi f} \right) = \sum_{k = - \infty}^{+ \infty} x \left( k \right) e^{-j2 \pi f} \\
\displaystyle X_d \left( f_a \right)  = \sum_{k = - \infty}^{+ \infty} x \left( k T_c \right) e^{-j 2 \pi k T_c f_a} \end{cases} \Rightarrow X_d \left( f \cdot f_c \right) = \sum_{k = - \infty}^{+ \infty} x \left( k \right) e^{-j2 \pi k f} = X \left( e^{j2 \pi f} \right)
\]

Per ricostruire perfettamente il segnale campionato, serve un filtro passa-basso ideale $H \left( f \right)$, che è la porta di ampiezza $T_c$ e supporto $\left[ - \tfrac{f_c}{2}, \tfrac{f_c}{2} \right]$, per eliminare le repliche dello spettro periodico $X_d \left( f_a \right)$ lasciando solo quella centrata intorno all'origine ($k = 0$):
\[
X \left( f_a \right) = H \left( f \right) * X_d \left( f_a \right) = T_c X_d \left( f_a \right) \quad f_a \in \left[ - \frac{f_c}{2} , \frac{f_c}{2} \right]
\]

Quindi il legame tra la DTFT del segnale a tempo discreto $x \left( k \right)$ e la trasformata di Fourier del segnale campionato $x \left( k T_c \right)$ filtrata con un filtro passa-basso è:
\[
X \left( f \cdot f_c \right) = T_c \cdot X_d \left( f \cdot f_c \right) = T_c \cdot X \left( e^{j2 \pi f} \right) \quad f_a \in \left[ - \frac{f_c}{2} , \frac{f_c}{2} \right]
\]

\section{DFT di un segnale analogico campionato}
\paragraph{Ipotesi}
\begin{itemize}
\item il segnale $x \left( t \right)$ ha supporto $\left( 0 , T_x \right)$ limitato nel tempo;
\item il segnale $x \left( t \right)$ è a banda limitata (pari a $B_x$);
\item il segnale $x \left( t \right)$ viene campionato con una frequenza di campionamento $f_c \geq 2 B_x$ per evitare l'aliasing in frequenza, generando la sequenza:
\[
x \left( n \right) = x \left( n T_c \right) \quad n = 0 , \ldots , N-1
\]
\item si considera un intervallo di tempo $T_0$:
\[
T_0 = \frac{1}{\Delta f} = N T_c = \frac{N}{f_c}
\]
che includa il supporto $T_x$ del segnale $x \left( t \right)$, per evitare l'aliasing nel tempo:
\[
T_0  \geq T_x
\]
\end{itemize}

L'uso della DFT al posto della DTFT permette di lavorare su frequenze discrete e su un numero finito di campioni nel tempo:
\[
\begin{cases} \displaystyle X \left( f \cdot f_c \right) = X \left( f \cdot \frac{N}{T_0} \right) = \frac{T_0}{N} \cdot X \left( e^{j2 \pi f} \right) \\
\displaystyle X \left( k \right) = X \left( e^{j2 \pi \frac{k}{N}} \right)  \end{cases} \Rightarrow X \left( \frac{k}{T_0} \right) = \frac{T_0}{N}  X \left( k \right) \quad k = 0 , \ldots , N-1
\]

Aumentando l'intervallo $T_0$ su cui si osserva il segnale con la tecnica dell'aggiunta degli zeri (a parità di frequenza di campionamento), si può aumentare la \textbf{risoluzione in frequenza} dello spettro, perché la trasformata di Fourier $X \left( \tfrac{k}{T_0} \right)$ del segnale viene campionata con una minore spaziatura in frequenza $\Delta f = \tfrac{1}{T_0}$.

\paragraph{Parametri}
La scelta dei parametri deve essere fatta in modo da limitare l'aliasing nel tempo e in frequenza:
\[
\begin{cases} T_0 = N T_c \\
f_c = \frac{1}{T_c} \\
\Delta f = \frac{1}{T_0} \end{cases}
\]

\begin{figure}[H]
\centering
	\begin{subfigure}[b]{.4875\textwidth}
		\centering
		\includegraphics[width=\linewidth]{pic/B05/DFT_segnali_campionati_aliasing_in_frequenza}
		\caption{Esempio di aliasing in frequenza (visibile soprattutto agli estremi), dovuto a frequenza di campionamento $f_c$ troppo bassa}
	\end{subfigure}%
	\hspace{0.025\textwidth}%
	\begin{subfigure}[b]{.4875\textwidth}
		\centering
		\includegraphics[width=\linewidth]{pic/B05/DFT_segnali_campionati_aliasing_nel_tempo}
		\caption{Esempio di aliasing nel tempo (visibile soprattutto intorno all'origine), dovuto a intervallo di tempo $T_0$ troppo basso}
	\end{subfigure}
\end{figure}