\chapter{Analisi dei sistemi LTI mediante trasformata zeta}
Un sistema LTI può essere descritto usando la trasformata zeta:
\[
\begin{cases} y \left( n \right) = h \left( n \right) * x \left( n \right) \\
Y \left( z \right) = H \left( z \right) X \left( z \right) \end{cases}
\]

\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|}
  \hline
  \textbf{Serie} & \begin{tabular}[c]{@{}c@{}}\includegraphics[scale=0.5]{pic/B06/Diagramma_serie_n.png}\end{tabular} & \begin{tabular}[c]{@{}c@{}}$ \displaystyle y \left( n \right) = x \left( n \right) * h_1 \left( n \right) * h_2 \left( n \right)$\\
  $\displaystyle Y \left( z \right) = X \left( z \right) H_1 \left( z \right) H_2 \left( z \right)$\end{tabular} \\
  \hline
  \textbf{Parallelo} & \begin{tabular}[c]{@{}c@{}}\includegraphics[scale=0.45]{pic/B06/Diagramma_parallelo_n.png}\end{tabular} & \begin{tabular}[c]{@{}c@{}}$ \displaystyle y \left( n \right) = x \left( n \right) * \left[ h_1 \left( n \right) + h_2 \left( n \right) \right]$\\
  $\displaystyle Y \left( z \right) = X \left( z \right) \left[ H_1 \left( z \right) + H_2 \left( z \right) \right]$\end{tabular} \\
  \hline
  \begin{tabular}[c]{@{}c@{}}\textbf{Con}\\\textbf{reazione}\end{tabular} & \begin{tabular}[c]{@{}c@{}}\includegraphics[scale=0.45]{pic/B06/Diagramma_retroazione_n.png}\end{tabular} & \begin{tabular}[c]{@{}c@{}}$ \displaystyle y \left( n \right) = \left[ x \left( n \right) - y \left( n \right) * h_2 \left( n \right) \right] * h_1 \left( n \right)$\\
  $\displaystyle Y \left( z \right) = X \left( z \right) \frac{H_1 \left( z \right)}{1 + H_1 \left( z \right) H_2 \left( z \right)}$\end{tabular} \\
  \hline
 \end{tabular}}
 \caption{Interconnessione di sistemi LTI.}
\end{table}

La regione di convergenza della trasformata $Y \left( z \right)$ coincide con l'intersezione tra le regioni di convergenza delle funzioni $X \left( z \right)$ e $H \left( z \right)$ (la cancellazione di poli e/o zeri estenderà la regione).

\section{Filtri digitali}
Un \textbf{filtro\footnote{Un \textbf{filtro} è un sistema che seleziona una certa banda di frequenze di un segnale.} digitale} è un sistema LTI causale e scarico\footnote{Si veda la sezione~\ref{sez:risposta_forzata}.}:
\[
\begin{cases} \displaystyle y \left( n \right) = \overbrace{- \sum_{k = 1}^M a_k y \left( n - k \right)}^{\text{ricorsivo}} + \overbrace{\sum_{k=0}^N b_k x \left( n  - k \right)}^{\text{non ricorsivo}} \\
\displaystyle Y^+ \left( z \right) = \frac{\displaystyle \sum_{k=0}^N b_k z^{-k}}{\displaystyle 1+ \sum_{k=1}^M a_k z^{-k}} X^+ \left( z \right) = H_N \left( z \right) H_R \left( z \right) X^+ \left( z \right) \end{cases}
\]

\subsection{Filtri FIR non ricorsivi}
Un sistema LTI causale con risposta all'impulso $h \left( n \right)$ a supporto finito può essere descritto:
\[
\begin{cases} \displaystyle y \left( n \right) = \sum_{k=0}^N b_k x \left( n  - k \right) \\
\displaystyle H_N \left( z \right) = \sum_{z =0}^N b_k z^{-k} \end{cases}
\]

\paragraph{Vantaggi dei filtri FIR}
\begin{itemize}
\item Sono sempre stabili perché la funzione di trasferimento:
\[
H \left( z \right) = \sum_{n=0}^{M-1} a_n z^{-n}
\]
possiede solo zeri e un polo multiplo nell'origine (quindi entro il cerchio di raggio unitario).
\item Possono essere progettati in modo da avere fase lineare.
\end{itemize}

\subsection{Filtri IIR puramente ricorsivi}
Un sistema LTI causale puramente ricorsivo può essere descritto:
\[
\begin{cases} \displaystyle y \left( n \right) = - \sum_{k = 1}^M a_k y \left( n - k \right) \\
\displaystyle H_R \left( z \right) = \frac{1}{\displaystyle 1+ \sum_{k=1}^M a_k z^{-k}} \end{cases}
\]

\paragraph{Vantaggio dei filtri IIR}
Generalmente soddisfano le specifiche di progetto con il minor numero possibile di coefficienti.

\section{Sistemi LTI non scarichi}
Se le condizioni iniziali $y \left( - 1 \right), y \left( - 1 \right), \ldots , y \left( -M \right)$ non sono nulle, bisogna considerare anche i campioni di $y \left( n \right)$ e $x \left( n \right)$ presi in valori negativi (da $-k$ a $-1$, con $k>0$):
\[
\begin{cases} \displaystyle y \left( n \right) = - \sum_{k = 1}^M a_k y \left( n - k \right) + \sum_{k=0}^N b_k x \left( n  - k \right) \\
\displaystyle Y \left( z \right) = - \sum_{k =1}^M a_k z^{-k} \left[ Y^+ \left( z \right) + \sum_{n=1}^k y \left( -n \right) z^n \right] + \sum_{k=0}^N b_k  z^{-k} \left[ X^+ \left( z \right) + \sum_{n=1}^k x \left( -n \right) z^n \right] \end{cases}
\]

\section{Stabilità di sistemi LTI}
\subsection{Sistemi LTI causali}
Un sistema LTI causale è BIBO-stabile\footnote{Si veda la sezione~\ref{sez:stabilità_BIBO}.} se la sua risposta all'impulso $h \left( n \right)$:
\[ y \left( n \right) = \sum_{k = 0}^{+ \infty} h \left( k \right) * x \left( n-k \right) \]
è sommabile in modulo:
\[
\sum_{n=0}^{+ \infty} \left| h \left( n \right) \right| \in \mathbb{R}
\]

Un sistema LTI causale è stabile se e solo se tutti i poli della risposta in frequenza $H \left( z \right)$ appartengono al cerchio di raggio unitario.
\begin{framed}
\paragraph{Dimostrazione}
\subparagraph{Ipotesi}
\begin{itemize}
\item $H \left( z \right)$ ha poli semplici
\item $p_n < p_d$
\end{itemize}

\[ \begin{cases} \displaystyle H \left( z \right) = \sum_{i = 1}^{p_d} \frac{\text{Res} \left( z_i \right)}{1- d_i z^{-1}} \\
\displaystyle h \left( n \right) = \sum_{i = 1}^{p_d} \text{Res} \left( z_i \right) \cdot d_i^n u \left( n \right) \end{cases} \]

\subparagraph{Condizione necessaria}
\[
\sum_{n=0}^{+ \infty} \left| h \left( n \right) \right| = \sum_{n = 0}^{+ \infty} \left|  \sum_{i = 1}^{p_d} \text{Res} \left( z_i \right) \cdot d_i^n u \left( n \right) \right| \leq \sum_{n = 0}^{+ \infty} \sum_{i = 1}^{p_d} \left| \text{Res} \left( z_i \right) \right| {\left| d_i \right|}^n = \sum_{n = 0}^{+ \infty} \left| \text{Res} \left( z_i \right) \right| \sum_{i = 1}^{p_d} {\left| d_i \right|}^n
\]

Se tutti i poli $d_i$ appartengono al cerchio di raggio unitario, il sistema è stabile:
\[
\sum_{n = 0}^{+ \infty} \left| \text{Res} \left( z_i \right) \right| \sum_{i = 1}^{p_d} {\left| d_i \right|}^n = \sum_{n = 0}^{+ \infty} \left| \text{Res} \left( z_i \right) \right| \frac{1}{1- \left| d_i \right|} \in \mathbb{R} \Rightarrow \sum_{n=0}^{+ \infty} \left| h \left( n \right) \right| \in \mathbb{R}
\]

\paragraph{Condizione sufficiente}
\[
\sum_{n=0}^{+ \infty} \left| h \left( n \right) \right| \in R \Rightarrow \lim_{n \to + \infty} h \left( n \right) = 0
\]

\[ \lim_{n \to + \infty} h \left( n \right) = \lim_{n \to + \infty} \sum_{i = 1}^{p_d} \text{Res} \left( z_i \right) \cdot d_i^n u \left( n \right) = \]
\[ = \sum_{i = 1}^{p_d} \text{Res} \left( z_i \right) \cdot \lim_{n \to + \infty} d_i^n = 0 \Leftrightarrow d_i < 1 \quad i=1, \ldots, p_d \]
\end{framed}

Se un sistema LTI causale è stabile, la sua circonferenza di raggio unitario è inclusa nella sua regione di convergenza, che per i sistemi causali si estende all'esterno della circonferenza che comprende i poli.

\subsection{Sistemi LTI anti-causali}
Un sistema LTI anti-causale è stabile se e solo se tutti i poli della risposta in frequenza $H \left( z \right)$ non appartengono al cerchio di raggio unitario.

Se un sistema LTI anti-causale è stabile, la sua circonferenza di raggio unitario è inclusa nella regione di convergenza, che per i sistemi anti-causali si estende all'interno della circonferenza che comprende i poli.

\subsection{Sistemi LTI bilateri}
Se un sistema LTI bilatero è stabile, la sua circonferenza di raggio unitario è inclusa nella regione di convergenza, che per i sistemi bilateri è un anello circolare.

\section{Realizzabilità fisica di sistemi LTI causali}
\label{sez:realizzabilita_fisica_di_sistemi_lti_causali}
Un sistema LTI causale\footnote{Si suppone scarico.} è fisicamente realizzabile\footnote{Si veda la sezione~\ref{sez:b06_realizzabilita_fisica}.} se la sua risposta all'impulso $h \left( n \right)$ è reale, ossia ogni coefficiente $b_i$ e $a_i$ della sua risposta in frequenza $H \left( z \right)$:
\[ H \left( z \right) = \frac{\displaystyle \sum_{k=0}^N b_k z^{-k}}{\displaystyle 1+ \sum_{k=1}^M a_k z^{-k}} \]
è reale o è accoppiato con il suo complesso coniugato.\footnote{Infatti:
\[ \left( z - \alpha \right) \left( z - \alpha^* \right) = z^2 - \alpha z - \alpha^* z + {\left| \alpha \right|}^2 = z^2 -  \left( \alpha + \alpha^* \right) z + {\left| \alpha \right|}^2 = z^2 - 2 \Re{ \left\{ \alpha \right\} } z + {\left| \alpha \right|}^2 \]}

\section{Sistemi inversi}
Molto spesso nelle applicazioni pratiche è richiesta l'implementazione di un sistema LTI, chiamato \textbf{sistema inverso}, che inverta le caratteristiche di un altro sistema caratterizzato dalla trasformata $H \left( z \right)$:
\[ H_I \left( z \right) = H^{-1} \left( z \right) = \frac{1}{H \left( z \right)} \]

Ad esempio, nella trasmissione di dati attraverso il canale telefonico, al terminale di ricezione è necessario eliminare la distorsione del canale applicando al segnale un sistema inverso a quello del canale.

La cascata di un sistema con il suo inverso è chiamato \textbf{sistema identità}:
\[ \begin{cases} H_C \left( z \right) = H \left( z \right) H_I \left( z \right) = 1 \\ h_c \left( n \right) = \delta \left( n \right) \end{cases} \]

Se $H \left( z \right)$ ha forma razionale:
\[
H \left( z \right) = \frac{b_0}{a_0} \frac{\displaystyle \prod_{i=1}^{p_n} \left( 1- c_i z^{-1} \right)}{\displaystyle \prod_{i=1}^{p_d} \left( 1- d_i z^{-1} \right)}
\]
la risposta in frequenza $H_I \left( z \right)$ del sistema inverso vale:
\[
H_I \left( z \right) = \frac{b_0}{a_0} \frac{\displaystyle \prod_{i=1}^{p_d} \left( 1- d_i z^{-1} \right)}{\displaystyle \prod_{i=1}^{p_n} \left( 1- c_i z^{-1} \right)}
\]

Se il sistema inverso è causale, gli \ul{zeri} $c_i$ del sistema originario sono all'esterno della sua regione di convergenza. Se il sistema inverso è anche stabile, gli zeri $c_i$ del sistema originario sono contenuti nel cerchio di raggio unitario.

Quindi un sistema LTI causale e stabile ammette un sistema inverso causale e stabile se anche i suoi zeri sono contenuti nel cerchio di raggio unitario.