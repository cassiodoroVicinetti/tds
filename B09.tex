\chapter{Progetto di filtri IIR}
L'approccio più comune per affrontare il progetto di filtri IIR consiste nel convertire un filtro analogico di funzione di trasferimento $H_a \left( s \right)$ in un filtro numerico di funzione di trasferimento $H \left( z \right)$ che garantisca le specifiche richieste. Le conversioni devono preservare la causalità e la stabilità del filtro stesso.

\begin{enumerate}
\item Si definiscono i requisiti di progetto del filtro analogico passa-basso.
\item Si genera la risposta in frequenza $H_a \left( s \right)$ del filtro analogico tramite approssimazione con modelli analogici standard (come il filtro di Butterworth\footnote{Si veda la sezione~\ref{sez:filtro_di_butterworth}.}).
\item Si applica la trasformazione bilineare\footnote{Si veda la sezione~\ref{sez:tecnica_della_trasformazione_bilineare}.} per passare dal tempo continuo al tempo discreto, trovando la risposta in frequenza $H \left( z \right)$ del filtro numerico.
\item Si trasforma lo spettro passa-basso in uno spettro passa-alto o passa-banda.
\end{enumerate}

\section{Prototipo di filtro analogico}
Il progetto di un filtro IIR richiede la sintesi di un filtro analogico con funzione di trasferimento $H_a \left( s \right)$:
\[
H_a \left( s \right) = \mathcal{L} \left\{ h_a \left( t \right) \right\} = \int_{- \infty}^{+ \infty} h_a \left( t \right) e^{-s t} dt
\]
dove $s= \sigma + j \Omega = \sigma + j 2 \pi f_a$. Siccome il filtro dev'essere fisicamente realizzabile\footnote{Si veda la sezione~\ref{sez:realizzabilita_fisica_di_sistemi_lti_causali}.}, la sua risposta all'impulso $h_a \left( t \right)$ è causale e la sua risposta in frequenza $H_a \left( s \right)$ è unilatera:
\[
H_a^+ \left( s \right) = \int_0^{+ \infty} h_a \left( t \right) e^{-st} dt
\]

L'insieme dei valori di $\sigma = \Re{\left\{  s \right\}}$ per cui l'integrale in $H_a \left( s \right)$ converge è definita regione di convergenza della trasformata di Laplace. Per sistemi causali, la regione di convergenza della trasformata di Laplace è costituita dal semipiano complesso alla destra di una retta verticale. Se $\sigma = 0$, la regione di convergenza è il semipiano complesso destro:
\[
H_a \left( j \Omega \right) = \mathcal{F} \left\{ h_a \left( t \right) \right\} = \int_{- \infty}^{+ \infty} h_a \left( t \right) e^{-j \Omega t} dt = \left. H_a \left( s \right) \right\vert_{s = j \Omega}
\]

\subsection{Maschera delle specifiche}
\begin{figure}
	\centering
	\includegraphics[scale=0.55]{pic/B09/Funzione_di_trasferimento_filtro_passa-basso}
\end{figure}

\noindent
Il filtro ideale non è fisicamente realizzabile $\Rightarrow$ le specifiche di progetto devono imporre delle tolleranze della risposta in frequenza $H_a \left( j \Omega \right)$ del filtro analogico rispetto al filtro ideale:
\begin{itemize}
\item l'oscillazione massima in banda passante $2 \delta_1$, idealmente nulla;
\item l'oscillazione massima in banda attenuata $\delta_2$, idealmente nulla;
\item la banda di transizione $\Omega_s - \Omega_p$, idealmente nulla.
\end{itemize}
\FloatBarrier

\begin{figure}
	\centering
	\includegraphics[scale=0.6]{pic/B09/Maschera_delle_specifiche_dB}
\end{figure}

Spesso le specifiche di progetto dei filtri vengono date in termini della funzione di trasferimento in modulo, normalizzata rispetto al valore massimo ed espressa in scala logaritmica:
\[
-10 \log_{10}{{ \left( \frac{\left| H_a \left( j \Omega \right) \right|}{\max{ \left\{ \left| H_a \left( j \Omega \right) \right| \right\} }} \right)}^2} = -20 \log_{10}{\frac{\left| H_a \left( j \Omega \right) \right|}{\max{ \left\{ \left| H_a \left( j \Omega \right) \right| \right\} }} }
\]
con parametri:
\begin{itemize}
\item ripple in banda passante, idealmente nullo:
\[ R_p = -20 \log_{10}{\frac{1- \delta_1}{1+ \delta_1}} \]
\item attenuazione in banda attenuata, idealmente infinita:
\[
A_s = -20 \log_{10}{\frac{\delta_2}{1+ \delta_1}}
\]
\end{itemize}
\FloatBarrier

\subsection{Distribuzione di poli e zeri}
\begin{figure}[H]%WARNING
	\centering
	\includegraphics[scale=0.7]{pic/B09/Distribuzione_di_poli_e_zeri}
\end{figure}

Le specifiche del filtro analogico sono tipicamente fornite in termini del suo spettro di energia ${\left| H_a \left( s \right) \right|}^2$:
\[
{\left| H_a \left( s \right) \right|}^2 = {\left| H_a \left( \sigma + j \Omega \right) \right|}^2 = H_a \left(  \sigma + j \Omega \right) H_a^* \left( \sigma + j \Omega \right) =
\]

Siccome un filtro fisicamente realizzabile ha una risposta all'impulso $h_a \left( t \right)$ reale, e quindi tutti i poli e gli zeri sono reali o a coppie complesse coniugate (quindi simmetrici rispetto all'asse reale):
\[
= H_a \left( \sigma + j \Omega \right) H_a \left( \sigma - j \Omega \right) = H_a \left( s \right) H_a \left( - s \right)
\]

L'obiettivo del progetto consiste nell'assegnare metà dei poli a $H_a \left( s \right)$ e l'altra metà a $H_a \left( - s \right)$.

Un filtro causale e stabile ha tutti i poli nel semipiano di sinistra ($\sigma <0$) $\Rightarrow$ i poli di $H_a \left( s \right)$, che sono simmetrici rispetto all'asse reale, devono giacere tutti nel semipiano di sinistra. $H_a \left( -s \right)$ ha gli stessi zeri e poli di $H_a \left( s \right)$ ma ribaltati rispetto all'asse immaginario.
%\FloatBarrier

\subsection{Filtro di Butterworth}
\label{sez:filtro_di_butterworth}
\begin{figure}
	\centering
	\includegraphics[width=0.8\linewidth]{pic/B09/Butterworth_filter_bode_plot}
	\caption[]{\footnotemark}
\end{figure}
\footnotetext{Questa immagine è tratta da Wikimedia Commons (\href{https://commons.wikimedia.org/wiki/File:Butterworth_filter_bode_plot.svg}{Butterworth filter bode plot.svg}), è stata realizzata dall'utente \href{https://en.wikipedia.org/wiki/User:Omegatron}{Omegatron} e da \href{https://en.wikipedia.org/wiki/User:Alejo2083}{Alessio Damato} ed è concessa sotto la \href{http://creativecommons.org/licenses/by-sa/3.0/deed.it}{licenza Creative Commons Attribuzione - Condividi allo stesso modo 3.0 Unported}.}

\noindent
Il \textbf{filtro di Butterworth} passa-basso di ordine $N$ è un prototipo di filtro analogico:
\[
{\left| H_a \left( j \Omega \right) \right|}^2 = \frac{1}{\displaystyle 1+ {\left( \frac{\Omega}{\Omega_c} \right)}^{2N}}
\]
dove la \textbf{pulsazione di taglio} $\Omega_c$ è contenuta all'interno della banda di transizione:
\[
\Omega_p \leq \Omega_c \leq \Omega_s
\]

\paragraph{Proprietà}
La risposta in frequenza in modulo $\left| H \left( j \Omega \right) \right|$ è:
\begin{itemize}
\item quasi piatta sia in banda passante sia in banda attenuata;
\item una funzione sempre decrescente della variabile $j \Omega$;
\item al crescere dell'ordine $N$ si avvicina sempre di più all'idealità;
\item nell'origine ($\Omega = 0$) è sempre pari a 1:
\[
{\left| H_a \left( 0 \right) \right|}^2 = 1
\]
\item alla frequenza di taglio ($\Omega = \Omega_c$) è sempre pari a $\tfrac{1}{2}$:
\[
{\left| H_a \left( j \Omega_c \right) \right|}^2 = \frac{1}{2}
\]
\end{itemize}
\FloatBarrier

\subsubsection{Distribuzione dei poli}
\begin{figure}
	\centering
	\includegraphics[scale=0.7]{pic/B09/Distribuzione_poli_filtro_di_Butterworth}
\end{figure}

\noindent
Il filtro di Butterworth non ha zeri, ma solo poli, che sono posizionati in modo equispaziato lungo la circonferenza di raggio $\Omega_c$ nel piano $s$:
\[
p_k = \Omega_c e^{j \frac{k \pi}{N}} e^{j \frac{\pi \left( N + 1 \right)}{2N}} \quad k = 0, 1, \ldots , 2N-1
\]
\begin{framed}
\paragraph{Dimostrazione}
\[
{\left| H_a \left( j \Omega \right) \right|}^2 = \frac{1}{\displaystyle 1+ {\left( \frac{\Omega}{\Omega_c} \right)}^{2N}} = \frac{{\left( j \Omega_c \right)}^{2N}}{{	\left( j \Omega \right)}^{2N} + {\left( j \Omega_c \right)}^{2N}}
\]

I poli $p_k$ sono:
\[
p_k = {\left( -1 \right)}^{\frac{1}{2N}} \left( j \Omega_c \right) = \Omega_c e^{j \frac{\pi}{2}} e^{j \frac{\pi  + 2k \pi}{2N}} = \Omega_c e^{j \frac{\pi}{2N} \left( 2k + N + 1 \right)} \quad k = 0, 1 , \ldots , 2N - 1
\]
\end{framed}
\FloatBarrier

\subsubsection{Progetto del filtro}
Il progetto del filtro consiste nel ricavare l'ordine $N$ e la pulsazione di taglio $\Omega_c$ partendo dalle specifiche, ossia dai valori di $\Omega_p$, $\Omega_s$, ${\mathbb{R}}_p$ e $A_s$:
\[
\begin{cases} \displaystyle N = \frac{\displaystyle \log_{10}{\frac{\displaystyle {10}^{\frac{R_p}{10}} - 1}{\displaystyle {10}^{\frac{A_s}{10}} - 1}}}{\displaystyle 2 \log_{10}{\frac{\Omega_p}{\Omega_s}}} \\ \displaystyle \Omega_c = \frac{\Omega_p}{\sqrt[2N]{{10}^{\frac{R_p}{10}}  -1}} = \frac{\Omega_s}{\sqrt[2N]{{10}^{\frac{A_s}{10}}  -1}} \end{cases}
\]
\begin{framed}
\paragraph{Dimostrazione}
La funzione di trasferimento in modulo, espressa in scala logaritmica, % TODO perché non è normalizzata rispetto al valore massimo?
deve passare per i punti $\left( \Omega_p , R_p \right)$ e $\left( \Omega_s , A_s \right)$:
\[
\begin{cases} \displaystyle -10 \log_{10}{{ \left( \frac{\displaystyle \left| H_a \left( j \Omega_p \right) \right|}{\displaystyle \max{ \left\{ \left| H_a \left( j \Omega \right) \right| \right\} }} \right)}^2} = R_p \\
\displaystyle -10 \log_{10}{{ \left( \frac{\displaystyle \left| H_a \left( j \Omega_s \right) \right|}{\displaystyle \max{ \left\{ \left| H_a \left( j \Omega \right) \right| \right\} }} \right)}^2} = A_s \\
\displaystyle {\left| H_a \left( j \Omega \right) \right|}^2 = \frac{\displaystyle 1}{\displaystyle 1+ {\left( \frac{\Omega}{\Omega_c} \right)}^{2N}} \end{cases} \Rightarrow \begin{cases} \displaystyle \frac{\displaystyle 1}{\displaystyle 1+ {\left( \frac{\Omega_p}{\Omega_c} \right)}^{2N}} = {10}^{\frac{R_p}{10}} \\ 
\displaystyle \frac{\displaystyle 1}{\displaystyle 1+ {\left( \frac{\Omega_s}{\Omega_c} \right)}^{2N}} = {10}^{\frac{A_s}{10}} \end{cases}
\]
\end{framed}

L'ordine $N$ dev'essere un numero intero $\Rightarrow$ si approssima all'intero superiore in modo da soddisfare le specifiche per eccesso:
\[
N = \left \lceil \frac{\log_{10}{\displaystyle \frac{{10}^{\frac{R_p}{10}} - 1}{\displaystyle {10}^{\frac{A_s}{10}} - 1}}}{2 \log_{10}{\frac{\displaystyle \Omega_p}{\displaystyle \Omega_s}}} \right \rceil
\]

Se avviene questa approssimazione, però, i valori di $\Omega_c$ non sono univoci:
\[
\frac{\Omega_p}{\sqrt[2N]{{10}^{\frac{R_p}{10}}  -1}} \neq \frac{\Omega_s}{\sqrt[2N]{{10}^{\frac{A_s}{10}}  -1}}
\]
\begin{itemize}
\item la scelta del valore per $\Omega_p$ produce un filtro più vicino a 0 in banda attenuata;\footnote{in scala lineare}
\item la scelta del valore per $\Omega_s$ produce un filtro più vicino a 1 in banda passante.\footnotemark[\value{footnote}]
\end{itemize}

Le specifiche sono soddisfatte per qualunque $\Omega_c$ scelto tra questi due valori $\Rightarrow$ conviene scegliere il valore medio come compromesso.

\section{Tecnica della trasformazione bilineare}
\label{sez:tecnica_della_trasformazione_bilineare}
La \textbf{trasformazione bilineare} consente di ottenere un filtro numerico $H \left( z \right)$ partendo da un filtro analogico $H_a \left( s \right)$:
\[ H \left( z \right) = H_a \left( s = \frac{2}{T_c} \frac{1- z^{-1}}{1+ z^{-1}} \right) \]
\begin{framed}
\paragraph{Dimostrazione}
Sia $x \left( t \right)$ un generico segnale causale a tempo continuo, e sia $y \left( t \right)$ il suo integrale:
\[
y \left( t \right) = \int_0^t x \left( \theta \right) d \theta
\]

Per le proprietà della trasformata di Laplace (a condizioni iniziali nulle):
\[
Y \left( s \right) = \frac{X \left( s \right)}{s}
\]

\begin{figure}[H]
	\centering
	\includegraphics[scale=0.6]{pic/B09/Regola_dei_trapezi}
\end{figure}

Sia $x \left( n \right)$ il segnale campionato $x \left( n T_c \right)$. Il suo integrale $y \left( n \right)$ può essere ricavato tramite la regola dei trapezi:
\[
y \left( n \right) = y \left( n - 1 \right) + \frac{x \left( n \right) + x \left( n - 1 \right)}{2} T_c
\]

Confrontando la trasformata zeta $Y \left( z \right)$:
\[
Y \left( z \right) = z^{-1} Y \left( z \right) + \frac{X \left(  z \right) + z^{-1} X  \left( z \right)}{2} T_c ; \; Y \left( z \right) = X \left( z \right) \frac{T_c}{2} \frac{1+ z^{-1}}{1- z^{-1}}
\]
con l'espressione della trasformata di Laplace $Y \left( s \right)$:
\[
\begin{cases} \displaystyle s = \frac{2}{T_c} \frac{1- z^{-1}}{1+ z^{-1}} \\
\displaystyle z = \frac{1+s \frac{T_c}{2}}{1-s \frac{T_c}{2}} \end{cases}
\]
\end{framed}

\begin{figure}
	\centering
	\includegraphics[scale=0.6]{pic/B09/Relazione_tra_piano_s_e_piano_z}
\end{figure}

Esiste una corrispondenza biunivoca tra i punti del piano $s$ e i punti del piano $z$:
\begin{itemize}
\item l'asse immaginario $j \Omega$ nel piano complesso $s$ corrisponde alla circonferenza di raggio unitario nel piano complesso $z$;
\item le pulsazioni complesse $\sigma < 0$ sono mappate all'interno del cerchio di raggio unitario;
\item le pulsazioni complesse con $\sigma > 0$ sono mappate all'esterno del cerchio di raggio unitario.
\end{itemize}
\begin{framed}
\paragraph{Dimostrazione}
\[
\left| z \right| = \left| \frac{1+ \left( \sigma + j \Omega \right) \frac{T_c}{2}}{1- \left( \sigma + j \Omega \right) \frac{T_c}{2}} \right| = \sqrt{\frac{{\left( 1+ \sigma \frac{T_c}{2} \right)}^2 + {\left( \Omega \frac{T_c}{2} \right)}^2}{{\left( 1- \sigma \frac{T_c}{2} \right)}^2 + {\left( \Omega \frac{T_c}{2} \right)}^2}}
\]

Lungo l'asse immaginario ($\sigma = 0$):
\[
\left| z \right| = \sqrt{\frac{1+ {\left( \Omega \frac{T_c}{2} \right)}^2}{1+ {\left( \Omega \frac{T_c}{2} \right)}^2}} = 1
\]
\end{framed}
\FloatBarrier

Un filtro causale è stabile se:
\begin{itemize}
\item tutti i poli di $H_a \left( s \right)$ appartengono al semipiano sinistro nel piano complesso $s$;
\item tutti i poli di $H \left( z \right)$ appartengono al cerchio di raggio unitario nel piano complesso $z$.
\end{itemize}

La trasformazione bilineare impone la seguente relazione tra la risposta in frequenza analogica $H_a \left( j \Omega \right)$ e la risposta in frequenza numerica $H \left( e^{j \omega} \right)$:
\[
\begin{cases} \displaystyle H \left( e^{j 2 \pi f} \right) = H_a \left( j 2 \pi f_a = j \frac{2}{T_c} \tan{\left( \pi f \right)} \right) \\
\displaystyle H \left( e^{j \omega} \right) = H_a \left( j \Omega = j \frac{2}{T_c} \tan{\frac{\omega}{2}} \right) \end{cases}
\]
\begin{framed}
\paragraph{Dimostrazione}
\[
\begin{cases} \displaystyle H_a \left( j2 \pi f_a \right) = \left. H \left( s \right) \right\vert_{s=j 2 \pi f_a} \\
\displaystyle H \left( e^{j2 \pi f} \right) = \left. H \left( z \right) \right\vert_{z= e^{j 2 \pi f}} \\
\displaystyle s = \frac{2}{T_c} \frac{1 - z^{-1}}{1+ z^{-1}} \end{cases} \Rightarrow j2 \pi f_a = \frac{2}{T_c} \frac{1- e^{-j2 \pi f}}{1+ e^{-j2 \pi f}}
\]
\end{framed}

\begin{figure}
	\centering
	\includegraphics[scale=0.66]{pic/B09/Relazione_tra_frequenza_analogica_e_discreta}
	\caption{$\displaystyle f_a = \frac{1}{\pi T_c} \tan{\left( \pi f  \right)}$}
\end{figure}

I valori che la funzione $H_a \left( j 2 \pi f_a \right)$ assume sull'asse immaginario $f_a$ sono mappati sull'intervallo $\left[ - \tfrac{1}{2} T_c ; \, \tfrac{1}{2} T_c \right]$ mediante la relazione non lineare:
\[
\begin{cases} \displaystyle f = \frac{1}{\pi} \arctan{\left( \pi f_a T_c \right)} \\
\displaystyle \omega = 2 \arctan{\frac{\Omega T_c}{2}} \end{cases}
\]

Il filtro numerico ricavato è molto approssimato al filtro analogico per bassi valori di $f_a T_c$, mentre differisce tanto più ci si allontana dalla linearità. Se la frequenza analogica $f_a$ data è alta, è possibile aumentare la frequenza di campionamento $f_c$ per migliorare l'approssimazione del filtro numerico.

La trasformazione bilineare non introduce aliasing.
\FloatBarrier

\section{Trasformazioni spettrali tra diverse tipologie di filtro}
Tramite le seguenti trasformazioni spettrali è possibile ricavare, a partire da un filtro con pulsazione di taglio $\omega_t$, un filtro di diversa tipologia con pulsazione di taglio ${\hat{\omega}}_t$ e stessi parametri di progetto $\omega_p$, $\omega_s$, $A_S$ e $R_p$:
\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|}
  \hline
  \multirow{3}{*}{\textbf{Tipo di filtro desiderato}} & \textbf{Trasformazione spettrale} \\
  \cline{2-2}
  & $\displaystyle G \left( z^{-1} \right) = \pm \prod_{p=1}^n \frac{z^{-1} - \alpha_p}{1- \alpha_p z^{-1}} , \quad \left| \alpha_p \right| < 1$ \\
  \hline
  Passa-basso & $\displaystyle z^{-1} = \frac{{\hat{z}}^{-1} - \alpha}{1- \alpha {\hat{z}}^{-1}} , \quad \alpha = \frac{\displaystyle \sin{\frac{\omega_t - {\hat{\omega}}_t}{2}}}{\displaystyle \sin{\frac{\omega_t + {\hat{\omega}}_t}{2}}}$ \\
  \hline
  Passa-alto & $\displaystyle z^{-1} = - \frac{{\hat{z}}^{-1} + \alpha}{1+ \alpha {\hat{z}}^{-1}} , \quad \alpha = - \frac{\displaystyle \cos{\frac{\omega_t + {\hat{\omega}}_t}{2}}}{\displaystyle \cos{\frac{\omega_t - {\hat{\omega}}_t}{2}}}$ \\
  \hline
 \end{tabular}}
\end{table}

La trasformazione di un filtro causale e stabile deve portare a un filtro causale e stabile:
\begin{itemize}
\item l'interno del cerchio unitario si deve mappare in se stesso;
\item i poli all'interno del cerchio di raggio unitario devono rimanere all'interno.
\end{itemize}