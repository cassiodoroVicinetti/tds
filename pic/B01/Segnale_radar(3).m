pkg load signal
close all
#clear all
n=1000;
x=zeros(1,n);
x(1:100)=ones(1,100);
figure
set(gca,'FontSize',14)
stem(x, 'k')
xlabel('n')
title('x(n)')
axis([0 1000 -0.5 1.5])
grid on
D=200;
x_shift=[zeros(1,D) x(1:end-D)];
g=0.05*randn(1,n);
alpha=0.3;
r=alpha*x_shift+g;
z=xcorr(r,x);
figure
set(gca,'FontSize',14)
stem(r,'k')
xlabel('n')
title('r(n)')
axis([0 1000 -0.5 1.5])
grid on
figure
set(gca,'FontSize',14)
stem([-n+1:n-1],z,'k')
xlabel('n')
title('z(n)')
grid on