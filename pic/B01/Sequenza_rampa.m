n=[-10:10];
y=zeros(1,21);
y(11:21)=[0:10];
figure
set(gca,'FontSize',14)
stem(n,y,'k')
xlabel('n')
title('r(n)')
axis([-10 10 -0.5 10])
grid on