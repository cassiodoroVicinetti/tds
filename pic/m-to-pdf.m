#!/usr/bin/octave -qf
arg_list = argv ();
figureCount = str2num(arg_list{1});
inputFileName = arg_list{2};
outputFileName = arg_list{3};

graphics_toolkit('gnuplot'); # http://savannah.gnu.org/bugs/?42320

source(strcat(inputFileName, '.m'));

if (figureCount > 1)
  for i=1:figureCount
    print(i, strcat(outputFileName, '_', int2str(i), '.pdf'), '-dpdf');
  endfor

else
  print(strcat(outputFileName, '.pdf'), '-dpdf');
endif