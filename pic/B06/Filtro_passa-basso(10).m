N=11;
n=[0:N-1];
x=ones(1,N);
r1=0.8;
r2=0.5;
r3=0.2;
h1=r1.^n;
h2=r2.^n;
h3=r3.^n;
figure
set (gca,'FontSize',14)
stem(n,x)
xlabel('n')
ylabel('x(n)')
axis([0 10 0 2])
grid on
figure
set (gca,'FontSize',14)
stem(n,h1)
xlabel('n')
ylabel('h_1(n)')
axis([0 10 0 1])
grid on
figure
set (gca,'FontSize',14)
stem(n,h2)
xlabel('n')
ylabel('h_2(n)')
axis([0 10 0 1])
grid on
figure
set (gca,'FontSize',14)
stem(n,h3)
xlabel('n')
ylabel('h_3(n)')
axis([0 10 0 1])
grid on

N=11;
n=[0:N-1];
x=ones(1,N);
r1=0.8;
r2=0.5;
r3=0.2;
k1=(1-r1)/(1-r1^N);
k2=(1-r2)/(1-r2^N);
k3=(1-r3)/(1-r3^N);
h1=k1*r1.^n;
h2=k2*r2.^n;
h3=k3*r3.^n;
%  figure
%  set (gca,'FontSize',14)
%  stem(n,x)
%  xlabel('n')
%  ylabel('x(n)')
%  axis([0 10 0 2])
%  grid on
%  figure
%  set (gca,'FontSize',14)
%  stem(n,h1)
%  xlabel('n')
%  ylabel('h_1(n)')
%  axis([0 10 0 1])
%  grid on
%  figure
%  set (gca,'FontSize',14)
%  stem(n,h2)
%  xlabel('n')
%  ylabel('h_2(n)')
%  axis([0 10 0 1])
%  grid on
%  figure
%  set (gca,'FontSize',14)
%  stem(n,h3)
%  xlabel('n')
%  ylabel('h_3(n)')
%  axis([0 10 0 1])
%  grid on

f=linspace(-0.5,0.5,1000);
H1=k1*(1-(r1*exp(-j*2*pi*f)).^N)./(1-r1*exp(-j*2*pi*f));
H2=k2*(1-(r2*exp(-j*2*pi*f)).^N)./(1-r2*exp(-j*2*pi*f));
H3=k3*(1-(r3*exp(-j*2*pi*f)).^N)./(1-r3*exp(-j*2*pi*f));
figure
set (gca,'FontSize',14)
plot(f,abs(H1))
hold on
plot(f,abs(H2),'r')
plot(f,abs(H3),'g')
xlabel('f')
ylabel('|H_i(e^{j\omega})|')
axis([-0.5 0.5 0 1.5])
grid on
legend('|H_1(e^{j\omega})|','|H_2(e^{j\omega})|', '|H_3(e^{j\omega})|')

N = 11;
X = (sin(pi*f*N) ./ sin(pi*f)) .* exp(-j*pi*f*(N-1));
figure
set (gca,'FontSize',14)
plot(f,abs(X))
xlabel('f')
ylabel('|X(e^{j\omega})|')
axis([-0.5 0.5 0 11])
grid on

Y1 = X .* H1;
Y2 = X .* H2;
Y3 = X .* H3;
figure
set (gca,'FontSize',14)
plot(f,abs(Y1))
hold on
plot(f,abs(Y2), 'r')
plot(f,abs(Y3), 'g')
xlabel('f')
ylabel('|Y_i(e^{j\omega})|')
axis([-0.5 0.5 0 11])
grid on
legend('|Y_1(e^{j\omega})|','|Y_2(e^{j\omega})|', '|Y_3(e^{j\omega})|')

N=11;
n=[0:N-1];
x=ones(1,N);
r1=0.8;
r2=0.5;
r3=0.2;
k1=(1-r1)/(1-r1^N);
k2=(1-r2)/(1-r2^N);
k3=(1-r3)/(1-r3^N);
h1=k1*r1.^n;
h2=k2*r2.^n;
h3=k3*r3.^n;
y1=conv(x,h1);
y2=conv(x,h2);
y3=conv(x,h3);
n=[0:length(y1)-1];
figure
set (gca,'FontSize',14)
stem(n,y1)
xlabel('n')
ylabel('y_1(n)')
axis([0 20 0 1.5])
grid on
figure
set (gca,'FontSize',14)
stem(n,y2)
xlabel('n')
ylabel('y_2(n)')
axis([0 20 0 1.5])
grid on
figure
set (gca,'FontSize',14)
stem(n,y3)
xlabel('n')
ylabel('y_3(n)')
axis([0 20 0 1.5])
grid on