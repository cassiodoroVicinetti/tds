#!/bin/bash

# Debian Stretch: octave epstool octave-signal

my_convert() {
  figureCount=1
  outputFileName=$1
  
  if [ ${1:(-1)} == ")" ]
  then
    countWidth=1
    bracketPosition=-3
    while [ ${1:($bracketPosition):(1)} != '(' ] # http://stackoverflow.com/a/19858692
    do
      countWidth=$(($countWidth + 1))
      bracketPosition=$(($bracketPosition - 1))
    done
    figureCount=${1:($(($bracketPosition + 1))):($countWidth)}
    outputFileName=${1:(0):($((${#1} - 2 - $countWidth)))} # http://stackoverflow.com/a/15596250
  fi
  
  ../m-to-pdf.m $figureCount "$1" "$outputFileName"
  
  if [ $figureCount -eq 1 ]
  then
    pdfcrop "$outputFileName.pdf" "$outputFileName.pdf"
  else
    i=1
    while [ $i -le $figureCount ]
    do
      pdfcrop "$outputFileName""_""$i.pdf" "$outputFileName""_""$i.pdf"
      i=$(($i + 1))
    done
  fi
}

SAVEIFS=$IFS # http://www.cyberciti.biz/tips/handling-filenames-with-spaces-in-bash.html
IFS=$(echo -en "\n\b")

for dir in ./*/
do
  cd "$dir"
  for m in $(find -maxdepth 1 -type f -name "*.m")
  do
    my_convert "$(basename "$m" ".m")"
  done
  cd ../
done

IFS=$SAVEIFS