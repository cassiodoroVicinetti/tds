\chapter{Sistemi LTI a tempo discreto}
\section{Classificazione dei sistemi a tempo discreto}
Un \textbf{sistema} per i segnali a tempo discreto è definito tramite la sua relazione ingresso-uscita:
\[
y \left( n \right) = L \left[ x \left( n \right) \right]
\]

\subsection{Sistemi lineari}
Un sistema è \textbf{lineare} se soddisfa il principio di sovrapposizione degli effetti:
\[
L \left[ \alpha_1 x_1 \left( n \right) + \alpha_2 x_2 \left(   n \right) \right] = \alpha_1 L \left[ x_1 \left( n \right) \right] + \alpha_2 L \left[ x_2 \left( n \right) \right]
\]

\subsection{Sistemi tempo-invarianti o stazionari}
Un sistema è \textbf{tempo-invariante} (o \textbf{stazionario}) se un ritardo/anticipo $n_0$ sull'ingresso $x \left( n \right)$ si traduce in un ritardo/anticipo uguale sull'uscita $y \left( n \right)$ senza che cambi la forma del segnale di uscita $y \left( n \right)$:
\[
L \left[ x \left( n - n_0 \right) \right] = y \left( n - n_0 \right) \quad \forall n_0
\]

\subsection{Sistemi passivi}
Un sistema è \textbf{passivo} se a un ingresso $x \left( n \right)$ con energia finita $E_x$ corrisponde un segnale $y \left( n \right)$ con energia $E_y$ minore o uguale all'energia dell'ingresso:
\[
E_y = \sum_{n = - \infty}^{+ \infty} {\left| y \left( n \right) \right|}^2 \leq E_x = \sum_{n = - \infty}^{+ \infty} {\left| x \left( n \right) \right|}^2 \in \mathbb{R}
\]

Il sistema è \textbf{senza perdite} se la relazione vale con il segno di uguaglianza: tutta l'energia dell'ingresso $x \left( n \right)$ viene conservata:
\[
E_y = E_x
\]

\subsection{Sistemi causali}
Un sistema è \textbf{causale} se l'uscita $y \left( n \right)$ non dipende dai valori futuri dell'ingresso $x \left( n \right)$, ma solo da quelli passati e da quello corrente.

Il comportamento di sistemi LTI causali a tempo discreto può essere descritto da un'\textbf{equazione alle differenze} finite e coefficienti costanti, che esprime l'uscita $y \left( n \right)$ all'istante corrente come combinazione lineare degli ingressi agli istanti passati e a quello corrente e delle uscite agli istanti passati (di solito $a_0  = 1$):
\[
a_0   y \left( n \right) = - \sum_{k = 1}^{M} a_k y \left( n - k \right) + \sum_{k=0}^N b_k x \left( n - k \right) =
\]
\[
=- a_1 y \left( n - 1 \right) - a_2 y \left( n - 2 \right) - \ldots - a_M y \left( n - M \right) +
\]
\[
+b_0 x \left(  n \right) + b_1 x \left( n - 1 \right)  + b_2 x \left( n  - 2 \right) + \ldots + b_N x \left( n - N \right)
\]

\begin{itemize}
\item Il sistema è \textbf{ricorsivo} se l'uscita $y \left( n \right)$ dipende da almeno un valore dell'uscita in istanti precedenti.
\item Il sistema è \textbf{non ricorsivo} se tutti i coefficienti $a_i$ sono nulli.
\end{itemize}

L'equazione alle differenze di un sistema causale permette di trovare i valori di $y \left( n \right)$ per $n \geq 0$, noti i valori di $x \left( n \right)$ e le condizioni iniziali $y \left( - 1 \right) , \ldots , y \left( - M \right)$:
\[
y \left( n \right) = - \sum_{k = 1}^M a_k y \left( n - k \right) + \sum_{k = 0}^N b_k x \left( n  - k \right) = y_{\text{so}} \left( n \right) + y_{\text{io}} \left( n \right)
\]

\subsubsection{Risposta forzata}
\label{sez:risposta_forzata}
$y_{\text{so}} \left( n \right)$ è detta \textbf{risposta allo stato nullo}, e rappresenta l'evoluzione del sistema con condizioni iniziali nulle, tenendo conto solo degli ingressi:
\[
y_{\text{so}} \left( n \right) = - \sum_{k = 0}^M a_k y_{\text{so}} \left( n - k \right) + \sum_{k = 0}^{N} b_k x \left( n -k \right)
\]

Trascurare le condizioni iniziali significa studiare il comportamento del sistema a regime (sistema scarico).

\subsubsection{Risposta libera}
$y_{\text{io}}$ è detta \textbf{risposta all'ingresso nullo}, e rappresenta l'evoluzione del sistema con ingresso nullo, ma tenendo conto delle condizioni iniziali:
\[
\begin{cases} y_{\text{io}} \left( n \right) = - \sum_{k = 0}^M a_k y \left( n-k \right) \\
y_{\text{io}} \left( k \right) = y \left( k \right) \quad k = - M , \ldots , -1 \end{cases}
\]

Per sistemi LTI stabili, $y_{\text{io}} \left( n \right)$ è anche detta \textbf{risposta al transitorio} perché siccome l'ingresso è nullo tende a smorzarsi nel tempo fino ad annullarsi.

\subsubsection{Sistemi senza memoria}
Un sistema è \textbf{senza memoria} se l'uscita $y \left( n \right)$ dipende solo dal valore corrente dell'ingresso $x \left( n \right)$.

Il sistema non ricorsivo:
\[
y \left( n \right) = \sum_{k=0}^N a_k x \left( n - k \right)
\]
ha memoria pari a $N$, perché l'uscita $y \left( n \right)$ dipende anche da $N$ valori passati dell'ingresso $x \left( n \right)$.

\section{Sistemi LTI}
\subsection{Risposta all'impulso}
I sistemi LTI causali scarichi sono caratterizzati da una \textbf{risposta all'impulso} $h \left( n \right)$, che è la risposta del sistema quando in ingresso è presente la sequenza $x \left( n \right) = \delta \left( n \right)$:
\[
h \left( n \right)  = L \left[ \delta \left( n \right) \right]
\]

La risposta all'impulso $h \left( n \right)$ lega l'ingresso $x \left( n \right)$ e l'uscita $y \left( n \right)$ del sistema:
\[
y \left( n  \right) = x \left( n \right) * h \left( n \right) = \sum_{i = - \infty}^{+ \infty} h \left( i  \right) x \left( n - i \right)
\]
\begin{framed}
\paragraph{Dimostrazione}
\[ y \left( n \right) = L \left[ x \left( n \right) \right] = L \left[ \sum_{i = - \infty}^{+ \infty} x \left( i \right) \delta \left( n - i \right) \right] = \]

Sfruttando il fatto che il sistema è lineare:
\[ = \sum_{i = - \infty}^{+ \infty} x \left( i \right) L \left[  \delta \left( n - i \right) \right] = \]

Sfruttando il fatto che il sistema è tempo-invariante:
\[ = \sum_{i = - \infty}^{+ \infty} x \left( i \right) h \left( n - i \right) \]
\end{framed}

Tutti i sistemi LTI possono essere quindi espressi in forma non ricorsiva, dove $b_k = h \left( k \right)$.

L'uscita del sistema dipende dai contributi causale ($i \geq 0$) e anticausale ($i < 0$):
\[
y \left( n \right) = \sum_{i = - \infty}^{+ \infty} h \left( i  \right) x \left( n - i \right) = \sum_{i = - \infty}^{-1} h \left( i  \right) x \left( n - i \right) + \sum_{i = 0}^{+ \infty} h \left( i  \right) x \left( n - i \right)
\]

Siccome il sistema è causale, la parte anticausale è nulla:
\[
y \left( n \right) = \sum_{i = 0}^{+ \infty} h \left( i \right) x \left( n - i \right)
\]

\subsection{Risposta in frequenza}
\paragraph{Ipotesi}
\begin{itemize}
\item $x \left( n \right)$ e $h \left( n \right)$ sono trasformabili mediante DTFT;
\item si trascura la risposta al transitorio $y_{\text{so}} \left( n \right)$.
\end{itemize}

La DTFT del segnale in uscita $y \left( n \right)$ è pari al prodotto delle DTFT del segnale in ingresso $x \left( n \right)$ e della risposta all'impulso $h \left( n \right)$ del sistema LTI:
\[
Y \left( e^{j \omega} \right) = H \left( e^{j \omega} \right) \cdot X \left( e^{j \omega} \right)
\]
\begin{framed}
\paragraph{Dimostrazione}
\[ Y \left( e^{j \omega} \right) = \sum_{n = - \infty}^{+ \infty} y \left( n \right) e^{-j \omega n} = \sum_{n = - \infty}^{+ \infty} \left[ x \left( n \right) * h \left( n \right) \right] e^{-j \omega n} = \]
\[ = \sum_{n = - \infty}^{+ \infty} \left[ \sum_{i = - \infty}^{+ \infty} x \left( i \right) h \left( n - i \right) \right] e^{-j \omega n} = \sum_{i = - \infty}^{+ \infty} h \left( i \right) \left[ \sum_{n = - \infty}^{+ \infty} x \left( n - i \right) e^{-j \omega n} \right] = \]

Effettuando il cambio di variabile $m = n - i$ nella sommatoria interna:
\[ = \sum_{i = - \infty}^{+ \infty} h \left( i \right) \left[ \sum_{m = - \infty}^{+ \infty} x \left( m \right) e^{-j \omega \left( m + i \right)} \right] = \left[ \sum_{i = - \infty}^{+ \infty} h \left( i \right) e^{-j \omega i} \right] \left[ \sum_{m = - \infty}^{+ \infty} x \left( m \right) e^{-j \omega n} \right] = \]
\[ = H \left( e^{j \omega} \right) \cdot X \left( e^{j \omega} \right) \]
\end{framed}

La DTFT $H \left( e^{j \omega} \right)$ della risposta all'impulso $h \left( n \right)$ è detta \textbf{risposta in frequenza} del sistema LTI:
\[
H \left( e^{j \omega} \right) = \sum_{i = - \infty}^{+ \infty} h \left( i \right) e^{-j \omega i} = \frac{Y \left( e^{j \omega} \right)}{X \left( e^{j \omega} \right)}
\]

\begin{table}[H]
 \centering
 \centerline{\begin{tabular}{|c|c|c|}
  \hline
  \textbf{Serie} & \begin{tabular}[c]{@{}c@{}}\includegraphics[scale=0.5]{pic/B06/Diagramma_serie_n.png}\end{tabular} & \begin{tabular}[c]{@{}c@{}}$ \displaystyle y \left( n \right) = x \left( n \right) * h_1 \left( n \right) * h_2 \left( n \right)$\\
  $\displaystyle Y \left( e^{j \omega} \right) = X \left( e^{j \omega} \right) H_1 \left( e^{j \omega} \right) H_2 \left( e^{j \omega} \right)$\end{tabular} \\
  \hline
  \textbf{Parallelo} & \begin{tabular}[c]{@{}c@{}}\includegraphics[scale=0.45]{pic/B06/Diagramma_parallelo_n.png}\end{tabular} & \begin{tabular}[c]{@{}c@{}}$ \displaystyle y \left( n \right) = x \left( n \right) * \left[ h_1 \left( n \right) + h_2 \left( n \right) \right]$\\
  $\displaystyle Y \left( e^{j \omega} \right) = X \left( e^{j \omega} \right) \left[ H_1 \left( e^{j \omega} \right) + H_2 \left( e^{j \omega} \right) \right]$\end{tabular} \\
  \hline
  \begin{tabular}[c]{@{}c@{}}\textbf{Con}\\\textbf{reazione}\end{tabular} & \begin{tabular}[c]{@{}c@{}}\includegraphics[scale=0.45]{pic/B06/Diagramma_retroazione_n.png}\end{tabular} & \begin{tabular}[c]{@{}c@{}}$ \displaystyle y \left( n \right) = \left[ x \left( n \right) - y \left( n \right) * h_2 \left( n \right) \right] * h_1 \left( n \right)$\\
  $\displaystyle Y \left( e^{j \omega} \right) = X \left( e^{j \omega} \right) \frac{H_1 \left( e^{j \omega} \right)}{1 + H_1 \left( e^{j \omega} \right) H_2 \left( e^{j \omega} \right)}$\end{tabular} \\
  \hline
 \end{tabular}}
 \caption{Interconnessione di sistemi LTI.}
\end{table}

\subsubsection{Risposta a esponenziali complessi}
Gli esponenziali complessi sono delle autofunzioni dei sistemi LTI, perché a un ingresso a esponenziale complesso corrisponde ancora un'uscita a esponenziale complesso:
\[
x \left( n \right)  = e^{j \omega_0 n} \Rightarrow y \left( n \right) = e^{j \omega_0 n} H \left( e^{j \omega_0} \right) = \left| H \left( e^{j \omega0} \right) \right| \cdot e^{j  \omega_0 n + j \arg{\left[ H \left( e^{j \omega_0} \right) \right]}  }
\]
\begin{framed}
\paragraph{Dimostrazione}
\[
y \left( n \right) =  x \left( n \right) * h \left( n \right) = \sum_{i = - \infty}^{+ \infty} h \left( i \right) e^{j \omega_0 \left( n - i \right)} = e^{j \omega_0 n} \sum_{i = - \infty}^{+ \infty} h \left( i \right) e^{-j \omega_0 i}
\]
\end{framed}

Se la risposta all'impulso $h \left( n \right)$ è reale, e la sinusoide $x \left( n \right)$ è reale:
\[
x \left( n \right) = \cos{\left( \omega_0 n + \theta \right) } \Rightarrow y \left( n \right) = \left| H \left( e^{j \omega_0} \right) \right| \cos{\left( \omega_0 n  + \theta + \arg{ \left[ H \left( e^{j \omega_0} \right) \right] } \right)}
\]
\begin{framed}
\paragraph{Dimostrazione}
\[
x \left( n \right) = \cos{\left( \omega_0 n + \theta \right) } = \frac{e^{j \omega_0 n + j \theta } + e^{-j \omega_0 n - j \theta} }{2}
\]

Applicando il risultato precedente a ciascuno dei due esponenziali complessi:
\[
y \left( n \right) = \frac{1}{2} \left\{ e^{j \theta} \left| H \left( e^{j \omega_0} \right) \right| \cdot e^{j \omega_0 n + j \arg{\left[ H \left( e^{j \omega_0} \right) \right]}} + e^{- j \theta} \left| H \left( e^{- j \omega_0} \right) \right| \cdot e^{-j \omega_0 n + j \arg{\left[ H \left( e^{-j \omega_0} \right) \right] }}  \right\} =
\]

Ricordando le proprietà di simmetria della DTFT della sequenza reale $h \left( n \right)$ (modulo pari e fase dispari):
\[
= \frac{1}{2} \left| H \left( e^{j \omega_0} \right) \right| \left\{ e^{j \theta}  e^{j \omega_0 n + j \arg{\left[ H \left( e^{j \omega_0} \right) \right]}} + e^{-j \theta} e^{-j \omega_0 n - j \arg{\left[ H \left( e^{j \omega_0} \right) \right]}} \right\}
\]
\end{framed}

\subsubsection{Sistemi IIR}
L'uscita $y \left( n \right)$ di un \textbf{sistema IIR} (Infinite Impulse Response) dipende non solo dal segnale in ingresso $x \left( n \right)$, ma anche dai campioni del segnale in uscita $y \left( n \right)$:
\[
y \left( n \right) = - \sum_{k = 1}^M a_k y \left( n - k \right) + \sum_{k = 0}^N b_k x \left( n - k \right) \Rightarrow Y \left( e^{j \omega} \right) = X \left( e^{j \omega} \right) \frac{\sum_{k = 0}^{N} b_k e^{-j \omega k} }{1 + \sum_{k = 1}^M a_k e^{-j \omega k}}
\]

La risposta in frequenza $H \left( e^{j \omega} \right)$ del sistema si può scrivere:
\[
H \left( e^{j \omega} \right) = \frac{Y \left( e^{j \omega} \right)}{X \left( e^{j \omega} \right)} = \frac{\sum_{k = 0}^{N} b_k e^{-j \omega k} }{1 + \sum_{k = 1}^M a_k e^{-j \omega k}}
\]

\paragraph{Sistemi IIR puramente ricorsivi}
Un sistema IIR è \textbf{puramente ricorsivo} se:
\[
y \left( n \right) = x \left( n \right) - \sum_{k = 1}^M a_k y \left( n - k \right) \quad b_k = 0 \; \forall k \neq 0
\]

La risposta all'impulso $h \left( n \right)$ di un sistema IIR puramente ricorsivo è a supporto illimitato.

\paragraph{Esempio: risposta all'impulso di un sistema IIR puramente ricorsivo}
\[
y \left( n \right) = \frac{1}{3} y \left( n -1 \right) + x \left( n \right) \Rightarrow h \left( n \right) = {\left( \frac{1}{3} \right)}^n u \left( n \right)
\]
\begin{framed}
\paragraph{Calcoli}
\[ Y \left( e^{j \omega} \right) = \frac{1}{3} Y \left( e^{j \omega} \right) e^{- j \omega} + X \left( e^{j \omega} \right) \]
\[ H \left( e^{j \omega} \right) = \frac{Y \left( e^{j \omega} \right)}{X \left( e^{j \omega} \right)} = \frac{1}{1 - \frac{1}{3} e^{-j \omega}} \]
\end{framed}

\subsubsection{Sistemi FIR}
L'uscita $y \left( n \right)$ di un \textbf{sistema FIR} (Finite Impulse Response) dipende solo dal segnale d'ingresso $x \left( n \right)$:
\[
- \sum_{k = 1}^M a_k y \left( n - k \right) = 0 \Rightarrow y \left( n \right) = \sum_{k = 0}^N b_k x \left( n - k \right)
\]
\[ H \left( e^{j \omega} \right) = \sum_{k = 0}^{N} b_k e^{-j \omega k} \]

La risposta all'impulso $h \left( n \right)$ di un sistema FIR è a supporto finito $\left[ 0 , N \right]$:
\[
h \left( n \right) \triangleq \sum_{k = 0}^N b_k \delta \left( n - k \right) = b_0 \delta \left( n \right) + b_1 \delta \left( n - 1 \right) + \ldots + b_N \delta \left( n - N \right)
\]

\subsection{Stabilità}
\label{sez:stabilità_BIBO}
Un sistema è \textbf{stabile secondo il criterio BIBO} (Bounded-Input Bounded-Output) (o BIBO-stabile) se e solo se ad ogni ingresso $x \left( n \right)$ di ampiezza limitata corrisponde un'uscita $y \left( n \right)$ di ampiezza limitata:
\[
\left| x \left( n \right) \right| \in \mathbb{R} \Leftrightarrow \left| y \left( n \right) \right| \in \mathbb{R}
\]

La risposta al transitorio di un sistema BIBO-stabile tende a smorzarsi al crescere di $n$.

\paragraph{Teorema}
Un sistema LTI è BIBO-stabile se e solo se la sua risposta all'impulso $h \left( n \right)$ è sommabile in modulo:
\[
\text{stabile } \Leftrightarrow \sum_{n = - \infty}^{+ \infty} \left| h \left( n \right) \right| \in \mathbb{R}
\]
\begin{framed}
\paragraph{Dimostrazione condizione sufficiente}
\[ \sum_{n = - \infty}^{+ \infty} \left| h \left( n \right) \right| \in \mathbb{R} \Rightarrow \text{ stabile} \]

Considerando un ingresso $x \left( n \right)$ di ampiezza limitata, il valore dell'uscita $y \left( n \right)$ è:
\[
\left| y \left( n \right)  \right| = \left| h \left( n \right) * x \left( n \right) \right| = \left| \sum_{k = - \infty}^{+ \infty} h \left( k \right) x \left( n - k \right) \right| \leq \sum_{k = - \infty}^{+ \infty} \left| h \left( k \right) \right| \left| x \left( n - k \right) \right| \in \mathbb{R}
\]
\end{framed}
\begin{framed}
\paragraph{Dimostrazione condizione necessaria}
\[
\text{stabile } \Rightarrow \sum_{n = - \infty}^{+ \infty} \left| h \left( n \right) \right| \in \mathbb{R}
\]

Considerando l'ingresso $x \left( n \right)$ di ampiezza limitata:
\[
x \left( n \right) = \begin{cases} \displaystyle \frac{h^* \left( - n \right) }{\left| h \left( - n \right) \right|} & h \left( - n \right) \neq 0 \\ \displaystyle 0 & h \left( - n \right) = 0 \end{cases} \Rightarrow \left| x \left( n \right) \right| = \begin{cases} \displaystyle \frac{\left| h^* \left( - n \right) \right|}{\left| h \left( - n \right) \right|} = 1 & h \left( - n \right) \neq 0 \\ \displaystyle 0 & h \left( - n \right) = 0 \end{cases}
\]
il valore dell'uscita $y \left( n \right)$ in $n = 0$ è:
\[
\begin{cases} \displaystyle y \left( 0 \right) = \sum_{k = - \infty}^{+ \infty} h \left( k \right) x \left(  - k \right) = \sum_{k = - \infty}^{+ \infty } h \left( k \right) \frac{h^* \left( k \right)}{\left| h \left(  k \right) \right|} = \sum_{k = - \infty}^{+ \infty} \frac{{\left| h \left( k \right) \right|}^2}{\left| h \left( k \right) \right|} = \sum_{k = - \infty}^{+ \infty} \left| h \left( k \right) \right| \\
\text{stabile } \Rightarrow y \left( n \right) \in \mathbb{R} \end{cases} \Rightarrow
\]
\[
\Rightarrow \sum_{k = - \infty}^{+ \infty} \left| h \left( k \right) \right| \in \mathbb{R}
\]
\end{framed}

\subsection{Realizzabilità fisica}
\label{sez:b06_realizzabilita_fisica}
Un sistema LTI è \textbf{fisicamente realizzabile} se l'equazione alle differenze è causale e i coefficienti $a_i$ e $b_i$ della sua equazione alle differenze sono tutti reali:
\[
y \left( n \right) = \sum_{k=1}^M a_k y \left(  n - k \right) + \sum_{k=0}^N b_k x \left( n - k \right) \quad \forall k : \, a_k , b_k \in \mathbb{R}
\]

Un sistema è fisicamente realizzabile se la sua risposta all'impulso $h \left( n \right)$ è reale e causale:
\[
\begin{cases} h \left( n \right) \in \mathbb{R} & \forall n \geq 0 \\ h \left( n \right) = 0 & \forall n < 0 \end{cases}
\]

La risposta in frequenza $H \left( e^{j \omega} \right)$ di un sistema fisicamente realizzabile è unilatera:
\[
H^+ \left( e^{j \omega} \right) = \sum_{n=0}^{+ \infty} h \left( n \right) e^{-j \omega n}
\]
ed ha le seguenti relazioni di parità:
\begin{itemize}
\item la parte reale e il modulo sono pari;
\item la parte immaginaria e la fase sono dispari.
\end{itemize}

\subsection{Esempio: filtro passa-basso}
\afterpage{
\begin{landscape}
\begin{figure}[t]
\centering
	\begin{subfigure}[b]{.35\linewidth}
		\centering
		\includegraphics[scale=0.409]{pic/B06/Filtro_passa-basso_2}
		\caption{$h_1 \left( n \right)$}
	\end{subfigure}%
	\begin{subfigure}[b]{.35\linewidth}
		\centering
		\includegraphics[scale=0.409]{pic/B06/Filtro_passa-basso_3}
		\caption{$h_2 \left( n \right)$}
	\end{subfigure}%
	\begin{subfigure}[b]{.35\linewidth}
		\centering
		\includegraphics[scale=0.409]{pic/B06/Filtro_passa-basso_4}
		\caption{$h_3 \left( n \right)$}
	\end{subfigure}
\end{figure}
\begin{figure}[b]
\centering
	\begin{subfigure}[b]{.35\linewidth}
		\centering
		\includegraphics[scale=0.409]{pic/B06/Filtro_passa-basso_5}
		\caption{$\displaystyle \left| H_i \left( e^{j \omega} \right) \right|$}
	\end{subfigure}%
	\begin{subfigure}[b]{.35\linewidth}
		\centering
		\includegraphics[scale=0.409]{pic/B06/Filtro_passa-basso_1}
		\caption{$x \left( n \right)$}
	\end{subfigure}%
	\begin{subfigure}[b]{.35\linewidth}
		\centering
		\includegraphics[scale=0.409]{pic/B06/Filtro_passa-basso_6}
		\caption{$\displaystyle \left| X \left( e^{j \omega} \right) \right|$}
	\end{subfigure}
\end{figure}
\end{landscape}
}
\afterpage{
\begin{landscape}
\begin{figure}[t]
\centering
		\includegraphics[scale=0.409]{pic/B06/Filtro_passa-basso_7}
		\caption{$\displaystyle \left| Y_i \left( e^{j \omega} \right) \right|$}
\end{figure}
\begin{figure}[b]
\centering
	\begin{subfigure}[b]{.35\linewidth}
		\centering
		\includegraphics[scale=0.409]{pic/B06/Filtro_passa-basso_8}
		\caption{$y_1 \left( n \right)$}
	\end{subfigure}%
	\begin{subfigure}[b]{.35\linewidth}
		\centering
		\includegraphics[scale=0.409]{pic/B06/Filtro_passa-basso_9}
		\caption{$y_2 \left( n \right)$}
	\end{subfigure}%
	\begin{subfigure}[b]{.35\linewidth}
		\centering
		\includegraphics[scale=0.409]{pic/B06/Filtro_passa-basso_10}
		\caption{$y_3 \left( n \right)$}
	\end{subfigure}
\end{figure}
\end{landscape}
}

Si vogliono confrontare tre sistemi LTI che hanno risposte all'impulso $h_1 \left( n \right)$, $h_2 \left( n \right)$ e $h_3 \left( n \right)$:
\[
h_i \left( n \right) = r_i^n \quad n = 0 , \ldots , N - 1
\]
dove:
\[
\begin{cases} r_1 = 0,8 \\ r_2 = 0,5 \\ r_3 = 0,2 \end{cases}
\]

La risposta in frequenza $H_i \left( e^{j \omega} \right)$ è pari a:
\[
H_i \left( e^{j \omega} \right) = \sum_{n = - \infty}^{+ \infty} h_i \left( n \right) e^{-j \omega n} = \sum_{n = 0}^{N-1} r_i^n e^{-j \omega n} = \frac{1- {\left( r_i e^{-j \omega } \right)}^N}{1- r_i e^{-j \omega}}
\]

Si introduce come ingresso $x \left( n \right)$ una porta:
\[
x \left( n \right) = u \left( n \right) - u \left( n - N \right) \Rightarrow X \left( e^{j \omega} \right) = \sum_{n = - \infty}^{+ \infty} x \left( n \right) e^{- j \omega n} = \frac{\sin{\left( \pi f N \right)}}{\sin{ \left( \pi f \right) }} e^{-j \pi f \left( N - 1 \right)}
\]

Il filtro passa-basso smorza le alte frequenze. L'uscita $y_i \left( n \right)$ appare più simile all'ingresso $x \left( n \right)$ con il filtro $h_3 \left( n \right)$.

L'ingresso $x \left( n \right)$ risulta quindi meno smorzato con il filtro $h_3 \left( n \right)$:
\[
E \left( x \right) = \sum_{n = - \infty}^{+ \infty} x^2 \left( n \right) = \sum_{n = 0}^{N-1} 1 = N = 11
\]
\[
E \left( y_1 \right) = \sum_{n = - \infty}^{+ \infty} y_1^2 \left( n \right) \cong 8
\]
\[ E \left( y_2 \right) = \sum_{n = - \infty}^{+ \infty} y_2^2 \left( n \right) \cong 9,7 \]
\[ E \left( y_3 \right) = \sum_{n = - \infty}^{+ \infty} y_3^2 \left( n \right) \cong 10,6 \]

Questo sistema LTI è di tipo passivo perché l'energia dell'uscita $y_i \left( n \right)$ è sempre minore dell'energia dell'ingresso $x \left( n \right)$.

\subsection{Distorsione}
\begin{itemize}
\item Se il modulo della risposta in frequenza $\left| H \left( e^{j \omega} \right) \right|$ non è costante, il sistema introduce una \textbf{distorsione in ampiezza}.
\item Se la fase della risposta in frequenza $\arg{\left[ H \left( e^{j \omega} \right) \right]}$ non è lineare in $\omega = 2 \pi f$, il sistema introduce una \textbf{distorsione di fase}.
\end{itemize}

Si definisce \textbf{ritardo di gruppo} $\tau \left( \omega \right)$ il ritardo medio subito dalle componenti armoniche del segnale in ingresso $x \left( n \right)$:
\[
\tau \left( \omega \right) =  - \frac{d}{d \omega} \arg{\left[ H \left( e^{j \omega} \right) \right]}
\]

\subsubsection{Sistema LTI non distorcente}
Si vuole realizzare un amplificatore non distorcente:
\[
y \left( n \right) = A \cdot x \left( n - N \right)
\]
dove:
\begin{itemize}
\item $N$ è il tempo impiegato dal segnale in ingresso $x \left( n \right)$ per attraversare il sistema (ritardo);
\item $A$ è l'amplificazione.
\end{itemize}

La sua risposta in frequenza $H \left( e^{j \omega} \right)$:
\[
H \left( e^{j \omega} \right) = \frac{Y \left( e^{j \omega} \right)}{X \left( e^{j \omega} \right)} = A e^{- j \omega N}
\]
ha:
\begin{itemize}
\item modulo $A$ costante;
\item rotazione di fase $- \omega N$ lineare in $\omega$.
\end{itemize}

La sua risposta all'impulso $h \left( n \right)$:
\[
h \left( n \right) = A \delta \left( n - N \right)
\]
è una delta di ampiezza $A$ centrata in $N$.

Se il ritardo di gruppo $\tau \left( \omega \right)$ è costante, cioè le componenti armoniche nel segnale di ingresso $x \left( n \right)$ subiscono lo stesso ritardo costante, allora la fase è lineare e il sistema è non distorcente.

\subsection{Analisi tramite DFT}
L'uscita $y \left( n \right)$ di un sistema LTI si può ottenere dalla IDTFT del prodotto tra la DTFT dell'ingresso $x \left( n \right)$ e la risposta in frequenza $H \left( e^{j \omega} \right)$:
\[
Y \left( e^{j \omega} \right) = X \left( e^{j \omega} \right) \cdot H \left( e^{j \omega} \right) \Rightarrow y \left( n \right) = x \left( n \right) * h \left( n \right) \quad n \in \left[ 0 , N_x + N_h - 2 \right]
\]
dove $N_x$ e $N_h$ sono i supporti rispettivamente di $x \left( n \right)$ e $h \left( n \right)$.

Se si vuole sostituire la DTFT con la DFT, la IDFT non restituisce lo stesso risultato:
\[
Y \left( k \right) = X \left( k \right) \cdot H \left( k \right) \Rightarrow y \left( n \right) \neq x \left( n \right) \otimes h \left( n \right) \quad n \in \left[ 0 , \max{\left\{ N_x , N_h \right\}} -1 \right]
\]
perché la convoluzione circolare ha un supporto diverso dalla convoluzione lineare.

Per far sì che il risultato coincida con la convoluzione lineare, occorre procedere all'inserimento di zeri (zero padding) nelle due sequenze $x \left( n \right)$ e $h \left( n \right)$, in numero tale da garantire la lunghezza di una convoluzione lineare:
\[
x_z \left( n \right) = \begin{cases} x \left( n \right) & n \in \left[ 0 , N_x - 1 \right] \\
0 & n \in \left[ N_x , N_x + N_h - 2 \right] \end{cases}
\]
\[
h_z \left( n \right) = \begin{cases} h \left( n \right) & n \in \left[ 0 , N_h - 1 \right] \\
0 & n \in \left[ N_h , N_x + N_h - 2 \right] \end{cases}
\]

In questo modo la convoluzione circolare ottenuta dalla IDFT coincide con la convoluzione lineare, e quindi con l'uscita del sistema $y \left( n \right)$:
\[
x_z \left( n \right) \otimes h_z \left( n \right) = x \left( n \right) * h \left( n \right) = y \left( n \right) \quad n = 0 , \ldots , N_x + N_h - 2
\]
\begin{framed}
\paragraph{Dimostrazione}
\[
x \left( n \right) * h \left( n \right) = x_z \left( n \right) * h_z \left( n \right) = \sum_{m = - \infty}^{+ \infty } x_z \left( m \right) h_z \left( n - m \right) =
\]

Poiché $x_z \left( m \right)$ ha supporto finito $\left[ 0 , N_y - 1 \right]$:
\[
= \sum_{m = 0}^{N_y - 1} x_z \left( m \right) h_z \left( n - m \right)
\]

\begin{itemize}
\item Se $n < 0$:
\[
h_z \left( n - m \right) = 0 \quad  m \geq 0 \Rightarrow x_z \left( m \right) h_z \left( n - m \right) = 0 \quad \forall m \geq 0
\]
\item Se $n = N_y$:
\[
\begin{cases} h_z \left( n - m \right) = 0 & m \in \left[ 0 , N_y - N_h \right] = \left[ 0 , N_x  - 1 \right] \\
x_z \left( m \right) = 0 & m \geq N_x \end{cases} \Rightarrow x_z \left( m \right) h_z \left( n - m \right) = 0 \quad \forall m \geq 0
\]
\item Se $n = N_y + k$ ($k > 0$):
\[
\begin{cases} h_z \left( n - m \right) = 0 & m \in \left[ 0 , N_y + k - N_h \right] = \left[ 0 , N_x + k  - 1 \right] \\
x_z \left( m \right) = 0 & m \geq N_x \end{cases} \Rightarrow
\]
\[
\Rightarrow x_z \left( m \right) h_z \left( n - m \right) = 0 \quad \forall m \geq 0
\]
\end{itemize}
\end{framed}

Se il supporto $N_y = N_x + N_h - 1$ viene scelto come potenza di 2, si può impiegare la FFT per il calcolo delle 3 DFT, con complessità finale proporzionale a $N_y \log_2{\left( N_y \right)}$, invece della complessità dell'ordine di $N_h N_x$ operazioni per il calcolo della convoluzione nel dominio del tempo.